class ContactsController < ApplicationController
  def new
    @banner = StaticPage.where(name: 'Contato').first.banner
    @contact = Contact.new
  end

  def create
    @banner = StaticPage.where(name: 'Contato').first.banner
    @contact = Contact.new(params[:contact])
    # @contact.request = request

    if @contact.deliver
      flash[:success] = "Mensagem enviada com sucesso."
      redirect_to root_path
    else
      flash[:error] = "A mensagem não pode ser enviada. Verifique se todos os campos foram preenchidos corretamente."
      render :new
    end
  end

  def contact_form_params
    params.require(:contact).permit(:name).permit(:email).permit(:message)
  end
end
