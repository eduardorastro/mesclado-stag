class BannerPhoto < ActiveRecord::Base
  acts_as_paranoid

  has_attached_file :photo, preserve_files: true, :styles => { :big => "1600x360#" }
  validates_attachment :photo, presence: true, content_type: { content_type: ["image/jpeg", "image/jpg", "image/JPG", "image/JPEG", "image/gif", "image/png"] }, size: { less_than: 2.megabyte }

  belongs_to :banner, -> { with_deleted }, inverse_of: :banner_photos
end
