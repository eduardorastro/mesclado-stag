class AddShippingPriceInCentsToCarts < ActiveRecord::Migration
  def change
    change_table :carts do |t|
      t.integer :shipping_price_in_cents
      t.references :address, index: true
      t.text :address_hash
      t.text :user_hash
    end
  end
end
