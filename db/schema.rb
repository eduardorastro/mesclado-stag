# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151102002436) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"
  enable_extension "unaccent"

  create_table "about_strips", force: true do |t|
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "about_id"
    t.string   "strip_title"
    t.text     "strip_text"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "about_strips", ["about_id"], name: "index_about_strips_on_about_id", using: :btree

  create_table "abouts", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "absolute_advantages", force: true do |t|
    t.integer  "advantage_id"
    t.integer  "amount_in_cents"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "absolute_advantages", ["advantage_id"], name: "index_absolute_advantages_on_advantage_id", using: :btree
  add_index "absolute_advantages", ["deleted_at"], name: "index_absolute_advantages_on_deleted_at", using: :btree

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "addresses", force: true do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "street"
    t.string   "street_number"
    t.string   "complement"
    t.string   "zip_code"
    t.string   "neighborhood"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "addresses", ["deleted_at"], name: "index_addresses_on_deleted_at", using: :btree
  add_index "addresses", ["owner_id", "owner_type"], name: "index_addresses_on_owner_id_and_owner_type", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "advantages", force: true do |t|
    t.integer  "promotion_profile_id"
    t.string   "promotion_profile_type"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "free_shipping"
  end

  add_index "advantages", ["deleted_at"], name: "index_advantages_on_deleted_at", using: :btree
  add_index "advantages", ["promotion_profile_id"], name: "index_advantage_promo_profile_id", using: :btree

  create_table "artist_photos", force: true do |t|
    t.integer  "artist_id"
    t.boolean  "default",            default: false
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer  "position",           default: 0
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "artist_photos", ["artist_id"], name: "index_artist_photos_on_artist_id", using: :btree
  add_index "artist_photos", ["deleted_at"], name: "index_artist_photos_on_deleted_at", using: :btree

  create_table "artist_promotions", force: true do |t|
    t.integer  "promotion_id"
    t.integer  "artist_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "artist_promotions", ["artist_id"], name: "index_artist_promotions_on_artist_id", using: :btree
  add_index "artist_promotions", ["deleted_at"], name: "index_artist_promotions_on_deleted_at", using: :btree
  add_index "artist_promotions", ["promotion_id"], name: "index_artist_promotions_on_promotion_id", using: :btree

  create_table "artists", force: true do |t|
    t.boolean  "active",          default: true
    t.string   "name",                           null: false
    t.string   "sub_title"
    t.text     "description"
    t.string   "slug"
    t.integer  "number_of_views", default: 0
    t.integer  "number_of_sells", default: 0
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "facebook_url"
    t.string   "instagram_url"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "public",          default: true
  end

  add_index "artists", ["active"], name: "index_artists_on_active", using: :btree
  add_index "artists", ["deleted_at"], name: "index_artists_on_deleted_at", using: :btree
  add_index "artists", ["slug"], name: "index_artists_on_slug", using: :btree

  create_table "artists_keywords", id: false, force: true do |t|
    t.integer "keyword_id"
    t.integer "artist_id"
  end

  add_index "artists_keywords", ["keyword_id", "artist_id"], name: "index_artists_keywords_on_keyword_id_and_artist_id", using: :btree

  create_table "banner_photos", force: true do |t|
    t.integer  "banner_id"
    t.integer  "position",           default: 0
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "banner_photos", ["banner_id"], name: "index_banner_photos_on_banner_id", using: :btree
  add_index "banner_photos", ["deleted_at"], name: "index_banner_photos_on_deleted_at", using: :btree
  add_index "banner_photos", ["position"], name: "index_banner_photos_on_position", using: :btree

  create_table "banners", force: true do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.boolean  "active",     default: true
    t.string   "url"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "banners", ["active"], name: "index_banners_on_active", using: :btree
  add_index "banners", ["deleted_at"], name: "index_banners_on_deleted_at", using: :btree
  add_index "banners", ["owner_id", "owner_type"], name: "index_banners_on_owner_id_and_owner_type", using: :btree

  create_table "cart_items", force: true do |t|
    t.integer  "product_variation_id"
    t.integer  "cart_id"
    t.integer  "amount"
    t.integer  "unit_price_in_cents"
    t.integer  "discount_in_cents"
    t.text     "values_ids"
    t.text     "values_names"
    t.integer  "product_variation_size_id"
    t.string   "size"
    t.text     "variation_values_hash"
    t.text     "product_variation_hash"
    t.text     "product_hash"
    t.text     "artist_hash"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "promotion_id"
    t.integer  "coupon_id"
  end

  add_index "cart_items", ["cart_id"], name: "index_cart_items_on_cart_id", using: :btree
  add_index "cart_items", ["coupon_id"], name: "index_cart_items_on_coupon_id", using: :btree
  add_index "cart_items", ["deleted_at"], name: "index_cart_items_on_deleted_at", using: :btree
  add_index "cart_items", ["product_variation_id"], name: "index_cart_items_on_product_variation_id", using: :btree
  add_index "cart_items", ["promotion_id"], name: "index_cart_items_on_promotion_id", using: :btree

  create_table "cart_promotions", force: true do |t|
    t.integer  "promotion_id"
    t.integer  "minimum_cart_total_price_in_cents", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cart_promotions", ["deleted_at"], name: "index_cart_promotions_on_deleted_at", using: :btree
  add_index "cart_promotions", ["promotion_id"], name: "index_cart_promotions_on_promotion_id", using: :btree

  create_table "carts", force: true do |t|
    t.integer  "user_id"
    t.datetime "payment_date"
    t.string   "tracking_code"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "shipping_price_in_cents"
    t.integer  "address_id"
    t.text     "address_hash"
    t.text     "user_hash"
    t.text     "shipping_hash"
    t.integer  "promotion_id"
    t.integer  "coupon_id"
    t.text     "phone_hash"
    t.integer  "payments_counter",        default: 0
  end

  add_index "carts", ["address_id"], name: "index_carts_on_address_id", using: :btree
  add_index "carts", ["coupon_id"], name: "index_carts_on_coupon_id", using: :btree
  add_index "carts", ["deleted_at"], name: "index_carts_on_deleted_at", using: :btree
  add_index "carts", ["promotion_id"], name: "index_carts_on_promotion_id", using: :btree
  add_index "carts", ["user_id"], name: "index_carts_on_user_id", using: :btree

  create_table "coupons", force: true do |t|
    t.integer  "promotion_id"
    t.string   "code"
    t.integer  "maximum_uses"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "coupons", ["deleted_at"], name: "index_coupons_on_deleted_at", using: :btree
  add_index "coupons", ["promotion_id"], name: "index_coupons_on_promotion_id", using: :btree

  create_table "delivery_packages", force: true do |t|
    t.integer  "maximum_weight_in_grams"
    t.integer  "width_in_centimeters"
    t.integer  "height_in_centimeters"
    t.integer  "depth_in_centimeters"
    t.integer  "maximum_product_amount"
    t.integer  "extra_price_in_cents",    default: 0
    t.integer  "position",                default: 0
    t.boolean  "active",                  default: true
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "delivery_packages", ["active"], name: "index_delivery_packages_on_active", using: :btree
  add_index "delivery_packages", ["deleted_at"], name: "index_delivery_packages_on_deleted_at", using: :btree

  create_table "faq_questions", force: true do |t|
    t.boolean  "active",         default: true
    t.text     "question"
    t.text     "answer"
    t.integer  "position",       default: 0
    t.integer  "static_page_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "faq_questions", ["active"], name: "index_faq_questions_on_active", using: :btree
  add_index "faq_questions", ["deleted_at"], name: "index_faq_questions_on_deleted_at", using: :btree
  add_index "faq_questions", ["position"], name: "index_faq_questions_on_position", using: :btree
  add_index "faq_questions", ["static_page_id"], name: "index_faq_questions_on_static_page_id", using: :btree

  create_table "favorites", force: true do |t|
    t.integer  "product_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "favorites", ["product_id"], name: "index_favorites_on_product_id", using: :btree
  add_index "favorites", ["user_id"], name: "index_favorites_on_user_id", using: :btree

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "keywords", force: true do |t|
    t.string   "name",                    null: false
    t.integer  "total_times", default: 0
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "keywords", ["deleted_at"], name: "index_keywords_on_deleted_at", using: :btree
  add_index "keywords", ["total_times"], name: "index_keywords_on_total_times", using: :btree

  create_table "keywords_products", id: false, force: true do |t|
    t.integer "keyword_id"
    t.integer "product_id"
  end

  add_index "keywords_products", ["keyword_id", "product_id"], name: "index_keywords_products_on_keyword_id_and_product_id", using: :btree

  create_table "meta_infos", force: true do |t|
    t.integer  "owner_id"
    t.string   "owner_type"
    t.text     "meta_keys"
    t.string   "meta_title"
    t.text     "meta_description"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "meta_infos", ["deleted_at"], name: "index_meta_infos_on_deleted_at", using: :btree
  add_index "meta_infos", ["owner_id", "owner_type"], name: "index_meta_infos_on_owner_id_and_owner_type", using: :btree

  create_table "newsletters", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "percentual_advantages", force: true do |t|
    t.integer  "advantage_id"
    t.integer  "percentual_amount"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "percentual_advantages", ["advantage_id"], name: "index_percentual_advantages_on_advantage_id", using: :btree
  add_index "percentual_advantages", ["deleted_at"], name: "index_percentual_advantages_on_deleted_at", using: :btree

  create_table "pg_search_documents", force: true do |t|
    t.text     "content"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "phones", force: true do |t|
    t.string   "area_code"
    t.string   "number"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "phones", ["deleted_at"], name: "index_phones_on_deleted_at", using: :btree
  add_index "phones", ["owner_id", "owner_type"], name: "index_phones_on_owner_id_and_owner_type", using: :btree

  create_table "pos", force: true do |t|
    t.string   "name"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.text     "description"
    t.string   "address"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pos", ["deleted_at"], name: "index_pos_on_deleted_at", using: :btree

  create_table "product_photos", force: true do |t|
    t.integer  "product_id"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_photos", ["deleted_at"], name: "index_product_photos_on_deleted_at", using: :btree
  add_index "product_photos", ["product_id"], name: "index_product_photos_on_product_id", using: :btree

  create_table "product_promotions", force: true do |t|
    t.integer  "promotion_id"
    t.integer  "product_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "all_products", default: false
  end

  add_index "product_promotions", ["deleted_at"], name: "index_product_promotions_on_deleted_at", using: :btree
  add_index "product_promotions", ["product_id"], name: "index_product_promotions_on_product_id", using: :btree
  add_index "product_promotions", ["promotion_id"], name: "index_product_promotions_on_promotion_id", using: :btree

  create_table "product_variation_photos", force: true do |t|
    t.integer  "product_variation_id"
    t.boolean  "default",              default: true
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "deleted_at"
    t.integer  "position",             default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_variation_photos", ["deleted_at"], name: "index_product_variation_photos_on_deleted_at", using: :btree
  add_index "product_variation_photos", ["product_variation_id"], name: "index_product_variation_photos_on_product_variation_id", using: :btree

  create_table "product_variation_sizes", force: true do |t|
    t.boolean  "active",                      default: true
    t.string   "name"
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "selected_photo_file_name"
    t.string   "selected_photo_content_type"
    t.integer  "selected_photo_file_size"
    t.datetime "selected_photo_updated_at"
    t.integer  "position",                    default: 0
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_variation_sizes", ["active"], name: "index_product_variation_sizes_on_active", using: :btree
  add_index "product_variation_sizes", ["deleted_at"], name: "index_product_variation_sizes_on_deleted_at", using: :btree

  create_table "product_variation_sizes_product_variation_type_values", id: false, force: true do |t|
    t.integer "product_variation_size_id"
    t.integer "product_variation_type_value_id"
  end

  add_index "product_variation_sizes_product_variation_type_values", ["product_variation_size_id", "product_variation_type_value_id"], name: "index_prod_var_size_prod_var_type_value", unique: true, using: :btree

  create_table "product_variation_type_values", force: true do |t|
    t.boolean  "active",                      default: true
    t.integer  "product_variation_type_id"
    t.string   "name"
    t.string   "value"
    t.integer  "price_in_cents",              default: 0
    t.string   "photo_file_name"
    t.string   "photo_content_type"
    t.integer  "photo_file_size"
    t.datetime "photo_updated_at"
    t.string   "selected_photo_file_name"
    t.string   "selected_photo_content_type"
    t.integer  "selected_photo_file_size"
    t.datetime "selected_photo_updated_at"
    t.integer  "position",                    default: 0
    t.datetime "deleted_at"
    t.boolean  "keep_old_photos",             default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "product_code"
  end

  add_index "product_variation_type_values", ["active"], name: "index_product_variation_type_values_on_active", using: :btree
  add_index "product_variation_type_values", ["deleted_at"], name: "index_product_variation_type_values_on_deleted_at", using: :btree
  add_index "product_variation_type_values", ["product_variation_type_id"], name: "index_var_value_type", using: :btree

  create_table "product_variation_types", force: true do |t|
    t.boolean  "active",           default: true
    t.string   "name"
    t.datetime "deleted_at"
    t.integer  "position",         default: 0
    t.boolean  "customize_photos", default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_variation_types", ["active"], name: "index_product_variation_types_on_active", using: :btree
  add_index "product_variation_types", ["deleted_at"], name: "index_product_variation_types_on_deleted_at", using: :btree

  create_table "product_variations", force: true do |t|
    t.boolean  "active",                 default: true
    t.string   "product_variation_code"
    t.integer  "product_id"
    t.boolean  "default",                default: false
    t.integer  "position",               default: 0
    t.datetime "deleted_at"
    t.boolean  "customize_photos"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_variations", ["active"], name: "index_product_variations_on_active", using: :btree
  add_index "product_variations", ["deleted_at"], name: "index_product_variations_on_deleted_at", using: :btree
  add_index "product_variations", ["product_id"], name: "index_product_variations_on_product_id", using: :btree

  create_table "product_variations_product_variation_type_values", id: false, force: true do |t|
    t.integer "product_variation_id"
    t.integer "product_variation_type_value_id"
  end

  add_index "product_variations_product_variation_type_values", ["product_variation_id", "product_variation_type_value_id"], name: "index_prod_var_prod_var_type_value", unique: true, using: :btree

  create_table "products", force: true do |t|
    t.boolean  "active",                default: true
    t.boolean  "highlight",             default: false
    t.string   "product_code"
    t.string   "name",                                  null: false
    t.string   "sub_title"
    t.text     "description"
    t.text     "technical_details"
    t.text     "inspiration"
    t.integer  "artist_id"
    t.integer  "position",              default: 0
    t.integer  "price_in_cents",                        null: false
    t.integer  "gross_weight_in_grams"
    t.integer  "net_weight_in_grams"
    t.date     "new_from"
    t.date     "new_to"
    t.string   "slug"
    t.integer  "number_of_sells",       default: 0
    t.integer  "number_of_views",       default: 0
    t.integer  "number_of_shows",       default: 0
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "public",                default: true
  end

  add_index "products", ["active"], name: "index_products_on_active", using: :btree
  add_index "products", ["artist_id"], name: "index_products_on_artist_id", using: :btree
  add_index "products", ["deleted_at"], name: "index_products_on_deleted_at", using: :btree
  add_index "products", ["highlight"], name: "index_products_on_highlight", using: :btree
  add_index "products", ["position"], name: "index_products_on_position", using: :btree
  add_index "products", ["slug"], name: "index_products_on_slug", using: :btree

  create_table "products_product_variation_types", id: false, force: true do |t|
    t.integer "product_variation_type_id"
    t.integer "product_id"
  end

  add_index "products_product_variation_types", ["product_id", "product_variation_type_id"], name: "index_prod_prod_var_type", unique: true, using: :btree

  create_table "promotions", force: true do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "uses",       default: 0
    t.boolean  "active",     default: true
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "promotions", ["deleted_at"], name: "index_promotions_on_deleted_at", using: :btree

  create_table "sessions", force: true do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "shipping_informations", force: true do |t|
    t.string   "zip_code"
    t.text     "services"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "free_services"
  end

  create_table "static_pages", force: true do |t|
    t.string   "name",             null: false
    t.string   "title"
    t.text     "content"
    t.string   "footer_link_name"
    t.string   "slug"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "static_pages", ["deleted_at"], name: "index_static_pages_on_deleted_at", using: :btree
  add_index "static_pages", ["slug"], name: "index_static_pages_on_slug", using: :btree

  create_table "testimonials", force: true do |t|
    t.string   "test_title"
    t.string   "test_author"
    t.string   "test_from"
    t.integer  "about_id"
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "testimonials", ["about_id"], name: "index_testimonials_on_about_id", using: :btree

  create_table "transactions", force: true do |t|
    t.integer  "cart_id"
    t.integer  "status"
    t.string   "pagseguro_transaction_code"
    t.string   "pagseguro_transaction_url"
    t.text     "pagseguro_transaction_hash"
    t.string   "pagseguro_transaction_reference"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "transactions", ["cart_id"], name: "index_transactions_on_cart_id", using: :btree
  add_index "transactions", ["deleted_at"], name: "index_transactions_on_deleted_at", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "cpf"
    t.string   "provider"
    t.string   "uid"
    t.text     "oauth_hash"
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
