ActiveAdmin.register ProductVariationType do
  menu parent: 'Produtos', priority: 2

  config.sort_order = 'position_asc' # assumes you are using 'position' for your acts_as_list column
  config.paginate   = false # optional; drag-and-drop across pages is not supported

  sortable # creates the controller action which handles the sorting

  actions :index, :show, :new, :create, :edit, :update

  config.batch_actions = false

  permit_params :active,
  :customize_photos,
  :name,
  product_ids: [],
  product_variation_type_values_attributes: [
    :id,
    :active,
    :name,
    :value,
    :price_in_cents,
    :photo,
    :selected_photo,
    :position,
    :keep_old_photos,
    :_destroy,
    product_variation_size_ids: []
  ]

  form do |f|
    f.actions

    f.semantic_errors *f.object.errors.keys

    f.inputs "Detalhes Gerais", :class => 'inputs expandable in' do
      f.input :name, hint: 'Por exemplo: modelo, tecido, cor, etc...'
      # f.input :active
      # f.input :customize_photos, hint: 'Ao marcar para customizar fotos, as variações do produto geradas através dessa variação irão exigir uma foto para cada tipo de valor. Por exemplo, se essa variação for de cor e tiver os valores branco e preto, será obrigatório o upload de uma foto para o produto em cor preta e outra em cor branca.'
      f.input :products, as: :select, multiple: true, hint: 'Deixe em branco caso todas as estampas possuam essa característica.'
    end

    f.has_many :product_variation_type_values, heading: "Valores", allow_destroy: true, sortable: :position, new_record: "Adicionar Valor", :class => "inputs expandable in" do |value_form|
      if f.object.new_record?
        value_form.input :keep_old_photos, hint: "A criação de uma nova característica, gera novas combinações para camisas já criadas anteriormente. Ao selecionar essa opção, as fotos antigas passarão a ter esse valor da nova característica."
      end
      value_form.input :active
      value_form.input :name
      value_form.input :price_in_cents, as: :string, hint: "Preço adicionado ao preço da estampa, ou seja, uma estampa com esse valor desta característica terá um valor final igual à soma dos dois preços"
      value_form.input :product_code

      value_form.input :value, hint: "Caso o ícone desse valor da característica deva ser preenchido com uma cor, coloque aqui o código RGB da cor. Por exemplo: se for branco, esse campo deverá ser preenchido com '#FFFFFF'. Caso seja um ícone, deixe em branco esse campo e selecione a imagem nos campos abaixo."

      value_form.input :photo, image_preview: true, size_hint: true

      value_form.input :selected_photo, image_preview: true, size_hint: true

      value_form.input :product_variation_sizes, as: :select, multiple: true, hint: 'Deixe em branco, caso todos os tamanhos estejam disponíveis para esse valor.'
    end

    f.actions
  end

  filter :name

  index do
    sortable_handle_column
    # column :active
    column :name
    column :product_variation_type_values do |type|
      type.product_variation_type_values.map { |x| x.name }.join(' - ')
    end
    actions
  end

  show do
    attributes_table do
      row :active
      row :name
      product_variation_type.product_variation_type_values.each do |value|
        if value.photo.present?
          row "Imagem #{value.name}" do
            content = image_tag(value.photo)
            content += image_tag(value.selected_photo)
            content
          end
        else
          row "Código da Cor do #{value.name}" do
            value.value
          end
        end
        row "Preço Adicionado #{value.name}" do
          value.price_in_cents.to_brazilian_currency
        end
      end
    end
  end

end
