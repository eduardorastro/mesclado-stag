class Advantage < ActiveRecord::Base
  acts_as_paranoid

  attr_accessor :amount_in_cents, :percentual_amount

  belongs_to :promotion_profile, polymorphic: true, inverse_of: :advantage

  has_one :absolute_advantage, inverse_of: :advantage
  accepts_nested_attributes_for :absolute_advantage, allow_destroy: false

  has_one :percentual_advantage, inverse_of: :advantage
  accepts_nested_attributes_for :percentual_advantage, allow_destroy: false

  # delegate :amount_in_cents, to: :absolute_advantage
  # delegate :percentual_amount, to: :percentual_advantage

  def advantage_profile
    self.absolute_advantage || self.percentual_advantage
  end

  def amount_in_cents= value
    if value.is_a? String
      value = value.gsub(/\D/, '')
    end

    return if value.blank? or value.to_i == 0

    unless self.absolute_advantage.present?
      self.absolute_advantage = AbsoluteAdvantage.new
    end

    self.absolute_advantage.amount_in_cents = value.to_i
  end

  def amount_in_cents
    if self.absolute_advantage.present?
      self.absolute_advantage.amount_in_cents
    else
      nil
    end
  end

  def percentual_amount= value
    if value.is_a? String
      value = value.gsub(/\D/, '')
    end

    return if value.blank? or value.to_i == 0

    unless self.percentual_advantage.present?
      self.percentual_advantage = PercentualAdvantage.new
    end

    self.percentual_advantage.percentual_amount = value.to_i
  end

  def percentual_amount
    if self.percentual_advantage.present?
      self.percentual_advantage.percentual_amount
    else
      nil
    end
  end

  before_validation :check_advantage_profile

  private

    def check_advantage_profile
      profiles_sum = (self.absolute_advantage ? 1 : 0) + (self.percentual_advantage ? 1 : 0)

      if profiles_sum == 0
        return if self.free_shipping

        self.errors.add :'absolute_advantage.amount_in_cents', 'menos um tipo de desconto deve ser escolhido'
        self.errors.add :'percentual_advantage.percentual_amount', 'menos um tipo de desconto deve ser escolhido'
      elsif profiles_sum > 1
        self.errors.add :'absolute_advantage.amount_in_cents', 'um tipo de desconto deve ser escolhido'
        self.errors.add :'percentual_advantage.percentual_amount', 'um tipo de desconto deve ser escolhido'
      end
    end
end
