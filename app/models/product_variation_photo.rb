class ProductVariationPhoto < ActiveRecord::Base
  acts_as_paranoid

  has_attached_file :photo, :styles => { :thumb => "445x522#", :standard => "780x780#" }, preserve_files: true
  validates_attachment :photo, presence: true, content_type: { content_type: ["image/jpeg", "image/jpg", "image/JPG", "image/JPEG", "image/gif", "image/png"] }, size: { less_than: 1.megabyte }

  belongs_to :product_variation, -> { with_deleted }
end
