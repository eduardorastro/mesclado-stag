Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations', omniauth_callbacks: 'users/omniauth_callbacks', passwords: 'users/passwords' }

  resources :newsletters, only: [:create]
  resources :contacts, only: [:new, :create], path: 'contato', path_names: { :new => 'novo' }

  resources :static_pages, only: [:show], path: 'institucional'
  get 'sobre', controller: 'static_pages', action: 'about', path: 'sobre', as: :about
  get 'pontos-de-venda', controller: 'static_pages', action: 'pos', path: 'pontos-de-venda', as: :pos

  resources :products, only: [:index, :show], path: 'produtos' do
    collection do
      get 'search', as: :search, path: 'busca'
      get 'page', as: :page, path: 'pagina'
    end
    resources :favorites, only: [:create]
  end

  resources :carts, only: [:show], path: 'carrinho' do
    resources :cart_items, only: [:create, :update, :destroy], path: 'itens'
    collection do
      get 'set_shipping_method', as: :set_shipping_method, path: 'frete-selecionado'
    end
  end

  resource :carts, only: [], path: '' do
    collection do
      get 'check_shipping_price', as: :check_shipping_price, path: 'calcular-frete'
    end
  end

  resource :transactions, only: [:new], path: 'transacoes' do
    collection do
      post 'notification', as: :notification, path: 'notificacao'
      post 'create', as: :create, path: 'create'
      post 'new', as: :new, path: 'new'
      get 'success', as: :success, path: 'obrigado'
    end
  end

  get 'transacoes', to: 'transactions#index', as: :transactions_index

  resources :coupons, only: [], path: 'cupons' do
    collection do
      get 'add', as: :add, path: 'adicionar'
    end
  end

  resources :favorites, only: [:index, :destroy], path: 'favoritos'

  resources :artists, only: [:index], path: 'artistas' do
    collection do
      get 'page', as: :page, path: 'pagina'
    end
  end
  resources :artists, only: [:show], path: '/'

  mount NewRelicPing::Engine => '/status'

  root to: 'static_pages#show', id: '1'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
