class ApplicationController < ActionController::Base
  include CartHelper

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper :cart

  before_filter :set_basic_variables

  def set_basic_variables
    @static_pages = StaticPage.where(deleted_at: nil).where.not(name: 'Home').where.not(name: 'Contato')
    @pos = Pos.all
  end

  # def after_sign_in_path_for(resource)
  #   if session[:after_redirect].present?
  #     session.delete(:after_redirect)
  #   else
  #     super
  #   end
  # end

  def after_sign_up_path_for(resource)
    if session[:after_redirect].present?
      session.delete(:after_redirect)
    else
      super
    end
  end

end
