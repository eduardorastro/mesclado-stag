class CreateProductsProductVariationTypes < ActiveRecord::Migration
  def change
    create_table :products_product_variation_types, id: false do |t|
      t.references :product_variation_type
      t.references :product
    end

    add_index :products_product_variation_types, [:product_id, :product_variation_type_id], unique: true, name: 'index_prod_prod_var_type'
  end
end
