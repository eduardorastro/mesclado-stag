class AddPublicToArtists < ActiveRecord::Migration
  def change
    add_column :artists, :public, :boolean, default: true
  end
end
