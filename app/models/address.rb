class Address < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :owner, polymorphic: true

  validates_presence_of :zip_code, :street, :street_number, :neighborhood, :city, :state

  after_save :update_user_cart_address_hash

  private

    def update_user_cart_address_hash
      if self.owner.present? and self.owner.active_cart.present?
        self.owner.active_cart.update_column(:address_hash, self.attributes)
      end
    end

end
