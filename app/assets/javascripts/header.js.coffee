ready = ->
  nav = $('nav.navbar')
  h = nav.height()

  opacity = 0
  opacity = 1 if nav.hasClass('black-bar')

  $(window).scroll ->
    scroll = $(document).scrollTop()
    if scroll > h
      nav.addClass('scrolled')
      nav.css
        'padding' : '10px 0 0 0'
      nav.find('ul').css
        'margin-bottom' : '10px'
    else
      nav.removeClass('scrolled')
      nav.css
        'padding' : '20px 0 0 0'
      nav.find('ul').css
        'margin-bottom' : '20px'

  $('html').click (e) ->
    obj = $(e.target)
    unless obj.hasClass('fa') || obj.hasClass('remove-item-link')
      $('.floating-cart').hide('fast')

  $('.shopping-cart').click (e) ->
    e.stopPropagation();
    $('.floating-cart').toggle('fast');

  $('.floating-cart').click (e) ->
    obj = $(e.target)
    unless obj.hasClass('fa') || obj.hasClass('remove-item-link')
      e.stopPropagation();

  timer = undefined

  $('.floating-cart').on 'show', ->
    clearTimeout(timer)
    timer = delay 4000, ->
      $('.floating-cart').hide('fast')

  $('.floating-cart').on 'hide', ->
    clearTimeout(timer)

  $('.floating-cart').hover ->
    clearTimeout(timer)
  , ->
    clearTimeout(timer)
    timer = delay 4000, ->
      $('.floating-cart').hide('fast')

  nav.find('li.dropdown').mouseover ->
    $(this).addClass('open')

  nav.find('li.dropdown').mouseout ->
    $(this).removeClass('open')


delay = (ms, func) ->
  setTimeout(func,  ms)


$(document).ready(ready)
$(document).on('page:load', ready)

$(window).load ->
  if $('#header-carousel').find('.item.cover').length > 1
    $('#header-carousel').owlCarousel
      autoplay: true
      autoplayTimeout: 6000
      navSpeed: 1200
      autoplaySpeed: 1200
      items: 1
      dots: true
      loop: true
      responsiveRefreshRate: 10
      autoWidth: false
      mouseDrag: false

  if $('#header-carousel').length > 0
    $('#header-carousel').css 'margin-top' : -$('#header-carousel').offset().top
