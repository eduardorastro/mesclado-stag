class CreateMetaInfos < ActiveRecord::Migration
  def change
    create_table :meta_infos do |t|
      t.references :owner, polymorphic: true, index: true

      t.text :meta_keys
      t.string :meta_title
      t.text :meta_description

      t.datetime :deleted_at

      t.timestamps
    end

    add_index :meta_infos, :deleted_at
  end
end
