include ActiveAdmin::MetaInfoHelper
include ActiveAdmin::BannerFormHelper

ActiveAdmin.register StaticPage do
  menu parent: "Páginas Institucionais", priority: 2, label: "Páginas de Texto"

  ################# FORM #################

  permit_params do
    params = [:name, :title, :content, :footer_link_name, :slug, faq_questions_attributes: [:id, :question, :answer, :position, :_destroy]]
    params += banner_permitted_params
    params += meta_info_permitted_params
    params
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    unless f.object.id == 1
      f.inputs "Detalhes Gerais" do
        f.input :name
        f.input :title
        f.input :slug
        f.input :content, input_html: { class: "tinymce small-textarea" }
      end
    end

    banner_form f

    meta_inputs f

    f.actions
  end

  ################# INDEX #################

  config.filters = false

  index do
    column :name
    column :title
    column :actions do |res|
      links = ''
      if res.home?
        links = link_to I18n.t('active_admin.view'), admin_home_path(res)
        links += link_to I18n.t('active_admin.edit'), edit_admin_home_path(res)
      elsif res.faq?
        links = link_to I18n.t('active_admin.view'), admin_faq_path(res)
        links += link_to I18n.t('active_admin.edit'), edit_admin_faq_path(res)
        links += link_to I18n.t('active_admin.delete'), admin_faq_path(res), confirm: 'Are you sure?', method: :delete
      else
        links = link_to I18n.t('active_admin.view'), resource_path(res)
        links += link_to I18n.t('active_admin.edit'), edit_resource_path(res)
        links += link_to I18n.t('active_admin.delete'), resource_path(res), confirm: 'Are you sure?', method: :delete
      end
      links
    end
  end

  controller do

    def show
      if resource.home?
        redirect_to admin_home_path(resource)
      elsif resource.faq?
        redirect_to admin_faq_path(resource)
      else
        super
      end
    end

    def edit
      if resource.home?
        redirect_to edit_admin_home_path(resource)
      elsif resource.faq?
        redirect_to edit_admin_faq_path(resource)
      else
        super
      end
    end

    def collection
      StaticPage.where(deleted_at: nil).where.not(name: 'Home').where.not(name: 'FAQ').where.not(name: 'Contato').page(params[:page]).per(10)
    end

  end

end
