class Pos < ActiveRecord::Base
  acts_as_paranoid

  has_attached_file :photo, :styles => { :standard => "230x230#" }, preserve_files: true
  validates_attachment :photo, presence: true, content_type: { content_type: ["image/jpeg", "image/jpg", "image/JPG", "image/JPEG", "image/gif", "image/png"] }, size: { less_than: 2.megabyte }

  validates_presence_of :name, :description, :address
end
