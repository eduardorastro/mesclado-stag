class Fixnum
  def to_brazilian_currency
    s = self.to_s.ljust(2, '0')
    "R$ #{s.length <= 2 ? '0' : s[0..-3]},#{s[-2..-1]}"
  end
end

class String
  def from_brazilian_currency
    self.gsub(/[R$]|[ ]|[,]/, '').to_i
  end
end
