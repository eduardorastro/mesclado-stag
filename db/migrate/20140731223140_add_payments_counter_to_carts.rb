class AddPaymentsCounterToCarts < ActiveRecord::Migration
  def change
    add_column :carts, :payments_counter, :integer, default: 0
  end
end
