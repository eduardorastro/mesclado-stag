class CreateShippingInformations < ActiveRecord::Migration
  def change
    create_table :shipping_informations do |t|
      t.string :zip_code
      t.text :services

      t.timestamps
    end
  end
end
