class CreateFaqQuestions < ActiveRecord::Migration
  def change
    create_table :faq_questions do |t|
      t.boolean :active, default: true
      t.text :question
      t.text :answer
      t.integer :position, default: 0
      t.references :static_page, index: true
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :faq_questions, :position
    add_index :faq_questions, :deleted_at
    add_index :faq_questions, :active
  end
end
