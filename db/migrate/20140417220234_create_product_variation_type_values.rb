class CreateProductVariationTypeValues < ActiveRecord::Migration
  def change
    create_table :product_variation_type_values do |t|
      t.boolean :active, default: true
      t.references :product_variation_type
      t.string :name
      t.string :value
      t.integer :price_in_cents, default: 0
      t.attachment :photo
      t.attachment :selected_photo
      t.integer :position, default: 0
      t.datetime :deleted_at
      t.boolean :keep_old_photos, default: false

      t.timestamps
    end
    add_index :product_variation_type_values, :active
    add_index :product_variation_type_values, :deleted_at
    add_index :product_variation_type_values, :product_variation_type_id, name: 'index_var_value_type'
  end
end
