ready = ->
  $('.artist-more-details').click ->
    col = $(this).parents('.artist-info-column:first')
    w = col.width()
    col.animate
      'margin-left' : -w/2
    , 'fast'
    col.next().animate
      'opacity' : 1
    , 'fast'
    $(this).fadeOut('fast')
    $(this).next().fadeIn('fast')

  $('.artist-less-details').click ->
    col = $(this).parents('.artist-info-column:first')
    w = col.width()
    col.animate
      'margin-left' : 0
    , 'fast'
    col.next().animate
      'opacity' : 0
    , 'fast'
    $(this).fadeOut('fast')
    $(this).prev().fadeIn('fast')

$(document).ready(ready)
$(document).on('page:load', ready)
