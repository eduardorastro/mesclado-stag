class Cart < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :user, inverse_of: :carts
  belongs_to :address, inverse_of: :carts
  belongs_to :promotion, inverse_of: :carts
  belongs_to :coupon, inverse_of: :carts

  has_many :cart_items, -> { order('created_at DESC') }, dependent: :destroy, inverse_of: :cart

  has_many :transactions, inverse_of: :cart

  serialize :address_hash
  serialize :phone_hash
  serialize :user_hash
  serialize :shipping_hash

  scope :opened, -> { where(payment_date: nil) }
  scope :closed, -> { where.not(payment_date: nil) }

  def total_items
    self.cart_items.map { |x| x.amount }.sum
  end

  def empty?
    self.cart_items.empty?
  end

  def total_price
    p = self.total_price_no_discount - self.discount

    if p < 0
      p = 0
    end

    p
    # self.total_price_no_shipping + self.shipping_price_in_cents - self.shipping_discount_in_cents - self.discount_in_cents
  end

  def total_price_no_shipping_no_coupon
    self.cart_items.map { |x| x.total_price }.sum
  end

  def total_price_no_shipping
    price = self.total_price_no_shipping_no_coupon
    if self.coupon.present?
      price -= self.coupon.discount_for_cart(self)
    end
    price
  end

  def total_price_no_discount
    self.total_price_no_shipping_no_discount + self.shipping_price_in_cents
  end

  def total_price_no_shipping_no_discount
    self.cart_items.map { |x| x.total_price_no_discount }.sum
  end

  def discount
     self.cart_discount + self.cart_items_discount
  end

  def cart_discount
    self.shipping_discount_in_cents + self.discount_in_cents
  end

  def cart_items_discount
    self.cart_items.map { |x| x.discount_in_cents * x.amount }.reject { |x| x.nil? }.sum
  end

  def shipping_discount_in_cents
    (self.all_promotions.map { |x| x.advantage.free_shipping }.include? true) ? self.shipping_price_in_cents : 0
  end

  def discount_in_cents
    if self.promotion.present? and self.promotion.advantage_profile.present?
      self.promotion.advantage_profile.discount(self.total_price_no_shipping_no_discount - self.cart_items_discount)
    else
      0
    end
  end

  def all_promotions
    [self.promotion, self.cart_items.map { |x| x.promotion }].flatten.reject { |x| x.nil? }
  end

  def shipping_price_in_cents
    super || 0
  end

  def closed?
    !self.payment_date.nil?
  end

  def add_product_variation product_variation_id, size_id, amount = 1
    self.update_attributes(shipping_hash: nil, shipping_price_in_cents: 0)
    ci = self.cart_items.where(product_variation_id: product_variation_id, product_variation_size_id: size_id).limit(1).first
    if ci
      ci.add_amount(amount)
    else
      pv = ProductVariation.find product_variation_id
      size = ProductVariationSize.find size_id
      if pv and size
        self.cart_items.create(product_variation_id: product_variation_id, amount: amount, unit_price_in_cents: pv.price_in_cents_no_discount, product_variation_size_id: size_id, discount_in_cents: pv.product.discount, promotion: pv.product.best_promotion)
      else
        nil
      end
    end
  end

  def remove_product_variation id
    self.update_attributes(shipping_hash: nil, shipping_price_in_cents: 0)
    ci = CartItem.find id
    if ci and ci.cart.id == self.id
      ci.really_destroy!
    else
      nil
    end
  end

  def merge cart
    if cart.class == Cart
      pv_ids = self.cart_items.map { |x| x.product_variation_id }
      cart.cart_items.each do |ci|
        if pv_ids.include? ci.product_variation_id
          self.cart_items.where(product_variation_id: ci.product_variation_id).limit(1).first.add_amount(ci.amount)
        else
          self.cart_items.push ci
          self.save
        end
      end
      cart.reload
      cart.destroy
    else
      raise 'Carrinho deve ser da classe Cart.'
    end
  end

  def reference
    "id#{self.id}at#{self.transactions.length}"
  end

  def create_pagseguro_transparente checkout_card_bandeira, checkout_tipo_pagamento, sender_hash, card_token, checkout_card_name, checkout_name, checkout_email, checkout_cpf, checkout_ddd, checkout_telefone, checkout_card_parcelas, checkout_nascimento, zip_code, street, street_number, complement, neighborhood, city, state

    return nil if self.cart_items.empty? or self.shipping_hash.blank? or self.user_hash.blank?


    
      

      if Rails.env.development?
        # notification_url = notification_url.gsub('localhost:3000', ENV['NOTIFICATION_URL'])
        # redirect_url = redirect_url.gsub('localhost:3000', ENV['REDIRECT_URL'])
        notification_url = ENV['NOTIFICATION_URL']
        redirect_url = ENV['REDIRECT_URL']
      end

      # CHECA SE O TIPO DE PAGAMENTO ESCOLHIDO FOI CARTÃO
      if checkout_tipo_pagamento == 'tipo-cc'


            payment = PagSeguro::CreditCardTransactionRequest.new
            payment.payment_mode = "default" # default
            payment.notification_url = ENV['NOTIFICATION_URL']


            # ENTREGA

            if self.shipping_hash.present?
              
              if self.shipping_price_in_cents > 0
                shipping_cost_value = self.shipping_price_in_cents / 100.0
              else
                shipping_cost_value = 0.00
              end


              if self.shipping_discount_in_cents > 0
                shipping_cost_discount = self.shipping_discount_in_cents / 100.0
              else
                shipping_cost_discount = 0.00
              end

              # shipping_cost = shipping_cost_value - shipping_cost_discount

              


              if self.address_hash.present?
                if STATES.keys.include? address_hash['state'].downcase.to_sym
                  address_hash['state'] = STATES[address_hash['state'].downcase.to_sym]
                end
 
                


              end

            end


            payment.shipping = {
              cost: shipping_cost_value,
              address: {
                district: neighborhood,
                city: city,
                country: 'Brazil',
                state: state,
                street: street,
                complement: complement,
                number: street_number,
                postal_code: zip_code
              }
            }

            payment.billing_address = {
              district: neighborhood,
              city: city,
              country: 'Brazil',
              state: state,
              street: street,
              complement: complement,
              number: street_number,
              postal_code: zip_code
            }


            puts "CARRINHO -- CARTÃO --- PARAMETRO CEP -----> #{zip_code}"
            puts "CARRINHO -- CARTÃO --- PARAMETRO RUA -----> #{street}"
            puts "CARRINHO -- CARTÃO --- PARAMETRO NUMERO -----> #{street_number}"
            puts "CARRINHO -- CARTÃO --- PARAMETRO COMPLEMENTO -----> #{complement}"
            puts "CARRINHO -- CARTÃO --- PARAMETRO BAIRRO -----> #{neighborhood}"
            puts "CARRINHO -- CARTÃO --- PARAMETRO CIDADE -----> #{city}"
            puts "CARRINHO -- CARTÃO --- PARAMETRO ESTADO -----> #{state}"



            # ITENS
            valor_total_produtos = 0

            self.cart_items.each do |ci|
              payment.items << {
                id: ci.id,
                description: ci.name,
                amount: ci.unit_price_in_cents / 100.0,
                quantity: ci.amount
              }

              valor_total_produtos += ci.unit_price_in_cents / 100.0
            end

            payment.extra_amount = -self.discount / 100.0

            # Criando uma referencia para a nossa ORDER
            payment.reference = self.reference
            


            # SENDER
            if self.user_hash.present?

              payment.sender = {
                hash: sender_hash,
                name: user_hash['name'],
                email: user_hash['email'],
                phone: {
                  area_code: checkout_ddd,
                  number: checkout_telefone
                },
                cpf: checkout_cpf

              }
            end

            payment.credit_card_token = card_token
            payment.holder = {
              name: checkout_card_name,
              birth_date: checkout_nascimento,
              document: {
                type: "CPF",
                value: checkout_cpf
              },
              phone: {
                area_code: checkout_ddd,
                number: checkout_telefone
              }
            }

            valor_total = sprintf "%.2f", self.total_price / 100.00

            puts "VALOR_TOTAL --------------> #{valor_total}"

            get_all_installments = PagSeguro::Installment.find(valor_total, checkout_card_bandeira)

            installment_total_amount = []

            get_all_installments.each do |x|
              installment_total_amount << x.amount
            end

            parcelas = checkout_card_parcelas.to_i - 1

            payment.installment = {
              # value: valor_total_produtos + shipping_cost,
              value: installment_total_amount[parcelas],
              quantity: checkout_card_parcelas
            }

            # payment.extra_params = [{
            #   noInterestInstallmentQuantity: 1
            # }]

            


            puts "=> REQUEST"
            puts PagSeguro::TransactionRequest::RequestSerializer.new(payment).to_params
            puts

            payment.create


      # SE NÃO FOR CARTÃO, MAS FOR BOLETO
      else 




            payment = PagSeguro::BoletoTransactionRequest.new
            payment.payment_mode = "default" # default
            payment.notification_url = ENV['NOTIFICATION_URL']



            # ENTREGA
            if self.shipping_hash.present?
              
              if self.shipping_price_in_cents > 0
                shipping_cost_value = self.shipping_price_in_cents / 100.0
              else
                shipping_cost_value = 0.00
              end


              if self.shipping_discount_in_cents > 0
                shipping_cost_discount = self.shipping_discount_in_cents / 100.0
              else
                shipping_cost_discount = 0.00
              end

              shipping_cost = shipping_cost_value - shipping_cost_discount


              if self.address_hash.present?
                if STATES.keys.include? address_hash['state'].downcase.to_sym
                  address_hash['state'] = STATES[address_hash['state'].downcase.to_sym]
                end

                


              end

            end


            payment.shipping = {
              cost: shipping_cost_value,
              address: {
                district: neighborhood,
                city: city,
                country: 'Brazil',
                state: state,
                street: street,
                complement: complement,
                number: street_number,
                postal_code: zip_code
              }
            }


            puts "CARRINHO -- BOLETO --- PARAMETRO CEP -----> #{zip_code}"
            puts "CARRINHO -- BOLETO --- PARAMETRO RUA -----> #{street}"
            puts "CARRINHO -- BOLETO --- PARAMETRO NUMERO -----> #{street_number}"
            puts "CARRINHO -- BOLETO --- PARAMETRO COMPLEMENTO -----> #{complement}"
            puts "CARRINHO -- BOLETO --- PARAMETRO BAIRRO -----> #{neighborhood}"
            puts "CARRINHO -- BOLETO --- PARAMETRO CIDADE -----> #{city}"
            puts "CARRINHO -- BOLETO --- PARAMETRO ESTADO -----> #{state}"




            # ITENS
            valor_total_produtos = 0

            self.cart_items.each do |ci|
              payment.items << {
                id: ci.id,
                description: ci.name,
                amount: ci.unit_price_in_cents / 100.0,
                quantity: ci.amount
              }

              valor_total_produtos += ci.unit_price_in_cents / 100.0
            end

            payment.extra_amount = -self.discount / 100.0

            # Criando uma referencia para a nossa ORDER
            payment.reference = self.reference




            # SENDER
            if self.user_hash.present?

              payment.sender = {
                hash: sender_hash,
                name: user_hash['name'],
                email: user_hash['email'],
                phone: {
                  area_code: checkout_ddd,
                  number: checkout_telefone
                },
                cpf: checkout_cpf

              }
            end


            puts "=> REQUEST"
            puts PagSeguro::TransactionRequest::RequestSerializer.new(payment).to_params
            puts

            payment.create
            

      end

  end






  def create_pagseguro notification_url, redirect_url
    return nil if self.cart_items.empty? or self.shipping_hash.blank? or self.user_hash.blank?

    # payment = PagSeguro::PaymentRequest.new

    pagseguro_token = Rails.env.development? ? ENV['PAGSEGURO_SANDBOX_TOKEN'] : ENV['PAGSEGURO_TOKEN']
    pagseguro_email = Rails.env.development? ? ENV['PAGSEGURO_SANDBOX_EMAIL'] : ENV['PAGSEGURO_EMAIL']

    if Rails.env.development?
      notification_url = notification_url.gsub('localhost:3000', 'cb692bde.ngrok.io')
      redirect_url = redirect_url.gsub('localhost:3000', 'cb692bde.ngrok.io')
    end

    payment = PagSeguro::PaymentRequest.new(token: pagseguro_token, email: pagseguro_email)


    if self.shipping_hash.present?
      payment.shipping = PagSeguro::Shipping.new
      payment.shipping.type_name = self.shipping_hash['name'].downcase
      if self.shipping_price_in_cents > 0
        payment.shipping.cost = self.shipping_price_in_cents / 100.0
      else
        payment.shipping.cost = 0
      end


      if self.address_hash.present?
        if STATES.keys.include? address_hash['state'].downcase.to_sym
          address_hash['state'] = STATES[address_hash['state'].downcase.to_sym]
        end
        
        payment.shipping.address = PagSeguro::Address.new
        payment.shipping.address.street = self.address_hash['street'],
        payment.shipping.address.number = self.address_hash['street_number'],
        payment.shipping.address.complement = self.address_hash['complement'],
        payment.shipping.address.district = self.address_hash['neighborhood'],
        payment.shipping.address.city = self.address_hash['city'],
        payment.shipping.address.state = self.address_hash['state'],
        payment.shipping.address.postal_code = self.address_hash['zip_code']
      end
    end

    if self.user_hash.present?
      payment.sender = PagSeguro::Sender.new
      payment.sender.name = user_hash['name']
      payment.sender.email = user_hash['email']
      payment.sender.cpf = user_hash['cpf']
    end

    if Rails.env.development?
      notification_url = notification_url.gsub('localhost:3000', 'mesclado.ngrok.com')
    end

    payment.reference = self.reference
    payment.notification_url = notification_url
    payment.redirect_url = redirect_url
    payment.max_age = 1200

    self.cart_items.each do |ci|
      payment.items << {
        id: ci.id,
        description: ci.name,
        amount: ci.unit_price_in_cents / 100.0,
        # amount: 1,
        quantity: ci.amount
      }
    end

    payment.extra_amount = -self.discount / 100.0

    payment
  end

  def self.cleanup
    where(['updated_at < ? AND user_id IS NULL', 12.hours.ago]).find_each do |c|
      c.really_destroy!
    end
  end

  def divide_in_delivery_packages
    possible_packages = DeliveryPackage.all
    big_ones = self.total_items/6
  end

  def update_promotions
    self.check_if_promotions_are_valid?

    self.cart_items.each do |ci|
      ci.update_promotions
    end
  end

  def check_if_promotions_are_valid?
    if self.promotion.present?
      unless self.promotion.valid_for?(self)
        self.promotion = nil
        self.coupon = nil
        self.save(validate: false)
      end
    end
    self.cart_items.each do |ci|
      ci.check_if_promotion_is_valid?
    end
  end

  def update_products_number_of_sells
    self.cart_items.each do |ci|
      next if ci.product_variation.nil?

      product = ci.product_variation.product
      next if product.nil?

      product.update_column(:number_of_sells,
        product.number_of_sells + ci.amount
      )
    end
  end

  before_validation :check_other_carts
  after_validation :set_extra_variables

  private

    def check_other_carts
      unless self.user_id.nil?
        c = self.user.get_last_open_cart
        unless c.nil? or c.id == self.id
          self.errors.add(:user, 'deve ter apenas um carrinho de compras')
        end
      end
    end

    def set_extra_variables
      if (self.user_id_changed? or self.payment_date_changed? or self.user_hash.blank?) and self.user_id.present?
        self.user_hash = self.user.attributes.merge('name' => self.user.name)
      end

      if (self.payment_date_changed? or self.address_hash.blank?) and self.user.present? and self.user.address.present?
        self.address_hash = self.user.address.attributes
      end

      if (self.payment_date_changed? or self.phone_hash.blank?) and self.user.present? and self.user.phone.present?
        self.phone_hash = self.user.phone.attributes
      end
    end

end
