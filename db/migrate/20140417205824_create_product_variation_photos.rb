class CreateProductVariationPhotos < ActiveRecord::Migration
  def change
    create_table :product_variation_photos do |t|
      t.references :product_variation, index: true
      t.boolean :default, default: true
      t.attachment :photo
      t.datetime :deleted_at
      t.integer :position, default: 0

      t.timestamps
    end

    add_index :product_variation_photos, :deleted_at
  end
end
