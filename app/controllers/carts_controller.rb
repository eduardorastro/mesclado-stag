class CartsController < ApplicationController
  respond_to :js

  def show
    if get_cart.empty?
      redirect_to root_path
    else
      get_cart.update_promotions
      check_promotions_for_cart get_cart
      @cart = get_cart
    end
  end

  def check_shipping_price
    @cart = get_cart

    zip_code = BrZipCode.formatted(params[:zip_code]).gsub('.', '')

    shipping = nil

    packages = DeliveryPackage.divide_in_packages @cart.total_items
    extra_price = packages.map { |x| x.extra_price_in_cents }.sum / 100.0

    if packages.length == 1
      p = packages.first
      shipping = Correios::Frete::Calculador.new cep_origem: ShippingInformation.first.formatted_zip_code, cep_destino: zip_code, peso: p.maximum_weight_in_grams/1000.0, comprimento: (p.depth_in_centimeters >= 16 ? p.depth_in_centimeters : 16), largura: (p.width_in_centimeters >= 11 ? p.width_in_centimeters : 11), altura: (p.height_in_centimeters >= 2 ? p.height_in_centimeters : 2)
    else
      items = []
      packages.each do |p|
        items.push Correios::Frete::PacoteItem.new(peso: p.maximum_weight_in_grams/1000.0, comprimento: (p.depth_in_centimeters >= 16 ? p.depth_in_centimeters : 16), largura: (p.width_in_centimeters >= 11 ? p.width_in_centimeters : 11), altura: (p.height_in_centimeters >= 2 ? p.height_in_centimeters : 2))
      end
      delivery_package = Correios::Frete::Pacote.new
      items.each do |i|
        delivery_package.adicionar_item i
      end

      shipping = Correios::Frete::Calculador.new cep_origem: ShippingInformation.first.formatted_zip_code, cep_destino: zip_code, encomenda: delivery_package
    end

    # free_services = ShippingInformation.first.free_services.reject { |x| x.blank? }.map { |x| x.to_sym }

    services = []
    ShippingInformation.first.services.reject { |x| x.blank? }.map { |x| x.to_sym }.each do |s|
      next unless s == :pac
      serv = shipping.calcular(s)

      # if free_services.include? s
      #   serv.valor = 0.0
      # end

      services.push serv
    end

    optimized_services = []
    services.each_with_index do |s, i|
      optimized_services[i] = s
      optimized_services[i].valor = "R$ #{("%.2f" % (s.valor + extra_price)).to_s.gsub('.', '').ljust(3, '0')[0..-3]},#{("%.2f" % (s.valor + extra_price)).to_s.gsub('.', '').ljust(3, '0')[-2..-1]}"
      optimized_services[i].prazo_entrega = (s.prazo_entrega + 2).to_s.rjust(2, '0')
    end

    if optimized_services.map { |x| x.erro == '0' }.include?(false)
      respond_to do |format|
        format.json { render json: { success: false } }
      end
    else
      respond_to do |format|
        format.json { render json: { success: true, services: optimized_services } }
      end
    end

  end

  def set_shipping_method
    @cart = get_cart
    @success = false

    if @cart.update_attributes(shipping_hash: params['shipping'], shipping_price_in_cents: params['shipping']['price'].from_brazilian_currency)
      flash.now[:success] = "Forma de entrega selecionada com sucesso."
      @success = true
    else
      flash.now[:error] = "Ocorreu um erro ao adicionar a forma de entrega escolhida."
    end
  end
end
