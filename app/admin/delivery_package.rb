ActiveAdmin.register DeliveryPackage do
  menu parent: 'Frete', priority: 2

  config.filters = false
  config.batch_actions = false

  permit_params do
   permitted = [:name, :maximum_weight_in_grams, :width_in_centimeters, :height_in_centimeters, :depth_in_centimeters, :extra_price_in_cents, :maximum_product_amount, :position]
   permitted
  end

  index do
    column :name
    column :maximum_product_amount
    actions
  end

  form do |f|
    f.inputs "Detalhes Gerais", :class => 'inputs expandable in' do
      f.input :name
      f.input :active
      f.input :maximum_product_amount
      f.input :extra_price_in_cents, as: :string
    end
    f.inputs "Detalhes do Tamanho", :class => 'inputs expandable in' do
      f.input :width_in_centimeters
      f.input :height_in_centimeters
      f.input :depth_in_centimeters
      f.input :maximum_weight_in_grams
    end
    f.actions
  end

end
