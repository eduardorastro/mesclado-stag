class CpfValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    unless value.blank?
      unless CPF.valid?(value)
        record.errors[attribute] << (options[:message] || "não é válido")
      end
    end
  end

end
