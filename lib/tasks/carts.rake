namespace :carts do
  task clean: :environment do
    puts 'Starting Cleanup'
    carts = Cart.cleanup
    if carts
      puts "Cleared #{carts.length} carts."
    end
    puts 'Cleanup Ended'
  end
end
