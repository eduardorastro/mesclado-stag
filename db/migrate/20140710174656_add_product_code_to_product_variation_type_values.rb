class AddProductCodeToProductVariationTypeValues < ActiveRecord::Migration
  def change
    add_column :product_variation_type_values, :product_code, :string
  end
end
