class CreateAdvantages < ActiveRecord::Migration
  def change
    create_table :advantages do |t|
      t.references :promotion_profile, polymorphic: true
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :advantages, :deleted_at
    add_index :advantages, :promotion_profile_id, name: 'index_advantage_promo_profile_id'

  end
end
