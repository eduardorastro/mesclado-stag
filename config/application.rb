require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require "active_model/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

if Rails.env.development?
  require 'dotenv'
  Dotenv.load
end

module Mesclado
  class Application < Rails::Application

    config.generators do |g|
      g.assets false
      g.helper false
    end

    config.assets.initialize_on_precompile = false

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Brasilia'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '*.{rb,yml}').to_s]
    config.i18n.available_locales = [:en, :'pt-BR', :pt]
    config.i18n.default_locale = :'pt-BR'
    config.i18n.fallbacks = { :pt => :'pt-BR' }
    config.i18n.reload!

    # Load libs
    config.autoload_paths += %W(#{config.root}/lib)

    # Add extra assets
    # config.assets.paths << Rails.root.join("app", "assets", "svg")
    config.assets.precompile += %w( ie-spacer.gif gritter.png )

    # Precompile aditional assets
    # config.assets.precompile += %w( .eot .woff .ttf .svg)

    # Config paperclip
    config.paperclip_defaults = {
      :storage => :s3,
      :s3_credentials => {
        :bucket => ENV['AWS_BUCKET_NAME'],
        :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
        :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
      },
      :s3_host_name => ENV["AWS_HOST_NAME"],
      :url => ':s3_domain_url',
      :path => 'uploads/:class/:id/:attachment/:style.:extension',
      :s3_permissions => :private,
      :s3_protocol => 'https'
    }
  end
end

# require File.expand_path('../pre-env-initializers/load_aws_config.rb', __FILE__)
require File.expand_path('../pre-env-initializers/load_email_config.rb', __FILE__)
