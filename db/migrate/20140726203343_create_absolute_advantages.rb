class CreateAbsoluteAdvantages < ActiveRecord::Migration
  def change
    create_table :absolute_advantages do |t|
      t.references :advantage, index: true
      t.integer :amount_in_cents
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :absolute_advantages, :deleted_at
  end
end
