# encoding: UTF-8

module ActiveAdmin::MetaInfoHelper
  def meta_info_permitted_params
    [meta_info_attributes: [:id, :meta_title, :meta_description]]
  end

  def meta_inputs form
    form.inputs "Meta Data", :for => [:meta_info, form.object.meta_info || MetaInfo.new], :class => 'inputs expandable out' do |meta_form|
      meta_form.input :meta_title, hint: "Título da página dentro do site."
      meta_form.input :meta_description, as: :text, input_html: { class: 'small-textarea' }, hint: "Descrição da página dentro do site."
    end
  end
end
