class CreateArtistsKeywords < ActiveRecord::Migration
  def change
    create_table :artists_keywords, id: false do |t|
      t.references :keyword
      t.references :artist
    end

    add_index :artists_keywords, [:keyword_id, :artist_id]
  end
end
