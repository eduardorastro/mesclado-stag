class TransactionsMailer < ActionMailer::Base
  default from: "Mesclado <#{EMAIL_CONFIG[:user_name]}>"

  def new(transaction)
    set_variables transaction
    mail(subject: 'Mesclado - Pedido Recebido', to: @user.email)
  end

  def paid(transaction)
    set_variables transaction
    mail(subject: 'Mesclado - Pagamento Confirmado', to: @user.email)
  end

  def sent(transaction)
    set_variables transaction
    mail(subject: 'Mesclado - Pedido Enviado', to: @user.email)
  end

  def refused(transaction)
    set_variables transaction
    mail(subject: 'Mesclado - Pagamento Recusado', to: @user.email)
  end

  private

    def set_variables(transaction)
      @transaction = transaction
      @user = transaction.cart.user
      if @user.address.present?
        @address = @user.address
      end
    end
end
