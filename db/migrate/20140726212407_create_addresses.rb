class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :owner, polymorphic: true, index: true
      t.string :street
      t.string :street_number
      t.string :complement
      t.string :zip_code
      t.string :neighborhood
      t.string :city
      t.string :state
      t.string :country
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :addresses, :deleted_at
  end
end
