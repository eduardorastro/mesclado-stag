$ ->
  if $('#edit_user_registration').find('input[id*="zip_code"]').length > 0
    $('#edit_user_registration').find('input[id*="zip_code"]').keyup ->
      $this = $(this)
      $this.val $this.val().replace(/[^0-9]/g, '')
      zip_code = $this.val()
      if zip_code.length == 8
        window.start_loading()
        $this.focusout()
        $.ajax '/zip_code/' + zip_code,
          data: 'GET'
          dataType: 'json'
          complete: ->
            window.stop_loading()
          error: (jqXHR, textStatus, errorThrown) ->
            $.gritter.add
              class_name: 'gritter-error'
              title: 'Erro'
              text: 'Ocorreu algum erro de conexão. Por favor tente novamente.'
          success: (data, textStatus, jqXHR) ->
            if data.success == true
              for k, v of data.zip_info
                $this.parents('form:first').find("input[id*='#{k}']:first").attr('value': v)
            else
              $.gritter.add
                class_name: 'gritter-error'
                title: 'Erro'
                text: 'CEP não encontrado.'

      else if zip_code.length > 8
        $.gritter.add
          class_name: 'gritter-error'
          title: 'Erro'
          text: 'O CEP preenchido é inválido'

