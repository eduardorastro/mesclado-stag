$ ->
  $('form#new_promotion').find('#promotion_promotion_profile_input').find('select').change ->
    id = "#{$(this).val()}_profile"
    $(this).parents('form').find('.promotion_profile_inputs').each ->
      if $(this).hasClass('shown')
        $(this).hide 'fast', ->
          $(this).removeClass('shown')

    $(this).parents('form').find("##{id}").show 'fast',  ->
      $(this).addClass('shown')
