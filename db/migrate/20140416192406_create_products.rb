class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.boolean :active, default: true
      t.boolean :highlight, default: false
      t.string :product_code
      t.string :name, null: false
      t.string :sub_title
      t.text :description
      t.text :technical_details
      t.text :inspiration
      t.references :artist, index: true
      t.integer :position, default: 0
      t.integer :price_in_cents, null: false
      t.integer :gross_weight_in_grams
      t.integer :net_weight_in_grams
      t.date :new_from
      t.date :new_to
      t.string :slug
      t.integer :number_of_sells, default: 0
      t.integer :number_of_views, default: 0
      t.integer :number_of_shows, default: 0

      t.datetime :deleted_at

      t.timestamps
    end
    add_index :products, :active
    add_index :products, :highlight
    add_index :products, :position
    add_index :products, :deleted_at

    add_index :products, :slug
  end
end
