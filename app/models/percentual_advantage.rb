class PercentualAdvantage < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :advantage, inverse_of: :percentual_advantage

  validates :percentual_amount, numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 100 }, allow_nil: true

  def my_advantage
    "Desconto de #{self.percentual_amount}%"
  end

  def final_value value
    value * (100 - percentual_amount) / 100
  end

  def discount value
    value - final_value(value)
  end
end

