product_variation_id = undefined

ready = ->
  get_selected_product_variation()
  set_buy_link()

  $('.product-variation-photo.in').each ->
    src = $(this).attr('src')
    add_zoom $(this), src

  $('.product-variation-photo').load ->
    window.stop_loading()
    if $(this).hasClass('out')
      photo_in = $('.product-variation-photo.in')
      photo_out = $('.product-variation-photo.out')

      photo_in.addClass('out').removeClass('in')
      photo_out.removeClass('out').addClass('in')

  $('.value-icon').click ->
    unless $(this).hasClass('selected')
      type = $(this).parents('.variation-values:first').data('type')
      elem = $(this)

      set_variation(elem, type)

  $('.product-photo').mouseover ->
    $(this).addClass('big')

  $('.product-photo').mouseout ->
    $(this).removeClass('big')

  $('#variations-modal').find('.buy-button').click ->
    $(this).parents('.modal').modal('hide')


add_zoom = (elem, src) ->
  elem
    .wrap('<span style="display:inline-block"></span>')
    .css('display', 'block')
    .parent()
    .zoom
      url: src

set_variation = (value_elem, type) ->
  type_lines = $(".variation-values[data-type='#{type}']")
  type_lines.find('.value-icon.selected').removeClass('selected')

  value = value_elem.data('value')
  type_lines.find(".value-icon[data-value='#{value}']").addClass('selected')

  related = value_elem.data('related')
  # parent = value_elem.parents('.variation-line:first')

  for i in related
    unless i[0] == 0
      type_lines.each ->
        elem = $(this).parents('.variation-line:first').nextAll('.variation-line').find(".variation-values[data-type='#{i[0]}']")
        unless elem.length == 0
          elem.find('.value-icon').each ->
            if $.inArray($(this).data('value'), i[1]) > -1
              $(this).removeClass('inactive')
            else
              $(this).addClass('inactive')
            if $(this).hasClass('selected') and $(this).hasClass('inactive')
              $(this).parents('.variation-values:first').find('.value-icon').not('.inactive').eq(0).addClass('selected')
              $(this).removeClass('selected')


  relateds = []
  $('.variation-values').not('[data-type="0"]').find('.value-icon.selected').each ->
    for rel in $(this).data('related')
      if rel[0] == 0 or rel[0] == '0'
        relateds.push rel[1]

  sizes = _.intersection.apply(_,relateds)
  $(".variation-values[data-type='0']").find('.value-icon').each ->
    val = parseInt($(this).data('value'))

    if _.contains(sizes, val)
      $(this).removeClass('inactive')
    else
      $(this).addClass('inactive')
      if $(this).hasClass('selected')
        $(this).removeClass('selected')

  unless $(this).parents('.variation-values:first').data('type') == 0
    get_selected_product_variation()
    change_photo()

  set_buy_link()


change_photo = ->
  photo_in = $('.product-variation-photo.in')
  photo_out = $('.product-variation-photo.out')

  photo_in.parent().trigger('zoom.destroy')
  photo_in.unwrap()

  if $(".product-variation[data-product-variation-id=#{product_variation_id}]").data('url') == $('.product-variation-photo.out').attr 'src'

    photo_in.addClass('out').removeClass('in')
    photo_out.removeClass('out').addClass('in')

    src = $(this).attr('src')
    add_zoom photo_out, src
  else
    window.start_loading()

    src = $(".product-variation[data-product-variation-id=#{product_variation_id}]").data('url')
    add_zoom photo_out, src
    photo_out.attr 'src' : src


get_selected_product_variation = ->
  values = []
  $('.variation-values').each ->
    unless $(this).data('type') == 0
      values.push $(this).find('.value-icon.selected').data('value')

  $('.ajax-photos').find('.product-variation').each ->
    if $($(this).data('values')).not(values).length == 0 && $(values).not($(this).data('values')).length == 0
      product_variation_id = $(this).data('product-variation-id')

set_buy_link = ->
  if $('.buy-button-row').find('.buy-button').length > 0
    myObj = {}
    $('.variation-values').each ->
      myObj[$(this).data('type')] = $(this).find('.value-icon.selected').data('value')

    myObj['product_id'] = $('.variation-options').data('product')
    myObj['product_variation_id'] = product_variation_id
    myObj['redirect_to_cart'] = true

    params = $.param(myObj)

    old_url = $('.buy-button-row').find('.buy-button').attr('href')
    new_url = [old_url.split('?')[0], params].join('?')
    $('.buy-button-row').find('.buy-button').attr 'href' : new_url

$(document).ready(ready)
$(document).on('page:load', ready)
