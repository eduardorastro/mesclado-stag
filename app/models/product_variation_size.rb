class ProductVariationSize < ActiveRecord::Base
  acts_as_paranoid
  acts_as_list

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :deleted, -> { only_deleted }

  has_and_belongs_to_many :product_variation_type_values, join_table: 'product_variation_sizes_product_variation_type_values'

  has_attached_file :photo, :styles => { :icon => "51x51#" }, preserve_files: true
  validates_attachment :photo, presence: true, content_type: { content_type: ["image/jpeg", "image/jpg", "image/JPG", "image/JPEG", "image/gif", "image/png"] }


  has_attached_file :selected_photo, :styles => { :icon => "51x51#" }, preserve_files: true
  validates_attachment :selected_photo, presence: true, content_type: { content_type: ["image/jpeg", "image/jpg", "image/JPG", "image/JPEG", "image/gif", "image/png"] }

  def product_variation_type_values
    self.destroyed? ? super.with_deleted : super
  end

  def related_values
    response = self.product_variation_type_values.includes(:product_variation_type).group_by { |x| x.product_variation_type }

    new_response = {}
    response.each do |k, v|
      new_response[k.id] = v.sort_by { |x| x.position }.map { |x| x.id }
    end
    new_response
  end

  def related_values_for product
    self.related_values
  end

  validates_presence_of :name

  before_validation :set_values

  private

    def set_values
      if self.product_variation_type_values.blank?
        self.product_variation_type_values = ProductVariationTypeValue.all
      end
    end
end
