$ ->
  $('.amount-chooser').find('.amount-changer').click ->
    $this = $(this)
    amount = parseInt $this.parents('.amount-chooser:first').find('.amount-indicator').text()

    if $this.hasClass('plus')
      amount += 1
    else
      amount -= 1

    unless amount == 0
      url = $this.parents('.cart-item:first').data('edit-url')


      window.start_loading()

      $.ajax url,
        type: 'PUT'
        data:
          amount: amount
        complete: ->
          window.stop_loading()
        error: (jqXHR, textStatus, errorThrown) ->
          $.gritter.add
            class_name: 'gritter-error'
            title: 'Erro'
            text: 'Ocorreu algum erro de conexão. Por favor tente novamente.'
        success: (data, textStatus, jqXHR) ->
          $this.parents('.amount-chooser:first').find('.amount-indicator').text(amount)
          if amount == 1
            $this.parents('.amount-chooser:first').find('.minus').addClass('inactive')
          else
            $this.parents('.amount-chooser:first').find('.inactive').removeClass('inactive')


# ZIP CODE
$ ->
  $('.my-cart').find('.shipping-zip-code').find('.submit-button').click ->
    set_shipping $(this)
  $('#shipping-modal').find('.shipping-zip-code').find('.submit-button').click ->
    set_shipping $(this)

set_shipping = (elem) ->
  unless elem.parents('.float-label-input:first').find('input').val() == ''
    zip_code = elem.parents('.float-label-input:first').find('input').val().replace(/\./g, '').replace(/\-/g, '')

    if zip_code.length == 8
      window.start_loading()
      $.ajax '/calcular-frete',
        type: 'GET'
        data:
          zip_code: zip_code
        complete: ->
          window.stop_loading()
        error: (jqXHR, textStatus, errorThrown) ->
          $.gritter.add
            class_name: 'gritter-error'
            title: 'Erro'
            text: 'Ocorreu algum erro de conexão. Por favor tente novamente.'
        success: (data, textStatus, jqXHR) ->
          $('.shipping-method').hide 'fast', ->
            elem.remove()
          if data.success
            shipping_methods = []
            for service in data.services
              method = $('<div>').addClass('shipping-method')
              method.css 'display' : 'none'
              label = $('<label>').attr('for' : service.codigo)
              input = $('<input>').attr
                'name' : 'shipping-methods'
                'id' : service.codigo
                'type' : 'radio'
                'value' : service.type
                'data-price' : service.valor
                'data-prazo' : parseInt(service.prazo_entrega)
                'data-name' : service.nome

              span = $('<span>').text(service.valor)

              days = parseInt(service.prazo_entrega)
              label.text("#{service.nome} (#{service.prazo_entrega} " + ( if days == 1
                  "dia útil)"
                else
                  "dias úteis)"
              ))
              label.prepend(input)
              label.append(span)
              method.append(label)

              set_shipping_method(input, elem)

              $('.shipping-services').append(method)
              # $('.shipping-services').show('fast')
              # method.show 'fast'
            $('.shipping-services').find('input[type="radio"]:first').click()
            $('#shipping-modal').modal('hide')
          else
            $('.shipping-services').hide('fast')
            $.gritter.add
              class_name: 'gritter-error'
              title: 'Erro'
              text: 'Ocorreu algum erro.\nVerifique se o CEP foi preenchido corretamente.\nCaso o erro continue, tente novamente dentro de alguns instantes.'
    else
      $('.shipping-services').hide('fast')
      $.gritter.add
        class_name: 'gritter-error'
        title: 'Erro'
        text: 'Este CEP é inválido'


set_shipping_method = (elem, parent) ->
  elem.change ->
    window.start_loading()
    $.ajax '/carrinho/frete-selecionado',
      type: 'GET'
      data:
        shipping: elem.data()
      error: (jqXHR, textStatus, errorThrown) ->
        window.stop_loading()
        $.gritter.add
          class_name: 'gritter-error'
          title: 'Erro'
          text: 'Ocorreu algum erro de conexão. Por favor tente novamente.'
      success: (data, textStatus, jqXHR) ->
        ok = 'ok'
        if parent.parents('#shipping-modal:first').length > 0
          window.start_loading()




# COUPON
$ ->
  $('.my-cart').find('.promotion-code').find('.submit-button').click ->
    unless $(this).parents('.float-label-input:first').find('input').val() == ''
      coupon_code = $(this).parents('.float-label-input:first').find('input').val()

      window.start_loading()
      $.ajax '/cupons/adicionar',
        type: 'GET'
        data:
          coupon_code: coupon_code
        complete: ->
          window.stop_loading()
        error: (jqXHR, textStatus, errorThrown) ->
          $.gritter.add
            class_name: 'gritter-error'
            title: 'Erro'
            text: 'Ocorreu algum erro de conexão. Por favor tente novamente.'
        # success: (data, textStatus, jqXHR) ->

$ ->
  url = window.location.href
  params = url.split('?')[1]
  unless params == undefined
    params = params.split('&')
    for i of params
      if params[i].split('=')[0] == 'login_modal'and params[i].split('=')[1] == 'true'
        $('#register-header-button').click()


