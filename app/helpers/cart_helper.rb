module CartHelper
  def get_cart
    @my_cart ||= current_cart
  end

  def current_cart
    cart = Cart.where(id: cookies[:cart]).first
    if user_signed_in?
      if cart.nil? or cart.closed?
        cart = current_user.active_cart
      else
        unless cart.user_id == current_user.id
          if cart.user_id.nil?
            current_user.active_cart.merge cart
            cart = current_user.active_cart
          else
            cart = current_user.active_cart
          end
        end
      end
    else
      if cart.nil? or cart.user_id.present?
        cart = Cart.create
      end
    end

    cookies[:cart] = { value: cart.id, expires: 12.hours.from_now }
    cart
  end

  def check_promotions_for_cart cart
    valid_for_cart = []
    Promotion.valid.no_coupon.cart.each do |p|
      if p.valid_for? cart
        valid_for_cart.push p
      end
    end

    return unless valid_for_cart.any?

    p = cart.promotion
    valid_for_cart.sort_by! { |x| cart.promotion = x; cart.discount }
    cart.promotion = p

    if cart.promotion.nil?
      cart.promotion = valid_for_cart.last
      cart.update_promotions
      cart.save(validate: false)

      flash.now[:success] = "Uma promoção para o seu carrinho estava disponível e foi aplicada automaticamente."
    elsif cart.promotion.advantage_profile.present? and valid_for_cart.last.advantage_profile.present? and cart.promotion.advantage_profile.discount(cart.total_price_no_discount) < valid_for_cart.last.advantage_profile.discount(cart.total_price_no_discount)
      cart.promotion = valid_for_cart.last
      cart.coupon = nil
      cart.update_promotions
      cart.save(validate: false)

      flash.now[:success] = "Uma promoção melhor que a que estava aplicada ao seu carrinho está disponível e foi aplicada automaticamente."
    end
  end
end
