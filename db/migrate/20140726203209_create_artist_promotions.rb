class CreateArtistPromotions < ActiveRecord::Migration
  def change
    create_table :artist_promotions do |t|
      t.references :promotion, index: true
      t.references :artist, index: true
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :artist_promotions, :deleted_at
  end
end
