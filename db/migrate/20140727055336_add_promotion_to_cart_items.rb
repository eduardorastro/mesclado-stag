class AddPromotionToCartItems < ActiveRecord::Migration
  def change
    add_reference :cart_items, :promotion, index: true
    add_reference :cart_items, :coupon, index: true
    add_reference :carts, :coupon, index: true
  end
end
