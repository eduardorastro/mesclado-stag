module WithZipCode
  extend ActiveSupport::Concern

  included do
    validates :zip_code, zip_code: true
    before_validation :strip_zip_code, :fill_fields_by_zip_code

    def formatted_zip_code
      if self.zip_code.present?
        BrZipCode.formatted(self.zip_code).gsub('.', '')
      end
    end
  end

  private

    def strip_zip_code
      self.zip_code = BrZipCode.strip(self.zip_code) unless zip_code
      .blank?
    end

    def fill_fields_by_zip_code
      if self.zip_code.present? and self.zip_code_changed?

        address_vars = [:street, :city, :state, :neighborhood]
        responding_methods = address_vars.reject { |x| not self.respond_to? x }

        if responding_methods.present? and responding_methods.map { |x| self[x].nil? }.include? true

          zip_info = BrZipCode.get_address(self.zip_code)

          responding_methods.each do |address_var|
            if zip_info[address_var]
              begin
                self[address_var] ||= zip_info[address_var].encode("UTF-8")
              rescue Exception => e
                conversion = { "\xED".force_encoding("ASCII-8BIT") => "í", "\xF3".force_encoding("ASCII-8BIT") => "ó" }
                conversion.each do |k, v|
                  if zip_info[address_var].chars.include? k
                    zip_info[address_var].gsub!(k, v)
                  end
                end
                self[address_var] ||= zip_info[address_var].encode("UTF-8")
              end
            end
          end
        end
      end
    end
end
