class CartItem < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :product_variation
  belongs_to :cart, inverse_of: :cart_items
  belongs_to :product_variation_size
  belongs_to :promotion, inverse_of: :cart_items
  belongs_to :coupon, inverse_of: :cart_items

  serialize :values_ids
  serialize :values_names
  serialize :variation_values_hash
  serialize :product_variation_hash
  serialize :product_hash
  serialize :artist_hash

  validates_presence_of :amount, :unit_price_in_cents, :product_variation
  validates :amount, numericality: { only_integer: true }
  validates :unit_price_in_cents, numericality: { only_integer: true }
  validates :discount_in_cents, numericality: { only_integer: true }

  def total_price
    self.amount * self.total_unit_price
  end

  def total_price_with_coupon
    self.amount * self.unit_price_with_coupon
  end

  def total_unit_price
    self.unit_price_in_cents - self.discount_in_cents
  end

  def total_price_no_discount
    self.amount * self.unit_price_in_cents
  end

  def unit_price_with_coupon
    if self.with_discount_from_coupon?
      self.cart.coupon.promotion.promotion_profile.advantage.advantage_profile.final_value(self.total_unit_price)
    else
      self.total_unit_price
    end
  end

  def with_discount_from_coupon?
    if self.cart.coupon.present?
      items = self.cart.coupon.coupon_items(self.cart)
      unless items.nil?
        if items.include? self
          return true
        end
      end
    end
    return false
  end

  def add_amount value
    self.update_attributes(amount: self.amount + value)
  end

  def product_name
    self.product_hash['name'] if self.product_hash.present?
  end

  def product_id
    self.product_hash['id'] if self.product_hash.present?
  end

  def artist_name
    self.artist_hash['name'] if self.artist_hash.present?
  end

  def artist_id
    self.artist_hash['id'] if self.artist_hash.present?
  end

  def photo
    self.product_variation_hash['photo_url'] if self.product_variation_hash.present?
  end

  def name
    "#{self.product_variation_hash['name']} - #{self.product_variation_size.name}"
  end

  def update_promotions
    self.discount_in_cents = self.product_variation.product.discount
    self.promotion = self.product_variation.product.best_promotion
    self.save(validate: false)
  end

  def check_if_promotion_is_valid?
    if self.promotion.present?
      product = self.product_variation.product
      artist = product.artist
      unless self.promotion.valid_for?(product) or self.promotion.valid_for?(artist)
        self.promotion = nil
        self.coupon = nil
        self.discount_in_cents = 0
        self.save(validate: false)
      end
    end
  end

  before_validation :set_numbers
  after_validation :set_extra_variables, if: -> { self.cart.payment_date.nil? }
  after_commit :suicide_if_no_amount, if: -> { self.amount == 0 }

  private

    def set_extra_variables
      pv = self.product_variation

      # Set hash of the product variation values attributes
      variation_values = pv.product_variation_type_values
      self.values_ids = variation_values.map { |x| x.id }
      self.values_names = variation_values.map { |x| x.name }
      self.variation_values_hash = variation_values.map { |x| x.attributes.merge('photo_url' => x.photo.url(:icon), 'selected_photo_url' => x.selected_photo.url(:icon)) }

      # Set the name of the size
      self.size = self.product_variation_size.name

      # Set hash of the product attributes
      self.product_hash = pv.product.attributes.merge('photo_url' => pv.product.photo.url(:standard))

      # Set hash of the artist attributes
      self.artist_hash = pv.product.artist.attributes

      # Set hash of the product_variation attributes
      self.product_variation_hash = pv.attributes.merge('photo_url' => pv.photo.url(:thumb), 'name' => pv.name)
    end

    def suicide
      self.destroy
    end

    def set_numbers
      self.unit_price_in_cents ||= 0
      self.discount_in_cents ||= 0
    end
end
