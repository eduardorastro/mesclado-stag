class CreateProductVariationTypes < ActiveRecord::Migration
  def change
    create_table :product_variation_types do |t|
      t.boolean :active, default: true
      t.string :name
      t.datetime :deleted_at

      t.integer :position, default: 0

      t.boolean :customize_photos, default: false

      t.timestamps
    end
    add_index :product_variation_types, :active
    add_index :product_variation_types, :deleted_at
  end
end
