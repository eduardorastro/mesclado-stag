root = exports ? this

$ ->
  root.initTinyMCE('tinymce')

$ ->
  add_tinymce_to_new_textareas $('.has_many_container')

add_tinymce_to_new_textareas = (elem) ->
  elem.find('.button.has_many_add').click ->
    root.delay 10, ->
      elem.find('.inputs:last').find('.tinymce').each ->
        $(this).addClass('temp-tinymce')
      root.initTinyMCE('temp-tinymce')
      $('.temp-tinymce').removeClass('.temp-tinymce')
      add_tinymce_to_new_textareas elem.find('.inputs:last').find('.has_many_container')

