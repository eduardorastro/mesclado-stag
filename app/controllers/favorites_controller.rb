class FavoritesController < ApplicationController
  respond_to :js, only: [:create, :destroy]

  def index
    if user_signed_in?
      @products = current_user.products.ready
    else
      redirect_to root_path
    end
  end

  def create
    if user_signed_in?
      @product = Product.find params[:product_id]
      if @product.present?
        @user = current_user
        @favorite = Favorite.where(user_id: @user.id, product_id: @product.id).first
        if @favorite.present?
          flash.now[:error] = "Produto já está nos seus favoritos."
        else
          @favorite = Favorite.new(user_id: @user.id, product_id: @product.id)
          if @favorite.save
            flash.now[:success] = "Produto adicionado aos favoritos com sucesso."
          else
            flash.now[:error] = "Ocorreu algum erro. Por favor tente novamente."
          end
        end
      else
        flash.now[:error] = "Produto não encontrado."
      end
    else
      flash.now[:error] = "Somente usuários cadastrados e logados podem adicionar produtos aos seus favoritos."
    end
  end

  def destroy
    @favorite = Favorite.find params[:id]
    if @favorite.present?
      @product = @favorite.product
      if @favorite.destroy
        flash.now[:success] = "Produto retirado dos favoritos com sucesso."
      else
        flash.now[:error] = "Ocorreu algum erro"
      end
    else
      flash.now[:error] = "Ocorreu algum erro"
    end
  end
end
