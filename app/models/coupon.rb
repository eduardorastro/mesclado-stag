class Coupon < ActiveRecord::Base
  acts_as_paranoid

  scope :active, -> { joins(:promotion).where('(promotions.start_date >= ? OR promotions.start_date IS NULL) AND (promotions.end_date <= ? OR promotions.end_date IS NULL)', DateTime.now, DateTime.now).where(promotions: { active: true }).uniq.where('promotions.uses < coupons.maximum_uses OR coupons.maximum_uses IS NULL OR promotions.uses IS NULL') }

  belongs_to :promotion, inverse_of: :coupon

  has_many :cart_items, inverse_of: :coupon
  has_many :carts, inverse_of: :coupon

  validates_presence_of :code
  validates_uniqueness_of :code

  def name
    self.code
  end

  def maximum_uses
    super || 0
  end

  def valid_for_cart? cart
    self.promotion.valid_for? cart
    # profile = self.promotion.promotion_profile
    # profile.valid_for_cart? cart

    # if profile.class == ProductPromotion
    #   if cart.cart_items.map { |x| x.product_id }.include? profile.product_id
    #     true
    #   else
    #     false
    #   end
    # elsif profile.class == ArtistPromotion
    #   if cart.cart_items.map { |x| x.artist_id }.include? profile.artist_id
    #     true
    #   else
    #     false
    #   end
    # else
    #   if profile.minimum_cart_total_price_in_cents.present?
    #     if profile.minimum_cart_total_price_in_cents <= cart.total_price_no_shipping_no_discount
    #       true
    #     else
    #       false
    #     end
    #   else
    #     true
    #   end
    # end
  end

  def discount_for_cart cart
    return 0 unless self.promotion.present?

    profile = self.promotion.promotion_profile

    items = self.coupon_items(cart)
    if items.nil?
      if profile.advantage.advantage_profile.present?
        profile.advantage.advantage_profile.discount(cart.total_price_no_shipping_no_coupon)
      else
        0
      end
    else
      if profile.advantage.advantage_profile.present?
        price = items.first.total_unit_price
        profile.advantage.advantage_profile.discount(price * items.length)
      else
        0
      end
    end
  end

  def coupon_items cart
    return nil unless self.promotion.present?

    profile = self.promotion.promotion_profile
    if profile.class == ProductPromotion
      cart.cart_items.select { |x| x.product_id == profile.product_id }
    elsif profile.class == ArtistPromotion
      cart.cart_items.select { |x| x.artist_id == profile.artist_id }
    else
      nil
    end
  end
end
