class ProductsController < ApplicationController
  def index
    if params[:tag].present?
      @products = Product.where(id: Keyword.includes(:products).where(name: params[:tag]).map { |x| x.products.ready.public_ones.includes(:artist) }.flatten.map { |x| x.id }).order('highlight DESC, RANDOM()').includes(:artist)
    else
      @products = Product.where(id: Product.ready.public_ones.pluck(:id)).order('highlight DESC, RANDOM()').includes(:artist)
    end

    if params[:price].present?
      @products = @products.order("price_in_cents #{params[:price]}")
    end

    @keywords = Keyword.includes(:products).all.reject { |x| x.products.ready.public_ones.empty? }
  end

  def page
    if request.xhr?
      products = Product.find params[:items]
      render partial: 'products/page', locals: { list: products }
    end
  end

  def show
    @product = Product.find(params[:id])
    @product_variation = @product.product_variation

    @other_products = Product.where(id: Product.ready.public_ones.where.not(id: @product.id).pluck(:id)).limit(4).order('RANDOM()')
  end

  def search
    @artists = Artist.active.public_ones.kinda(params[:search])
    @products = Product.ready.public_ones.kinda(params[:search])

    @results = PgSearch.multisearch(params[:search]).group_by { |x| x.searchable_type }

    # binding.pry

    if @results['Artist'].present?
      @artists += @results['Artist'].map { |x| x.searchable }.select { |x| x.public? }
    end

    if @results['Product'].present?
      @products += @results['Product'].map { |x| x.searchable }.reject { |x| x.product_variation.nil? or !(x.public?) }
    end

    @artists = Artist.where(id: @artists.map { |x| x.id }).uniq
    @products = Product.where(id: @products.map { |x| x.id }).uniq

    render partial: 'products/search'
  end
end
