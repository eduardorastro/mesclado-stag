ActiveAdmin.register User do
  menu parent: 'Usuários', priority: 1

  actions :index, :show

  filter :first_name
  filter :last_name
  filter :email

  index do
    column :formatted_id
    column :name
    column :email
    actions
  end

  show do |user|
    panel 'Detalhes Gerais' do
      attributes_table_for user do
        row :formatted_id
        row :name
        row :email
        row :formatted_cpf
        row :formatted_phone
      end
    end

    panel 'Endereço' do
      attributes_table_for user.address || Address.new do
        [:street, :street_number, :complement, :neighborhood, :zip_code, :city, :state].each do |attr|
          row attr do
            if user.address.present?
              user.address.send(attr)
            end
          end
        end
      end
    end
  end

  csv do
    column 'ID' do |u|
      u.formatted_id
    end
    column 'Código' do |u|
      ''
    end
    column 'Nome' do |u|
      u.name
    end
    column 'Fantasia' do |u|
      ''
    end
    translations = ['Endereco', 'Numero', 'Complemento', 'Bairro', 'CEP', 'Cidade', 'Estado']
    [:street, :street_number, :complement, :neighborhood, :zip_code, :city, :state].each_with_index do |address_var, i|
      column translations[i] do |u|
        if u.address.present?
          u.address.send(address_var)
        end
      end
    end
    column 'Contatos' do |u|
      ''
    end
    column 'Fone' do |u|
      if u.phone.present?
        "#{u.phone.area_code} #{u.phone.number}"
      end
    end
    column 'Fax' do |u|
      ''
    end
    column 'Celular' do |u|
      ''
    end
    column 'E-mail' do |u|
      u.email
    end
    column 'Website' do |u|
      ''
    end
    column 'Tipo_pessoa' do |u|
      'Pessoa Física'
    end
    column 'CNPJ_CPF' do |u|
      u.formatted_cpf
    end
    column 'IE_RG' do |u|
      ''
    end
    column 'IE_isento' do |u|
      'N'
    end
    column 'Situação' do |u|
      'Ativo'
    end
    column 'Obsevacoes' do
      ''
    end
    column 'Estado_civil' do
      ''
    end
    column 'Profissao' do
      ''
    end
    column 'Sexo' do
      ''
    end
    column 'Data_nascimento' do
      ''
    end
    column 'Naturalidade' do
      ''
    end
    column 'Nome_pai' do
      ''
    end
    column 'CPF_pai' do
      ''
    end
    column 'Nome_mãe' do
      ''
    end
    column 'CPF_mãe' do
      ''
    end
    column 'Segmento' do
      ''
    end
    column 'Vendedor' do
      ''
    end
    column 'Tipo_Contrato' do
      ''
    end
    column 'Email_para_envio_NFe' do
      ''
    end
  end
end
