class Product < ActiveRecord::Base
  include PgSearch
  multisearchable :against => [:name], if: lambda { |record| record.active }
  pg_search_scope :kinda, :against => [:name], using: { trigram: { any_word: true, treshold: 0.2 } }

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :reserved, :history], sequence_separator: ''

  acts_as_paranoid

  # scope :ready, -> { joins(product_variations: :product_variation_photos).select('DISTINCT(products.id), products.artist_id, products.slug, products.deleted_at, products.name, products.active, products.price_in_cents').active }
  scope :ready, -> { joins(product_variations: :product_variation_photos).uniq.active }

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :deleted, -> { only_deleted }
  scope :public_ones, -> { where(public: true) }
  scope :private_ones, -> { where(public: false) }

  scope :highlight, -> { where(highlight: true) }

  attr_accessor :string_keywords

  belongs_to :artist, -> { with_deleted }

  has_many :product_variations, dependent: :destroy
  accepts_nested_attributes_for :product_variations, allow_destroy: true

  has_many :product_promotions, inverse_of: :product

  has_and_belongs_to_many :keywords
  accepts_nested_attributes_for :keywords

  has_one :meta_info, as: :owner, dependent: :destroy
  accepts_nested_attributes_for :meta_info, allow_destroy: true

  has_and_belongs_to_many :product_variation_types, join_table: 'products_product_variation_types', inverse_of: :products

  has_many :product_photos, dependent: :destroy
  accepts_nested_attributes_for :product_photos, allow_destroy: true

  validates_presence_of :name, :artist, :price_in_cents, :product_photos, :product_code

  before_validation :set_variation_types
  # after_initialize :set_string_keywords

  after_save :create_keywords, :change_children_active_state
  after_commit :create_product_variations, on: :create
  after_commit :update_children_values_if_needed, on: :update

  before_restore :restore_children

  def ready?
    self.product_variations.ready.any?
  end

  def total_value
    self.price_in_cents - self.discount
  end

  def discount
    if possible_promotions.present?
      best_promotion.advantage_profile.discount(self.price_in_cents)
    else
      0
    end
  end

  def best_promotion
    self.possible_promotions.sort_by { |x| x.advantage.advantage_profile.discount(self.price_in_cents) }.last
  end

  def possible_promotions
    product_ids = product_promotions.pluck(:id)
    artist_ids = artist_promotions.pluck(:id)

    Promotion.where(id: (product_ids + artist_ids)).uniq
  end

  def product_promotions
    Promotion.valid.joins(:product_promotion).where('product_promotions.product_id = ? OR product_promotions.all_products = ?', self.id, true).includes(:coupon).where(coupons: { promotion_id: nil })
  end

  def artist_promotions
    Promotion.joins(:artist_promotion).where(artist_promotions: { artist_id: self.artist_id }).includes(:coupon).where(coupons: { promotion_id: nil })
  end

  def coupon_promotions

  end

  def price_in_cents=(value)
    if value.class.to_s.downcase == 'string'
      value = value.gsub(/\D/, '').to_i
    end
    super
  end

  def photo
    first = self.product_photos.limit(1).first

    if first.nil?
      nil
    else
      first.photo
    end
  end

  def product_variations
    self.destroyed? ? super.with_deleted : super
  end

  def product_variation
    vars = self.product_variations.ready.limit(1)

    vars.where(default: true).first || vars.first
  end

  def product_photos
    self.destroyed? ? super.with_deleted : super
  end

  def meta_info
    self.destroyed? ? MetaInfo.with_deleted.where(owner_id: self.id, owner_type: self.class).first : super
  end

  def keywords_to_s
    self.keywords.map { |x| "#{x.name}" }.join(' ')
  end

  def name
    "#{super}#{self.active ? '' : ' (Desativado)'}"
  end

  def active=(value)
    value = active_logic(value)
    super
  end

  def active_logic value = nil
    if value == "0"
      value = false
    elsif value == "1"
      value = true
    end
    value = self.active if value.nil?
    if self.artist.present?
      value and self.artist.active
    else
      value
    end
  end

  def update_children_values_if_needed
    children_values = self.product_variations.map { |x| x.product_variation_type_values }.flatten.uniq
    values = self.product_variation_types.map { |x| x.product_variation_type_values }.flatten.uniq

    values_to_remove = children_values - values
    values_to_add = values - children_values

    unless values_to_remove.empty? and values_to_add.empty?
      self.product_variations.each do |pv|
        pv.really_destroy!
      end
      ProductVariation.create_all_variations_from_product(self.id)
    end
  end

  def possible_values
    grouped_values = ProductVariationTypeValue.joins(:product_variations).where(product_variations: { id: self.product_variations.ready.map { |x| x.id } }).uniq.includes(:product_variation_type).includes(:product_variation_sizes).uniq.sort_by { |x| x.product_variation_type.position }.group_by { |x| x.product_variation_type }

    grouped_values.each do |k, v|
      grouped_values[k].sort_by! { |x| x.position }
    end

    sizes = grouped_values.map { |k, v| v.map { |x| x.product_variation_sizes } }.flatten.uniq.sort_by! { |x| x.position }
    grouped_values['sizes'] = sizes

    grouped_values
  end

  private

    def set_string_keywords
      self.string_keywords || self.keywords_to_s
    end

    def change_children_active_state
      if self.active_changed?
        self.product_variations.each do |p|
          p.update_column(:active, p.active_logic)
        end
      end
    end

    def restore_children
      ProductPhoto.restore self.product_photos.only_deleted.map { |x| x.id }
      ProductVariation.restore self.product_variations.only_deleted.map { |x| x.id }
      MetaInfo.restore self.meta_info.id
    end

    def create_keywords
      self.string_keywords = self.string_keywords || self.keywords_to_s
      self.string_keywords.downcase!

      if self.string_keywords != self.keywords_to_s
        words = self.string_keywords.split(' ')
        key_names = self.keywords.map { |x| x.name }
        key_names.each_with_index do |kn|
          unless words.include? kn
            self.keywords.delete Keyword.unscoped.find_by_name(kn)
          else
            words.delete(kn)
          end
        end
        words.each do |w|
          kw = Keyword.unscoped.find_by_name(w)
          if kw.present?
            if kw.destroyed?
              self.keywords.push Keyword.restore(kw.id)
            else
              self.keywords.push kw
            end
          else
            self.keywords.create(name: w)
          end
        end
      end
    end

    def should_generate_new_friendly_id?
      new_record? || slug.blank?
    end

    def set_variation_types
      if self.product_variation_types.empty?
        self.product_variation_types = ProductVariationType.all
      end
    end

    def create_product_variations
      ProductVariation.create_all_variations_from_product(self.id)
    end

    def slug_candidates
      [
        :name_no_spaces,
        [:name_no_spaces, :sub_title],
        [:name_no_spaces, :sub_title, :product_code]
      ]
    end

    def name_no_spaces
      self.name.gsub(' ', '')
    end

end
