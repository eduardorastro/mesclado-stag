include ActiveAdmin::MetaInfoHelper
include ActiveAdmin::BannerFormHelper

ActiveAdmin.register Artist do
  menu parent: 'Produtos', priority: 1

  scope :active
  scope :inactive
  # scope :deleted

  config.batch_actions = false

  permit_params do
    params = [:active, :public, :name, :sub_title, :description, :slug, :city, :state, :country, :facebook_url, :instagram_url, artist_photos_attributes: [:id, :photo, :position, :default, :_destroy]]
    params += banner_permitted_params
    params += meta_info_permitted_params
    params
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs "Detalhes Gerais", :class => 'inputs expandable in' do
      f.input :active
      f.input :public
      f.input :name
      f.input :sub_title
      f.input :description, input_html: { class: "tinymce small-textarea" }
      f.input :slug, hint: "O valor preenchido aqui, definirá a URL final do artista. Por exemplo, se o valor for 'bruno', será criada a URL mesclado.com.br/bruno. Esse valor é gerado automaticamente a partir do nome, caso fique em branco."
    end

    f.inputs "Endereço", :class => 'inputs expandable in' do
      f.input :country, as: :select, collection: ActionView::Helpers::FormOptionsHelper::COUNTRIES
      f.input :state
      f.input :city
    end

    if f.object.artist_photos.empty?
      f.object.artist_photos = [ArtistPhoto.new(default: true)]
    end

    f.has_many :artist_photos, heading: "Fotos", allow_destroy: true, sortable: :position, new_record: "Adicionar Foto", :class => 'inputs expandable in' do |photo_form|
      photo_form.input :photo, image_preview: true, size_hint: true
      photo_form.input :default
    end

    banner_form f, multiple: false, always_active: true, no_click: true

    f.inputs "Mídias Sociais", :class => 'inputs expandable in' do
      f.input :facebook_url
      f.input :instagram_url
    end

    meta_inputs f

    f.actions
  end

  index as: :block do |artist|
    div :for => artist do
      # resource_selection_cell artist
      div :class => 'left-image' do
        if artist.photo
          image_tag artist.photo.url(:thumb)
        end
      end
      div :class => 'content' do
        h2 auto_link artist.name
        div :class => 'sub-title' do
          simple_format artist.sub_title
        end
        div :class => 'actions' do
          if artist.destroyed?
            links = link_to 'Restaurar', restore_admin_artist_path(artist), data: { method: :put, confirm: "Você tem certeza?\nTodas as estampas deste artista também serão restauradas e vão aparecer no site." }
            links += link_to 'Remover Permanentemente', definetly_destroy_admin_artist_path(artist), data: { method: :delete, confirm: "Você tem certeza?\nTodas as estampas deste artista também serão removidas permanentemente." }
          else
            links = link_to 'Visualizar', admin_artist_path(artist)
            links += link_to 'Editar', edit_admin_artist_path(artist)
            links += link_to 'Remover', admin_artist_path(artist), data: { method: :delete, confirm: "Você tem certeza?\nTodas as estampas deste artista também serão removidas." }
          end
        end
      end
    end
  end

  show do
    div :class => 'artist-header-image' do
      image_tag artist.header_photo.url(:big)
    end
    div :class => 'artist-header' do
      div :class => 'left-image' do
        image_tag artist.photo.url(:thumb)
      end
      div :class => 'title' do
        h2 auto_link artist.name
      end
      div :class => 'sub-title' do
        simple_format artist.sub_title
      end
      div :class => 'description' do
        simple_format artist.description
      end
    end

    if artist.products.present?
      div :class => 'artist-products' do
        h2 'Estampas'
        div :class => 'produts-photos' do
          html = ''
          artist.products.each do |prod|
            html += link_to admin_product_path(prod) do
              html1 = image_tag(prod.photo.url(:standard))
              html1 += content_tag(:p, prod.name)
              html1
            end
          end
          html.html_safe
        end
      end
    end

    attributes_table do
      row :city
      row :state
      row :country
      row :facebook_url
      row :instagram_url
      row :number_of_views
      row :number_of_sells
    end
  end

  filter :name

  member_action :restore, method: :put do
    Artist.restore(params[:id])
    flash[:notice] = "Artista recuperado com sucesso."
    redirect_to admin_artists_path
  end

  member_action :definetly_destroy, method: :delete do
    Artist.unscoped.find(params[:id]).really_destroy!
    redirect_to admin_artists_path
  end

  controller do
    def index
      params[:scope] ||= 'active'
      super
    end
  end

end
