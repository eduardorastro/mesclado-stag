class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.string :name, null: false
      t.integer :total_times, default: 0

      t.datetime :deleted_at

      t.timestamps
    end
    add_index :keywords, :total_times
    add_index :keywords, :deleted_at
  end
end
