class CreateKeywordsProducts < ActiveRecord::Migration
  def change
    create_table :keywords_products, id: false do |t|
      t.references :keyword
      t.references :product
    end

    add_index :keywords_products, [:keyword_id, :product_id]
  end
end
