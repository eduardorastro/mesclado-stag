class CreateProductVariationSizes < ActiveRecord::Migration
  def change
    create_table :product_variation_sizes do |t|
      t.boolean :active, default: true
      t.string :name
      t.attachment :photo
      t.attachment :selected_photo

      t.integer :position, default: 0

      t.datetime :deleted_at

      t.timestamps
    end

    add_index :product_variation_sizes, :active
    add_index :product_variation_sizes, :deleted_at
  end
end
