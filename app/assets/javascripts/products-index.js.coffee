ready = ->
  $('.organize-products').click ->
    container = $(this)
    $('.organize-products').each ->
      if !container.is($(this))
        $(this).find('.organizer-bt').find('i').removeClass('fa-rotate-180')
        $(this).find('.organizer-popup').hide('fast')

    $(this).find('.organizer-bt').find('i').addClass('fa-rotate-180')
    $(this).find('.organizer-popup').show(300)

  $(document).mouseup (e) ->
    container = $('.organizer-popup')
    bt = $('.organizer-bt')
    i = bt.find('i')
    if !container.is(e.target) && container.has(e.target).length == 0 && !bt.is(e.target) && !i.is(e.target)
      container.parents('.organize-products').find('.organizer-bt').find('i').removeClass('fa-rotate-180')
      container.hide(300)



  $('.search-keywords').find('input').keyup ->
    popup = $(this).parents('.organizer-popup:first')
    value = $(this).val().toLowerCase()
    popup.find('a').each ->
      if $(this).data('key').toLowerCase().indexOf(value) >= 0
        $(this).show()
      else
        $(this).hide()


$(document).ready(ready)
$(document).on('page:load', ready)
