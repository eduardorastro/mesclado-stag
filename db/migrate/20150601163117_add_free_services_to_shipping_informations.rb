class AddFreeServicesToShippingInformations < ActiveRecord::Migration
  def change
    add_column :shipping_informations, :free_services, :text
  end
end
