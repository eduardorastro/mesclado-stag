class CreateCartPromotions < ActiveRecord::Migration
  def change
    create_table :cart_promotions do |t|
      t.references :promotion, index: true
      t.integer :minimum_cart_total_price_in_cents, default: 0
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :cart_promotions, :deleted_at
  end
end
