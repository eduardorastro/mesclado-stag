class CreateBannerPhotos < ActiveRecord::Migration
  def change
    create_table :banner_photos do |t|
      t.references :banner, index: true
      t.integer :position, default: 0
      t.attachment :photo
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :banner_photos, :position
    add_index :banner_photos, :deleted_at
  end
end
