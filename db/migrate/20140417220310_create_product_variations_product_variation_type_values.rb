class CreateProductVariationsProductVariationTypeValues < ActiveRecord::Migration
  def change
    create_table :product_variations_product_variation_type_values, id: false do |t|
      t.references :product_variation
      t.references :product_variation_type_value
    end

    add_index :product_variations_product_variation_type_values, [:product_variation_id, :product_variation_type_value_id], unique: true, name: 'index_prod_var_prod_var_type_value'
  end
end
