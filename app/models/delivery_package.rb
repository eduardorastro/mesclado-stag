class DeliveryPackage < ActiveRecord::Base
  acts_as_paranoid

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :deleted, -> { only_deleted }

  validates_presence_of :maximum_weight_in_grams, :width_in_centimeters, :height_in_centimeters, :depth_in_centimeters, :maximum_product_amount

  def extra_price_in_cents=(value)
    if value.class.to_s.downcase == 'string'
      value = value.gsub(/\D/, '').to_i
    end
    super
  end

  def self.divide_in_packages n
    pending_items = n

    final_packages = []

    while pending_items > 0
      package = find_smallest_package_for pending_items
      final_packages.push package
      pending_items -= package.maximum_product_amount
    end

    final_packages
  end

  private

    def self.find_smallest_package_for n
      response = nil

      packages = DeliveryPackage.order('maximum_product_amount ASC')

      packages.each do |package|
        max = package.maximum_product_amount
        if n <= max
          response = package
          break
        end
      end

      response ||= packages.last

      response
    end

end
