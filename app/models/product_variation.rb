class ProductVariation < ActiveRecord::Base
  acts_as_paranoid

  # scope :ready, -> { joins(:product_variation_photos).select('DISTINCT(product_variations.id), product_variations.product_id, product_variations.active, product_variations.deleted_at').active }
  scope :ready, -> { joins(:product_variation_photos).uniq.active }
  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :deleted, -> { only_deleted }

  scope :recent, -> { where(id: ProductVariation.ready.where(product_id: Product.ready.public_ones.pluck(:id)).to_a.uniq { |p| p.product_id }.map { |x| x.id }).includes(:product_variation_photos).includes(product: :artist).includes(:product).order('products.created_at DESC') }

  belongs_to :product, -> { with_deleted }

  has_many :product_variation_photos, dependent: :destroy
  accepts_nested_attributes_for :product_variation_photos, allow_destroy: true

  has_and_belongs_to_many :product_variation_type_values, -> { order('id ASC') }, join_table: 'product_variations_product_variation_type_values', inverse_of: :product_variations

  validates_presence_of :product

  has_many :cart_items

  after_save :set_as_only_default, :if => -> { self.default == true }

  before_restore :restore_children

  def name
    n = self.product.name + self.product_variation_type_values.map { |x| " - #{x.name}" }.join('').gsub(' (Desativado)', '')
    "#{n}#{' (Desativado)' unless self.active}"
  end

  def ready?
    self.active and self.product_variation_photos.present?
  end

  def photo
    if self.product_variation_photos.present?
      self.product_variation_photos.limit(1).first.photo
    else
      nil
    end
  end

  def price_in_cents
    self.product.total_value + self.product_variation_type_values.map { |x| x.price_in_cents }.sum
  end

  def price_in_cents_no_discount
    self.product.price_in_cents + self.product_variation_type_values.map { |x| x.price_in_cents }.sum
  end

  def product_variation_photos
    self.destroyed? ? super.with_deleted : super
  end

  def product_variation_type_values
    self.destroyed? ? super.with_deleted : super
  end

  def product_variation_type_values_active?
    if self.product_variation_type_values.present?
      response = true
      self.product_variation_type_values.map { |x| x.active }.each do |b|
        response = (response and b)
      end
      response
    else
      true
    end
  end

  def product_code
    ([self.product.product_code] + self.product_variation_type_values.map { |x| x.product_code }).join('')
  end

  def active=(value)
    value = active_logic(value)
    super
  end

  def active_logic value = nil
    if value == "0"
      value = false
    elsif value == "1"
      value = true
    end
    value = self.active if value.nil?
    if self.product.present?
      value and self.product.active and self.product_variation_type_values_active?
    else
      value and self.product_variation_type_values_active?
    end
  end

  def self.create_all_variations_from_product(product_id)
    product = Product.find product_id
    if product.present?
      types = product.product_variation_types
      if types.present?
        values = types.map { |x| x.product_variation_type_values }
        if values.present?
          combinations = values.first.product(*values[1..-1])
          create_combinations product, combinations
        end
      end
    end
  end

  def self.create_all_variations_from_value(product_variation_type_value_id)
    variation_value = ProductVariationTypeValue.find product_variation_type_value_id

    if variation_value.present?
      products = variation_value.product_variation_type.products
      if variation_value.keep_old_photos == true and products.present?
        products.map { |x| x.product_variations }.flatten.uniq.each do |pv|
          pv.product_variation_type_values.push variation_value
          pv.save
        end
      else
        products.each do |prod|
          values = prod.product_variation_types.reject { |x| x.id == variation_value.product_variation_type.id }.map { |x| x.product_variation_type_values }
          combinations = [variation_value].product(*values)

          create_combinations prod, combinations
        end
      end
    end
  end

  def can_have_value? type, size
    response = true
    values = self.product_variation_type_values.sort_by { |x| x.product_variation_type.position }.group_by { |x| x.product_variation_type.id }

    values.each do |k, v|
      values[k] = v.map { |x| x.related_values_for(self.product) }
    end

    values.each do |vid, related_ones|
      if vid > type
        related_ones.each do |related|
          if related[type].present? and !related[type].include? size
            response = false
          end
        end
      end
    end

    response
  end

  def selected_value? type, value
    unless type == 0
      self.product_variation_type_values.where(product_variation_type_id: type).first.id == value
    else
      nil
    end
  end

  private

    def self.create_combinations product, combinations
      combinations.each do |comb|
        comb_active = true
        comb.each do |comb_value|
          comb_active = comb_active and comb_value.active
        end
        pv = product.product_variations.new(active: (product.active and comb_active))
        pv.product_variation_type_values.push comb
        pv.save
      end
    end

    def restore_children
      ProductVariationPhoto.restore self.product_variation_photos.only_deleted.map { |x| x.id }
      ProductVariationTypeValue.restore self.product_variation_type_values.only_deleted.map { |x| x.id }
    end

    def set_as_only_default
      self.product.product_variations.where(default: true).where.not(id: self.id).each do |pv|
        pv.update_column(:default, false)
      end
    end
end
