ActiveAdmin.register Promotion do
  actions :index, :show, :new, :create, :edit, :update, :destroy

  config.filters = false

  permit_params do
   permitted = [:start_date_date, :start_date_time_hour, :start_date_time_minute, :end_date_date, :end_date_time_hour, :end_date_time_minute, :active, :promotion_profile]
   advantage_attributes = [:free_shipping, :amount_in_cents, :percentual_amount]
   permitted += [product_promotion_attributes: [:id, :product_id, :all_products, advantage_attributes: advantage_attributes]]
   permitted += [artist_promotion_attributes: [:id, :artist_id, advantage_attributes: advantage_attributes]]
   permitted += [cart_promotion_attributes: [:id, :minimum_cart_total_price_in_cents, advantage_attributes: advantage_attributes]]
   permitted += [promotion_profile_advantage: advantage_attributes]
   permitted += [coupon_attributes: [:id, :code, :maximum_uses]]
   # permitted << :other if resource.something?
   permitted
  end

  form do |f|
    f.object.active = true if f.object.active.nil?
    f.semantic_errors *f.object.errors.keys

    if f.object.new_record?
      params[:promotion] ||= {}
      params[:promotion][:promotion_profile] ||= f.object.promotion_profile.class.to_s.underscore
      f.inputs "Detalhes Gerais" do
        f.input :active
        f.input :start_date, as: :just_datetime_picker
        f.input :end_date, as: :just_datetime_picker
        f.input :promotion_profile, as: :select, collection: options_for_select({'Produto' => :product_promotion, 'Artist' => :artist_promotion, 'Carrinho' => :cart_promotion }, params[:promotion][:promotion_profile])
      end

      f.inputs "Promoção de Produto", :for => [:product_promotion, f.object.product_promotion || ProductPromotion.new], class: "inputs promotion_profile_inputs expandable in #{'shown' if params[:promotion][:promotion_profile] == 'product_promotion' }", id: 'product_promotion_profile' do |product_promotion_form|
        product_promotion_form.input :product
        product_promotion_form.input :all_products

        # advantage_helper product_promotion_form
      end

      f.inputs "Promoção de Artista", :for => [:artist_promotion, f.object.artist_promotion || ArtistPromotion.new], class: "inputs promotion_profile_inputs expandable in #{'shown' if params[:promotion][:promotion_profile] == 'artist_promotion' }", id: 'artist_promotion_profile' do |artist_promotion_form|
        artist_promotion_form.input :artist

        # advantage_helper artist_promotion_form
      end

      f.inputs "Promoção de Carrinho", :for => [:cart_promotion, f.object.cart_promotion || CartPromotion.new], class: "inputs promotion_profile_inputs expandable in #{'shown' if params[:promotion][:promotion_profile] == 'cart_promotion' }", id: 'cart_promotion_profile' do |cart_promotion_form|
        cart_promotion_form.input :minimum_cart_total_price_in_cents, as: :string

        # advantage_helper cart_promotion_form
      end
    else
      f.inputs "Detalhes Gerais" do
        f.input :active
        f.input :start_date, as: :just_datetime_picker
        f.input :end_date, as: :just_datetime_picker
        # f.input :promotion_profile, as: :select, collection: options_for_select({'Produto' => :product_promotion, 'Artist' => :artist_promotion, 'Carrinho' => :cart_promotion }, params[:promotion][:promotion_profile])
      end
    end


    f.inputs 'Vantagem', :for => [:promotion_profile_advantage, f.object.promotion_profile.present? ? f.object.advantage : Advantage.new], class: 'inputs expandable in' do |af|
      af.input :free_shipping
      af.input :amount_in_cents, as: :string
      af.input :percentual_amount, as: :string
    end

    f.inputs 'Cupom (opcional)', :for => [:coupon, f.object.coupon || Coupon.new], class: 'inputs expandable out' do |coupon_form|
      coupon_form.input :code
      coupon_form.input :maximum_uses
    end
    f.actions
  end

  index do
    id_column
    column :active
    column :valid_period
    # column :type
    # column :relative_item
    # column 'Frete Grátis?' do |p|
    #   p.advantage.free_shipping ? 'Sim' : 'Não'
    # end
    column :advantage do |p|
      # p.advantage_profile.my_advantage if p.advantage_profile.present?
      p.my_advantage
    end
    column :coupon
    # column :coupon_max_use
    # column :uses
    actions
  end

  show do |p|
    attributes_table do
      row :id
      row :active
      row :valid_period
      # row :type
      # row :relative_item
      row :uses
    end

    attributes_table do
      row 'Frete Grátis?' do
        p.advantage.free_shipping ? 'Sim' : 'Não'
      end
      row 'Vantagem' do
        p.my_advantage
        # p.advantage_profile.my_advantage
      end
    end

    if p.promotion_profile.is_a? CartPromotion
      attributes_table do
        row 'Tipo' do
          'Promoção de Carrinho'
        end
        row 'Valor Mínimo do Carrinho' do
          p.promotion_profile.minimum_cart_total_price_in_cents.to_brazilian_currency
        end
      end
    end

    if p.promotion_profile.is_a? ArtistPromotion
      attributes_table do
        row 'Tipo' do
          'Promoção de Artista'
        end
        row 'Artista da Promoção' do
          link_to p.promotion_profile.artist.name, [:admin, p.promotion_profile.artist]
        end
      end
    end

    if p.promotion_profile.is_a? ProductPromotion
      attributes_table do
        row 'Tipo' do
          'Promoção de Produto'
        end
        row 'Produto da Promoção' do
          if p.promotion_profile.present? and p.promotion_profile.product.present?
            link_to p.promotion_profile.product.name, [:admin, p.promotion_profile.product]
          elsif p.promotion_profile.all_products
            'Todos os Produtos'
          end
        end
      end
    end

    if p.coupon.present?
      attributes_table do
        row :code do
          p.coupon.code
        end
        row :coupon_max_use do
          p.coupon.maximum_uses
        end
      end
    end
  end

  controller do

    def create
      before_create_update

      create! do |success, failure|
        failure.html do
          profile_underscore = resource.promotion_profile.class.to_s.underscore
          params[:promotion][:promotion_profile] = profile_underscore.blank? ? profile : profile_underscore
          render :new
        end

        success.html do
          redirect_to admin_promotions_path
        end
      end
    end

    def update
      before_create_update

      update! do |success, failure|
        failure.html do
          profile_underscore = resource.promotion_profile.class.to_s.underscore
          params[:promotion][:promotion_profile] = profile_underscore.blank? ? profile : profile_underscore
          render :edit
        end

        success.html do
          redirect_to admin_promotions_path
        end
      end
    end

    private

      def before_create_update
        profile = params[:promotion].delete(:promotion_profile)

        [:product_promotion_attributes, :artist_promotion_attributes, :cart_promotion_attributes].each do |attrs|
          unless "#{profile}_attributes" == attrs.to_s
            params[:promotion].delete(attrs)
          else
            params[:promotion][attrs][:advantage_attributes] = params[:promotion][:promotion_profile_advantage]
            # [:absolute_advantage_attributes, :percentual_advantage_attributes].each do |attrs1|
            #   hash = params[:promotion][attrs][:advantage_attributes][attrs1]
            #   hash.each do |k, v|
            #     if v.blank? or (k == 'amount_in_cents' and v.from_brazilian_currency == 0)
            #       hash.delete(k)
            #     else
            #       if k == 'amount_in_cents'
            #         hash[k] = v.from_brazilian_currency
            #       else
            #         hash[k] = v.to_i
            #       end
            #     end
            #   end
            #   if hash.blank?
            #     params[:promotion][attrs][:advantage_attributes].delete(attrs1)
            #   end
            # end
          end
        end
        params[:promotion][:coupon_attributes].each do |k, v|
          if v.blank?
            params[:promotion][:coupon_attributes].delete(k)
          end
        end
        if params[:promotion][:coupon_attributes].blank?
          params[:promotion].delete(:coupon_attributes)
        end
      end

  end

end

# def advantage_helper f
#   f.inputs 'Vantagem', class: 'expandable in' do
#     f.semantic_fields_for :advantage_attributes do |advantage_form|
#       html = advantage_form.semantic_fields_for :absolute_advantage_attributes do |absolute_form|
#         absolute_form.input :amount_in_cents, label: 'Desconto absoluto'
#       end.html_safe
#       html += advantage_form.semantic_fields_for :percentual_advantage_attributes do |percentual_form|
#         percentual_form.input :percentual_amount, label: 'Desconto percentual'
#       end.html_safe
#       html
#     end
#   end
# end
