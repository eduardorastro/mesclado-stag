include ActiveAdmin::MetaInfoHelper
include ActiveAdmin::BannerFormHelper

ActiveAdmin.register About do
  menu parent: "Páginas Institucionais", priority: 5, label: "Sobre"

  permit_params do
    permitted = []
    permitted << [about_strips_attributes: [:strip_title, :strip_text, :id, :photo, :position]]
    permitted << [testimonials_attributes: [:id, :test_title, :test_author, :test_from, :position]]
    permitted += banner_permitted_params
    permitted += meta_info_permitted_params
    permitted
  end

  form do |f|
    f.has_many :testimonials, heading: 'Depoimentos', allow_destroy: true, sortable: :position, new_record: 'Adicionar Depoimento', :class => 'inputs expandable in' do |test_form|
      test_form.input :test_title
      test_form.input :test_author
      test_form.input :test_from
    end

    f.has_many :about_strips, heading: 'Faixas', allow_destroy: true, sortable: :position, new_record: 'Adicionar Faixa', :class => 'inputs expandable in' do |strip_form|
      strip_form.input :strip_title
      strip_form.input :strip_text, input_html: { class: 'tinymce small-textarea' }
      strip_form.input :photo, image_preview: true, size_hint: true
    end

    banner_form f, multiple: false, always_active: true, no_click: true

    meta_inputs f


    f.actions
  end

  controller do
    def index
      about = About.first
      if about.nil?
        about = About.create
      end
      redirect_to edit_admin_about_path(about)
    end
  end

end
