ActiveAdmin.register Transaction do
  permit_params do
   permitted = [cart_attributes: [:tracking_code]]
   permitted
  end

  filter :status, as: :select, collection: { 'Sem Status' => 0, 'Aguardando Pagamento' => 1, 'Em análise' => 2, 'Pago' => 3, 'Disponível' => 4, 'Em disputa' => 5, 'Devolvido' => 6, 'Cancelado' => 7 }
  filter :created_at
  filter :pagseguro_transaction_reference

  actions :index, :show, :edit, :update

  # config.filters = false

  form do |f|
    f.inputs 'Tracking Code', :for => [:cart, f.object.cart], class: 'inputs' do |c_form|
      c_form.input :tracking_code
    end
    f.actions
  end

  index do
    column :counter do |t|
      if t.cart.present?
        t.cart.payments_counter
      end
    end
    column :pagseguro_transaction_reference
    column :status_translation
    column :user do |t|
      if t.cart.present?
        t.cart.user_hash['name']
      end
    end
    column :total_value do |t|
      if t.pagseguro_transaction_hash.present?
        ((t.pagseguro_transaction_hash.gross_amount.to_f * 100).to_i).to_brazilian_currency
      end
    end
    # column :net_value do |t|
    #   if t.pagseguro_transaction_hash.present?
    #     ((t.pagseguro_transaction_hash.net_amount.to_f * 100).to_i).to_brazilian_currency
    #   end
    # end
    # column :cart_value do |t|
    #   if t.cart.present?
    #     t.cart.total_price.to_brazilian_currency
    #   end
    # end
    column :tracking_code do |t|
      if t.cart.present?
        t.cart.tracking_code
      end
    end
    # column :created_at
    # column :updated_at
    actions
  end

  show do |t|
    panel 'Detalhes do Usuário' do
      attributes_table_for t do
        # row :status_translation
        row 'Nome' do
          if t.cart.present?
            t.cart.user_hash['name']
          end
        end
        row 'E-mail' do
          if t.cart.present?
            t.cart.user_hash['email']
          end
        end
        row :cpf do
          if t.cart.present? and t.cart.user.present?
            t.cart.user.formatted_cpf
          end
        end
        row :phone do
          if t.pagseguro_transaction_hash.present?
            p = t.pagseguro_transaction_hash.sender.phone
            "#{p.area_code} #{p.number}"
          end
        end
        [:street, :number, :complement, :district, :postal_code, :city, :state].each do |attr|
          row attr do
            if t.pagseguro_transaction_hash.present? and
               t.pagseguro_transaction_hash.shipping.present? and
               t.pagseguro_transaction_hash.shipping.address.present?
              t.pagseguro_transaction_hash.shipping.address.send(attr)
            end
          end
        end
      end
    end

    panel 'Informações de Pagamento' do
      attributes_table_for t do
        row :status_translation
        row :pagseguro_code do
          if t.pagseguro_transaction_hash.present?
            t.pagseguro_transaction_hash.code
          end
        end
        row :payment_method do
          if t.pagseguro_transaction_hash.present?
            t.pagseguro_transaction_hash.payment_method.description
          end
        end
        row :total_value do
          if t.pagseguro_transaction_hash.present?
            (t.pagseguro_transaction_hash.gross_amount*100).to_i.to_brazilian_currency
          end
        end
        row :net_value do
          if t.pagseguro_transaction_hash.present?
            ((t.pagseguro_transaction_hash.net_amount.to_f * 100).to_i).to_brazilian_currency
          end
        end
        row :created_at
        row :updated_at
      end
    end

    panel 'Informações de Frete' do
      attributes_table_for t do
        row 'Método de Entrega' do
          t.cart.shipping_hash['name']
        end
        row 'Prazo de Entrega' do
          "#{t.cart.shipping_hash['prazo']} dias"
        end
        row 'Preço de Entrega' do
          t.cart.shipping_price_in_cents.to_brazilian_currency
        end
      end
    end

    panel 'Informações do Pedido' do
      attributes_table_for t do
        if t.pagseguro_transaction_hash.present?
          t.pagseguro_transaction_hash.items.each do |item|
          # [t.pagseguro_transaction_hash.items.first, t.pagseguro_transaction_hash.items.first].each do |item|
            row :item_name do
              item.description
            end
            row :item_artist do
              CartItem.find(item.id).artist_name
            end
            row :item_price do
              (item.amount.to_f*100).to_i.to_brazilian_currency
            end
            row :item_amount do
              item.quantity
            end
          end
        end
      end
    end

    panel 'Promoções Utilizadas' do
      if t.cart.all_promotions.any?
        t.cart.all_promotions.reject { |x| x.nil? }.each do |p|
          attributes_table_for t.cart do
            row 'Vantagem da Promoção' do
              p.my_advantage
            end
            row 'Tipo de Promoção' do
              if p.promotion_profile.is_a? CartPromotion
                'Promoção de Carrinho'
              elsif p.promotion_profile.is_a? ProductPromotion
                'Promoção de Produto'
              elsif p.promotion_profile.is_a? Artist
                'Promoção de Artistas'
              end
            end
            row 'Promoção aplicada a' do
              if p.promotion_profile.is_a? CartPromotion
                'Carrinho'
              elsif p.promotion_profile.is_a? ProductPromotion
                if p.promotion_profile.product.present?
                  link_to p.promotion_profile.product.name, [:admin, p.promotion_profile.product]
                elsif p.promotion_profile.all_products
                  'Todos os produtos'
                end
              elsif p.promotion_profile.is_a? Artist
                link_to p.promotion_profile.artist.name, [:admin, p.promotion_profile.artist]
              end
            end
          end
        end
      else
        h6 'Nenhuma promoção foi utilizada nesse carrinho.'
      end
    end
  end

  controller do
    def update
      code = params[:transaction][:cart_attributes][:tracking_code]
      @transaction = Transaction.find params[:id]
      if code.blank?
        flash[:error] = "Tracking code não pode ficar em branco."
        render :edit
      else
        if @transaction.cart.update_column(:tracking_code, code)
          # TransactionsMailer.sent(@transaction).deliver
          flash[:notice] = "Tracking code atualizado com sucesso."
          redirect_to admin_transactions_path
        end
      end
    end
  end
end
