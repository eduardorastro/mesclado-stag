class AbsoluteAdvantage < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :advantage, inverse_of: :absolute_advantage

  validates :amount_in_cents, numericality: { only_integer: true, greater_than_or_equal_to: 1 }, allow_nil: true

  def my_advantage
    if self.amount_in_cents.present?
      "Desconto de #{self.amount_in_cents.to_brazilian_currency}"
    end
  end

  def final_value value
    unless value.nil? or amount_in_cents.nil?
      value - amount_in_cents
    else
      value
    end
  end

  def discount value
    if value.present? and amount_in_cents.present?
      value - final_value(value)
    else
      0
    end
  end
end
