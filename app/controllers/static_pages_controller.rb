class StaticPagesController < ApplicationController
  def show
    @static_page = StaticPage.find params[:id]
    @banner = @static_page.banner

    if @static_page == StaticPage.where(name: 'Home').first
      @artists = Artist.active.public_ones.order('RANDOM()').limit(6)
      @products = Product.highlight.where(id: Product.ready.public_ones.pluck(:id)).includes(:artist).order('RANDOM()')

      @product_variations = ProductVariation.ready.where(default: true).where(product_id: @products.map { |x| x.id }).includes(:product_variation_photos).includes(product: :artist).includes(:product).sample(10)

      if @products.length < 9
        @products = Product.where(id: Product.ready.public_ones.pluck(:id)).includes(:artist).order('RANDOM()')
      end

      @keywords = Keyword.includes(:products).all.reject { |x| x.products.ready.public_ones.empty? }

      # @most_selled_products = Product.order('number_of_sells DESC')
      # @most_selled_color_products = ProductVariation.ready.where(default: true).where(product_id: @most_selled_products.pluck(:id)).includes(:product_variation_photos).includes(product: :artist).includes(:product).order('products.number_of_sells DESC').limit(10)

      # @most_recent_products = Product.where(id: Product.ready.public_ones.pluck(:id)).order('created_at DESC')

      @most_recent_product_variations = ProductVariation.ready.where(default: true).recent.limit(15)

      unless Rails.env.development?
        @instagram_photos = HTTParty.get("https://api.instagram.com/v1/tags/sejaMesclado/media/recent?count=10&access_token=#{ENV['INSTAGRAM_ACCESS_TOKEN']}")['data']
      end

      if params[:reset_password_token].present?
        @reset_user = User.find(params[:user_id])
        @reset_user.reset_password_token = params[:reset_password_token]
      end

      render 'home'
    elsif @static_page == StaticPage.where(name: 'FAQ').first
      render 'faq'
    end
  end

  def about
    @about = About.first
    @banner = @about.banner
  end

  def pos
  end
end
