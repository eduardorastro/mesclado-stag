root = exports ? this

$ ->
  $('input[id*=_in_cents]').each ->
    pricify_input($(this))

$ ->
  $('.button.has_many_add').click ->
    parent = $(this).parents('.has_many_container')
    root.delay 10, ->
      parent.find('.inputs:last').find('input[id*=_in_cents]').each ->
        pricify_input($(this))

pricify_input = (elem) ->
  turn_to_price(elem)
  elem.keyup ->
    turn_to_price(elem)

turn_to_price = (elem) ->
  numbers = elem.val().replace(/\D/g, '')
  if numbers.length < 3
    numbers = '000' + numbers

  numbers = clean_left_zeros(numbers)

  numbers = "R$ #{numbers.slice(0, -2)},#{numbers.slice(-2)}"
  elem.val(numbers)

clean_left_zeros = (numbers) ->
  if numbers.length > 3 and numbers[0] == '0'
    return clean_left_zeros(numbers.slice(1))
  else
    return numbers
