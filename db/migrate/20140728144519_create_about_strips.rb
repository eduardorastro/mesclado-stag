class CreateAboutStrips < ActiveRecord::Migration
  def change
    create_table :about_strips do |t|
      t.attachment :photo
      t.references :about, index: true
      t.string :strip_title
      t.text :strip_text
      t.integer :position

      t.timestamps
    end
  end
end
