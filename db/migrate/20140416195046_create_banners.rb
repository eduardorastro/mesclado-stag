class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.references :owner, index: true, polymorphic: true
      t.boolean :active, default: true
      t.string :url
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :banners, :active
    add_index :banners, :deleted_at
  end
end
