class CouponsController < ApplicationController
  respond_to :js

  def add
    @cart = get_cart

    @coupon = Coupon.find_by_code params[:coupon_code]
    if @coupon
      if @coupon.valid_for_cart? @cart
        @cart.coupon = @coupon
        @cart.promotion = @coupon.promotion
        if @cart.save
          @cart.update_promotions
          flash.now[:success] = "Cupom adicionado com sucesso."
        else
          flash.now[:error] = "Ocorreu algum erro. Por favor tente novamente."
        end
      else
        flash.now[:error] = "Este código não é válido para o seu carrinho de compras."
      end
    else
      flash.now[:error] = "Código do cupom inválido"
    end
  end
end
