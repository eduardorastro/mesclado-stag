class Testimonial < ActiveRecord::Base
  belongs_to :about

  validates_presence_of :test_title, :test_author
end
