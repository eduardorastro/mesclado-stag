class CreateProductPromotions < ActiveRecord::Migration
  def change
    create_table :product_promotions do |t|
      t.references :promotion, index: true
      t.references :product, index: true
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :product_promotions, :deleted_at
  end
end
