class CreatePos < ActiveRecord::Migration
  def change
    create_table :pos do |t|
      t.string :name
      t.attachment :photo
      t.text :description
      t.string :address
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :pos, :deleted_at
  end
end
