include ActiveAdmin::MetaInfoHelper

ActiveAdmin.register Product do
  menu parent: 'Produtos', priority: 4

  config.batch_actions = false

  scope :ready
  scope :active
  scope :inactive
  # scope :deleted

  permit_params do
    params = [:active, :public, :highlight, :product_code, :name, :sub_title, :description, :technical_details, :inspiration, :position, :price_in_cents, :gross_weight_in_grams, :net_weight_in_grams, :new_from, :new_to, :slug, :artist_id, :string_keywords, product_variation_type_ids: []]
    params += [product_photos_attributes: [:id, :photo, :_destroy]]
    # params += [product_variations: [:id, :active, :product_variation_code, :default, :position, :_destroy, product_variation_photos: [:id, :photo, :default, :_destroy]]]
    params += meta_info_permitted_params
  end

  form do |f|
    f.actions

    f.semantic_errors *f.object.errors.keys

    f.object.string_keywords ||= f.object.keywords_to_s
    f.object.price_in_cents ||= 7000

    f.inputs "Detalhes Gerais", :class => 'inputs expandable in' do
      f.input :active, hint: "#{'O artista está desativado. Mesmo marcando como ativo, a estampa continuará inativa.' if (f.object.artist.present? and f.object.artist.active == false)}"
      f.input :public
      f.input :highlight
      f.input :name
      f.input :product_code
      f.input :artist
      f.input :price_in_cents, as: :string
      f.input :product_variation_types, as: :select, multiple: true, hint: 'Deixe em branco caso a estampa possua todas as características.'
      f.input :string_keywords, as: :string
      f.input :slug
    end

    if f.object.product_photos.empty?
      f.object.product_photos = [ProductPhoto.new]
    end
    f.has_many :product_photos, heading: false, allow_destroy: false, sortable: false, new_record: false, :class => 'inputs no-remove' do |photo_form|
      photo_form.input :photo, image_preview: true, size_hint: true
    end

    f.inputs "Informações Extras", :class => 'inputs expandable out' do
      # f.input :gross_weight_in_grams
      # f.input :net_weight_in_grams
      f.input :new_from, as: :datepicker
      f.input :new_to, as: :datepicker
    end

    f.inputs "Detalhamento da Estampa", :class => 'inputs expandable in' do
      # f.input :description, input_html: { class: "tinymce small-textarea" }
      f.input :technical_details, input_html: { class: "tinymce small-textarea" }
      f.input :inspiration, input_html: { class: "tinymce small-textarea" }
    end

    meta_inputs f

    f.actions
  end

  filter :name, as: :string, label: 'Nome da Estampa'
  filter :artist_name_cont, as: :string, label: 'Nome do Artista'

  index do
    # selectable_column
    column :product_photos do |p|
      if p.photo
        image_tag p.photo.url(:standard)
      end
    end
    column 'Aparecendo' do |p|
      p.ready? ? status_tag( "yes", :ok ) : status_tag( "no" )
    end
    column :active
    column :name
    column :artist
    column :product_variations do |p|
      link_to 'Gerenciar', admin_product_product_variations_path(p)
    end
    column '' do |product|
      if product.destroyed?
        links = link_to 'Restaurar', restore_admin_product_path(product), data: { method: :put }
        links += link_to 'Remover permanentemente', definetly_destroy_admin_product_path(product), data: { method: :delete, confirm: 'Você tem certeza?' }
      else
        links = link_to 'Visualizar', admin_product_path(product)
        links += link_to 'Editar', edit_admin_product_path(product)
        links += link_to 'Remover', admin_product_path(product), data: { method: :delete, confirm: "Você tem certeza?" }
      end
    end
  end

  member_action :restore, method: :put do
    Product.restore(params[:id])
    flash[:notice] = "Artista recuperado com sucesso."
    redirect_to admin_products_path
  end

  member_action :definetly_destroy, method: :delete do
    Product.unscoped.find(params[:id]).really_destroy!
    redirect_to admin_products_path
  end

  controller do
    def index
      params[:scope] ||= 'active'
      super
    end

    def update
      update! { admin_products_path }
    end
  end
end
