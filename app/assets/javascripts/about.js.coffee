$ ->
  if $('#testimonials').find('.testimonial').length > 1
    $('#testimonials').owlCarousel
      items: 1
      navSpeed: 500
      autoplay: true
      autoplayTimeout: 4000
      autoplaySpeed: 500
      nav: false
      dots: false
      loop: true
      responsiveRefreshRate: 10
      center: true

    $('.carousel-control').click ->
      $('#testimonials').trigger($(this).data('slide') + '.owl.carousel')
