class NewslettersController < ApplicationController
  respond_to :js

  def create
    @newsletter = Newsletter.new newsletter_params

    if @newsletter.save
      flash.now[:success] = "Cadastro realizado com sucesso."
    else
      flash.now[:error] = @newsletter.errors.full_messages.join("\n")
    end
  end

  def newsletter_params
    params.require(:newsletter).permit(:email)
  end
end
