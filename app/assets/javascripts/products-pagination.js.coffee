counter = undefined
total_pages = undefined
loading = undefined

ready = ->
  counter = $('.page-container').length
  total_pages = parseInt $('.items-pagination-container').data('total-pages')
  if counter < total_pages and counter < 3
    unless $('body').hasClass('mobile-browser')
      $('.items-pagination-container').waypoint ->
        add_extra($(this))
      , offset: ->
        100
  else
    $('.see-all-link').show()
  $('.more-items-link').click ->
    unless $(this).text() == 'Carregando...'
      add_extra($(this))

  if $('body').hasClass('mobile-browser')
    $('.more-items-link').show()



add_extra = (elem) ->
  return if loading
  loading = true

  $this = elem
  current_container = $('.page-container:last')
  current_page = parseInt current_container.data('page')
  clicked_page = counter

  ajax_url = $('.items-pagination-container').data('url')

  if clicked_page != current_page
    if $(".page-container[data-page='#{clicked_page}']").length > 0
      slide(current_page, clicked_page, current_container, $(".page-container[data-page='#{clicked_page}']"))
    else
      items = $('.items-ajax-reference').find('.items-slice').eq(clicked_page).find('.item').map ->
        $(this).data('id')

      items = items.get()

      if counter%3 == 0 and counter >= 3
        $('.more-items-link').hide()
        $('.small-loading').show()
      else
        $('.small-loading').slideDown('fast')

      # window.start_loading()

      $.ajax ajax_url,
        type: 'GET'
        dataType: 'html'
        data:
          items: items
          page: clicked_page
        complete: ->
          window.stop_loading()
        error: (jqXHR, textStatus, errorThrown) ->
          $.gritter.add
            class_name: 'gritter-error'
            title: 'Erro'
            text: 'Ocorreu algum erro de conexão. Por favor tente novamente.'
        success: (data, textStatus, jqXHR) ->
          counter+= 1
          loading = false

          new_container = $(data)
          $this.waypoint('destroy')

          $('.small-loading').slideUp('fast')
          $('.more-items-link').text('Carregar mais')

          unless counter%3 == 0 or $('body').hasClass('mobile-browser')
            $('.more-items-link').slideUp()

          if counter == total_pages
            $('.more-items-link').slideUp()

          current_container.after(new_container)

          # $('html, body').animate
          #   scrollTop: new_container.offset().top - 100
          # , 50

          if counter < total_pages and !(counter%3 == 0) and not ($('body').hasClass('mobile-browser'))
            new_container.waypoint ->
              add_extra($(this))
            , offset: ->
              100

          if (counter%3==0 and total_pages > counter) or ($('body').hasClass('mobile-browser'))
            $('.more-items-link').slideDown('fast')

          if counter == total_pages
            $('.more-items-link').slideUp('fast')
            $('.see-all-link').slideDown('fast')




          # slide(current_page, clicked_page, current_container, new_container)


slide = (current_page, clicked_page, current_container, new_container) ->
  current_container.removeClass('active') if current_container.hasClass('active')
  new_container.addClass('active') unless new_container.hasClass('active')

  $('.items-pagination').find(".page-button[data-page='#{clicked_page}']").addClass('selected')
  $('.items-pagination').find(".page-button[data-page='#{current_page}']").removeClass('selected')

  if clicked_page > current_page
    new_container.css 'left' : '150%'
  else
    new_container.css 'left' : '-50%'

  if clicked_page > current_page
    current_container.animate 'left' : '-50%', 'slow'
  else
    current_container.animate 'left' : '150%', 'slow'

  new_container.animate 'left' : '50%', 'slow'








$(document).ready(ready)
$(document).on('page:load', ready)
