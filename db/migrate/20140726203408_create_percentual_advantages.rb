class CreatePercentualAdvantages < ActiveRecord::Migration
  def change
    create_table :percentual_advantages do |t|
      t.references :advantage, index: true
      t.integer :percentual_amount
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :percentual_advantages, :deleted_at
  end
end
