class ShippingInformation < ActiveRecord::Base
  include WithZipCode

  serialize :services
  serialize :free_services
end
