class TransactionsController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: :notification


  def index
    if user_signed_in?
      @carts = current_user.closed_carts
    else
      flash.now[:error] = "Você precisa estar logado para acessar essa página."
      redirect_to root_path
    end
  end

  def new


      if user_signed_in?

        @cart = get_cart
        if @cart.shipping_hash.blank?
          flash[:error] = "Antes de comprar, por favor preencha o seu CEP e clique em calcular frete."
          redirect_to cart_path(@cart, no_shipping: true)
        else

          @session_id = (PagSeguro::Session.create).id

          puts "---------------------> #{@session_id}"

          @cart_id = @cart.id
          @total_price = @cart.total_price / 100.00



          address = Address.find_by_owner_id(current_user.id)

          unless address.nil? || address == 0
            @endereco = address
          else
            @endereco = Address.new
          end

        end

      else
        # flash[:error] = "Para poder efetuar compras é necessário estar logado."
        session[:after_redirect] = cart_path(current_cart)
        redirect_to cart_path(current_cart, login_modal: true)
      end

  end

  def create
    if user_signed_in?
      if current_user.cpf.blank?
        flash[:error] = "Para poder efetuar compras é necessário que um CPF seja cadastrado."
        session[:after_redirect] = cart_path(current_cart)
        redirect_to edit_user_registration_path(current_user)
      elsif current_user.last_name.blank?
        flash[:error] = "Para poder efetuar compras é necessário que o seu nome esteja cadastrado com o sobrenome também."
        session[:after_redirect] = cart_path(current_cart)
        redirect_to edit_user_registration_path(current_user)
      else
        @cart = get_cart
        if @cart.shipping_hash.blank?
          flash[:error] = "Antes de comprar, por favor preencha o seu CEP e clique em calcular frete."
          redirect_to cart_path(@cart, no_shipping: true)
        else


          address = Address.find_by_owner_id(current_user.id)

          unless address.nil? || address == 0

            if address.update_attributes(address_params)
              endereco = 'ok'
            else
              flash[:error] = "Endereço não pôde ser atualizado"
              redirect_to :action => 'new'
            end

          else

            addrs = Address.new(address_params)

            if addrs.save
              endereco = 'ok'
            else
              puts "ERRO AO ADICIONAR ENDEREÇO ---------> #{addrs.errors.full_messages}"
              flash[:error] = "Endereço não pôde ser cadastrado"
              redirect_to :action => 'new'
            end

          end

          if endereco == 'ok'

            puts "PARAMETRO CEP -----> #{params[:address][:zip_code]}"
            puts "PARAMETRO RUA -----> #{params[:address][:street]}"
            puts "PARAMETRO NUMERO -----> #{params[:address][:street_number]}"
            puts "PARAMETRO COMPLEMENTO -----> #{params[:address][:complement]}"
            puts "PARAMETRO BAIRRO -----> #{params[:address][:neighborhood]}"
            puts "PARAMETRO CIDADE -----> #{params[:address][:city]}"
            puts "PARAMETRO ESTADO -----> #{params[:address][:state]}"


            @transaction = @cart.transactions.new
              
            payment = @cart.create_pagseguro_transparente(params[:checkout_card_bandeira], params[:checkout_tipo_pagamento], params[:checkout_sender_hash], params[:checkout_card_token], params[:checkout_card_name], params[:checkout_name], params[:checkout_email], params[:checkout_cpf], params[:checkout_ddd], params[:checkout_telefone], params[:checkout_card_parcelas], params[:checkout_nascimento], params[:address][:zip_code], params[:address][:street], params[:address][:street_number], params[:address][:complement], params[:address][:neighborhood], params[:address][:city], params[:address][:state])

            

            if payment.errors.any?
             
              puts payment.errors.join("\n")
              flash[:error] = "Erro No Pagamento #{payment.errors.join("\n")}"

              redirect_to :action => 'new'
            else
              puts "=> Transaction"
              puts "  code: #{payment.code}"
              puts "  reference: #{payment.reference}"

              puts "---------------PAGAMENTO---------------> #{payment}"

              @transaction.pagseguro_transaction_reference = payment.reference
              @transaction.pagseguro_transaction_code = payment.code

              unless payment.payment_link.blank?
                @transaction.pagseguro_transaction_url = payment.payment_link
              end
              # 
              @transaction.save

              redirect_to :action => 'success', reference_order: payment.reference
             
            end

          end

        end
      end
    else
      # flash[:error] = "Para poder efetuar compras é necessário estar logado."
      session[:after_redirect] = cart_path(current_cart)
      redirect_to cart_path(current_cart, login_modal: true)
    end
  end

  # def create
  #   if user_signed_in?
  #     if current_user.cpf.blank?
  #       flash[:error] = "Para poder efetuar compras é necessário que um CPF seja cadastrado."
  #       session[:after_redirect] = cart_path(current_cart)
  #       redirect_to edit_user_registration_path(current_user)
  #     elsif current_user.last_name.blank?
  #       flash[:error] = "Para poder efetuar compras é necessário que o seu nome esteja cadastrado com o sobrenome também."
  #       session[:after_redirect] = cart_path(current_cart)
  #       redirect_to edit_user_registration_path(current_user)
  #     else
  #       @cart = get_cart
  #       if @cart.shipping_hash.blank?
  #         # flash[:error] = "Antes de comprar, por favor preencha o seu CEP e clique em calcular frete."
  #         redirect_to cart_path(@cart, no_shipping: true)
  #       else
  #         @transaction = @cart.transactions.new

  #         payment = @cart.create_pagseguro(notification_transactions_url, success_transactions_url)

  #         if payment.nil?
  #           flash[:error] = "Ocorreu algum erro com os seus dados. Tente novamente."
  #           redirect_to @cart
  #         else
  #           response = payment.register

  #           if response.errors.any?
  #             flash[:error] = "Ocorreu algum erro."
  #             redirect_to @cart
  #           else
  #             @transaction.pagseguro_transaction_reference = payment.reference
  #             @transaction.pagseguro_transaction_code = response.code
  #             @transaction.pagseguro_transaction_url = response.url
  #             @transaction.save

  #             redirect_to response.url
  #           end
  #         end
  #       end
  #     end
  #   else
  #     # flash[:error] = "Para poder efetuar compras é necessário estar logado."
  #     session[:after_redirect] = cart_path(current_cart)
  #     redirect_to cart_path(current_cart, login_modal: true)
  #   end
  # end



  def success
    @success_buy = true

    if user_signed_in?
      # @buy_cart = current_user.closed_carts.order('updated_at DESC').first
      @transaction = Transaction.find_by_pagseguro_transaction_reference(params[:reference_order])



      unless @transaction.nil?

        @buy_cart = Cart.find(@transaction.cart_id)

        if @buy_cart.user_id == current_user.id

          @total_value = @buy_cart.total_price / 100.0

          @url_popup = nil

          unless @transaction.pagseguro_transaction_url.nil?
            @url_popup = @transaction.pagseguro_transaction_url
          end

        else

          flash.now[:error] = "Operação Proibida"
          redirect_to root_path

        end

      else

        flash.now[:error] = "Não existe transação com essa referência"
        redirect_to root_path

      end
    else
      flash.now[:error] = "Você precisa estar logado para acessar essa página."
      redirect_to root_path
    end
    #render 'static_pages/home'
  end

  def notification
    puts "NOTIFICATIONCODE ---->   #{params[:notificationCode]}"

    transaction = PagSeguro::Transaction.find_by_notification_code(params[:notificationCode])

    puts transaction

    @transaction = Transaction.find_by_pagseguro_transaction_reference(transaction.reference)
    @transaction.update_column(:pagseguro_transaction_hash, transaction)
    @transaction.reload

    if transaction.errors.empty?
      @transaction.status = transaction.status.id.to_i
      @transaction.save
    end

    render nothing: true, status: 200
  end


  def address_params
    params.require(:address).permit(:zip_code, :street, :street_number, :complement, :neighborhood, :city, :state).merge(owner_id: current_user.id, owner_type: "User")
  end

  

end
