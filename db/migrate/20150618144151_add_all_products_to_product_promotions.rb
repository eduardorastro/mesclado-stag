class AddAllProductsToProductPromotions < ActiveRecord::Migration
  def change
    add_column :product_promotions, :all_products, :boolean, default: false
  end
end
