$ ->
  set_everything()
  $(window).resize ->
    set_everything()

set_everything = ->
  w = $(window).width()
  h = $(window).height()

  header_photo_width = 1600
  header_photo_height = 360

  header_h = undefined
  if h/2 > 390
    header_h = 390
  else
    header_h = h/2

  $('#header-carousel').css
    height: "#{header_h}px"


  $('.home-product-variations-row').css
    height: "#{ ((2 * (w / 3)) / 3) * (522 / 445) }"

  $('.home-artists-row').css
    height: "#{ ((2 * (w / 3)) / 3) * 2 }"

  $('.instagram-photos-row').css
    height: "#{ w / 6 }"

  $('.strips-area').find('.strip').css
    height: "#{w/2}px"


  $('.product-thumb').children('img:first').css
    width: '240px'
    height: '262px'

  # is_chrome = navigator.userAgent.indexOf('Chrome') > -1
  # is_explorer = navigator.userAgent.indexOf('MSIE') > -1
  # unless is_explorer
  #   is_explorer = !!navigator.userAgent.match(/Trident.*rv\:11\./)
  # is_firefox = navigator.userAgent.indexOf('Firefox') > -1
  # is_safari = navigator.userAgent.indexOf("Safari") > -1
  # is_Opera = navigator.userAgent.indexOf("Presto") > -1

  # if is_safari && !is_chrome
  #   alert "I'm a Safari Browser"
  #   # $('.home-artists-row').find('.inner-artist-row').find('.col-xs-10').css
  #     # width: '33.34%'
  #   $('.home-artists-row').find('.col-xs-20').css
  #     width: '66.70%'
