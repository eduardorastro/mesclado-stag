class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.references :product_variation, index: true
      t.references :cart, index: true
      t.integer :amount
      t.integer :unit_price_in_cents
      t.integer :discount_in_cents

      t.text :values_ids
      t.text :values_names

      t.references :product_variation_size
      t.string :size

      t.text :variation_values_hash
      t.text :product_variation_hash
      t.text :product_hash
      t.text :artist_hash

      t.datetime :deleted_at

      t.timestamps
    end

    add_index :cart_items, :deleted_at
  end
end
