ActiveAdmin.register ShippingInformation do
  menu parent: 'Frete', priority: 1
  actions :index, :show, :edit, :update

  config.filters = false
  config.batch_actions = false

  permit_params do
    params = [:zip_code, services: [], free_services: []]
    params
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs 'Detalhes Gerais' do
      f.input :zip_code
      # f.input :services, as: :select, collection: { 'PAC' => :pac, 'Sedex' => :sedex, 'E-Sedex' => :e_sedex }, multiple: true
      # f.input :free_services, as: :select, collection: { 'PAC' => :pac, 'Sedex' => :sedex, 'E-Sedex' => :e_sedex }, multiple: true
    end

    f.actions
  end

  show do |si|
    attributes_table do
      row :zip_code do
        si.formatted_zip_code
      end
      # row :services do
      #   if si.services.present?
      #     si.services.reject { |x| x.blank? }.join(' | ')
      #   end
      # end
      # row :free_services do
      #   if si.free_services.present?
      #     si.free_services.reject { |x| x.blank? }.join(' | ')
      #   end
      # end
      row :created_at
      row :updated_at
    end
  end

  controller do
    def index
      info = ShippingInformation.first
      if info.nil?
        info = ShippingInformation.create
      end
      redirect_to admin_shipping_information_path(info)
    end
  end

end
