module ApplicationHelper
  def get_facebook_likes url
    if Rails.env.development?
      link = "https://graph.facebook.com/fql?q=SELECT%20like_count%20FROM%20link_stat%20WHERE%20url='https://www.facebook.com/pages/sejamesclado/1402779966656313'"
    else
      link = "https://graph.facebook.com/fql?q=SELECT%20like_count%20FROM%20link_stat%20WHERE%20url='#{url}'"
    end

    # https://graph.facebook.com/fql?q=SELECT%20url,%20normalized_url,%20share_count,%20like_count,%20comment_count,%20total_count,commentsbox_count,%20comments_fbid,%20click_count%20FROM%20link_stat%20WHERE%20url=%27https://www.facebook.com/pages/sejamesclado/1402779966656313%27
    JSON.parse(HTTParty.get(link))['data'].first['like_count']
  end

  def ensure_https url
    if url.include? 'https'
      return url
    elsif url.include? 'http'
      return url.gsub('http', 'https')
    end

    url
  end
end
