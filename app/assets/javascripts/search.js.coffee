root = exports ? this

root.delay = (ms, func) -> setTimeout func, ms

search_in = false
black = false

ready = ->
  mousescrollfix($('.floating-cart'))
  # mousescrollfix($('search-results'))

  black = $('nav.navbar').hasClass('black-bar')

  $('nav.navbar').find('.search').click ->
    unless search_in
      bring_in_search_bar()


  search()

bring_in_search_bar = ->
  unless black
    $('nav.navbar').addClass('black-bar')
  $('.search-bar').slideDown 'fast', ->
    search_in = true
    $('.search-bar').find('input').focus()

bring_out_search_bar = ->
  unless black
    $('nav.navbar').removeClass('black-bar')

  $('.search-bar').slideUp 'fast', ->
    search_in = false
  $('.search-bar').find('input').focusout()
  $('.search-bar').find('input').val('')

  $('body').css 'overflow-y' : 'auto'
  $('.search-results').fadeOut 'fast', ->
    $('.search-results').html('')
    $('.search-bar').removeClass('small') if $('.search-bar').hasClass('small')


$(document).ready(ready)
$(document).on('page:load', ready)

$(document).mouseup (e) ->
  container = $('.search-bar')
  obj = $(e.target)

  unless obj.hasClass('search-bar') || obj.parents('.search-bar').length > 0 || obj.hasClass('search-results') || obj.parents('.search-results').length > 0
    if search_in
      bring_out_search_bar()

search = ->
  input = $('.search-bar').find('input')
  countdown = undefined

  input.keyup ->
    countdown = root.delay(1000, make_the_search)

  input.keydown ->
    clearTimeout(countdown)


make_the_search = ->
  value = $('.search-bar').find('input').val()
  unless value == ''
    window.start_loading()
    $.ajax '/produtos/busca',
      type: 'GET'
      dataType: 'html'
      data:
        search: value
      complete: ->
        window.stop_loading()
      error: (jqXHR, textStatus, errorThrown) ->
        $.gritter.add
          class_name: 'gritter-error'
          title: 'Erro'
          text: 'Ocorreu algum erro de conexão. Por favor tente novamente.'
      success: (data, textStatus, jqXHR) ->
        results = $('.search-results')
        $('body').css 'overflow' : 'hidden'

        # nav_height = $('nav.navbar').outerHeight()
        # results.css 'padding-top' : "#{nav_height}px"

        results.html(data)
        $('.search-content').find('.close-search').click ->
          bring_out_search_bar()

        $('.search-bar').addClass('small') unless $('.search-bar').hasClass('small')
        results.fadeIn 'fast'


mousescrollfix = (elem) ->
  elem.bind 'mousewheel DOMMouseScroll', (e) ->
    scrollTo = undefined

    if e.type == 'mousewheel'
      scrollTo = (e.originalEvent.wheelDelta * -1)

    else if e.type == 'DOMMouseScroll'
      scrollTo = 40 * e.originalEvent.detail

    # if scrollTo
    #   e.preventDefault()
    #   elem.scrollTo(scrollTo + elem.scrollTop())