class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.string :area_code
      t.string :number
      t.references :owner, polymorphic: true, index: true
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :phones, :deleted_at
  end
end
