# include ActiveAdmin::MetaInfoHelper
# include ActiveAdmin::BannerFormHelper

ActiveAdmin.register Pos do
  menu parent: "Páginas Institucionais", priority: 5, label: "Pontos de Venda"

  permit_params do
    permitted = [:name, :description, :photo, :address]
    permitted
  end

  config.filters = false

  index do
    id_column
    column :name
    column :address
    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs 'Detalhes Gerais' do
      f.input :name
      f.input :description
      f.input :photo
      f.input :address
    end

    f.actions
  end

  controller do
    def index
      scope = Pos.all

      params[:page] ||= 0
      @collection = scope.page(params[:page]).per(10)
    end

    def show
      resource = Pos.find params[:id]
      @pos = @resource = resource
    end

    def new
      resource = Pos.new
      @pos = @resource = resource
    end

    def create
      resource = Pos.new permitted_params[:pos]
      @pos = @resource = resource
      super
    end

    def edit
      resource = Pos.find params[:id]
      @pos = @resource = resource
    end

    def update
      resource = Pos.find params[:id]
      @pos = @resource = resource
      super
    end

    def destroy
      resource = Pos.find params[:id]
      @pos = @resource = resource
      super
    end
  end
end
