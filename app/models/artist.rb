class Artist < ActiveRecord::Base
  include PgSearch
  multisearchable :against => [:name], if: lambda { |record| record.active }
  pg_search_scope :kinda, :against => [:name], using: { trigram: { any_word: true, treshold: 0.2 } }

  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :reserved, :history], sequence_separator: ''
  acts_as_paranoid

  scope :active, -> { where(active: true, deleted_at: nil) }
  scope :inactive, -> { where(active: false, deleted_at: nil) }
  scope :deleted, -> { only_deleted }
  scope :public_ones, -> { where(public: true) }
  scope :private_ones, -> { where(public: false) }

  has_many :products, dependent: :destroy
  has_many :artist_photos, -> { order(:position) }, dependent: :destroy
  accepts_nested_attributes_for :artist_photos, allow_destroy: true

  # has_many :artist_header_photos, dependent: :destroy
  # accepts_nested_attributes_for :artist_header_photos, allow_destroy: true

  has_many :artist_promotions

  has_one :banner, as: :owner, dependent: :destroy, inverse_of: :owner
  accepts_nested_attributes_for :banner, allow_destroy: true

  has_one :meta_info, as: :owner, dependent: :destroy
  accepts_nested_attributes_for :meta_info, allow_destroy: true

  has_and_belongs_to_many :keywords

  validates_presence_of :name, :artist_photos, :banner

  before_validation :check_only_one_default
  after_save :change_children_active_state

  before_restore :restore_children

  def public?
    read_attribute(:public)
  end

  def photo
    first = self.artist_photos.where(default: true).limit(1).first

    if first.nil?
      nil
    else
      first.photo
    end
  end

  def header_photo
    if self.banner.present? and self.banner.banner_photos.present?
      self.banner.banner_photos.limit(1).first.photo
    else
      nil
    end
  end

  def should_generate_new_friendly_id?
    new_record? || slug.blank?
  end

  def artist_photos
    self.destroyed? ? super.with_deleted : super
  end

  def banner
    self.destroyed? ? Banner.with_deleted.where(owner_id: self.id, owner_type: self.class).first : super
  end

  def products
    self.destroyed? ? super.with_deleted : super
  end

  def name
    "#{super}#{self.active ? '' : ' (Desativado)'}"
  end

  def address
    if self.city and self.state.present?
      "#{self.city}, #{self.state}"
    else
      nil
    end
  end

  private

    def change_children_active_state
      if self.active_changed?
        self.products.each do |p|
          puts p.update_column(:active, p.active_logic)
        end
      end
    end

    def restore_children
      ArtistPhoto.restore self.artist_photos.only_deleted.map { |x| x.id }
      Banner.restore self.banner.id
      Product.restore self.products.only_deleted.map { |x| x.id }
    end

    def check_only_one_default
      defaults = self.artist_photos.select { |x| x.default }
      if defaults.length != 1
        self.errors.add :artist_photos, 'should have only one default artist photo'
      end
    end

    def slug_candidates
      [
        :name_no_spaces,
        [:name_no_spaces, :city],
        [:name_no_spaces, :city, :state],
        [:name_no_spaces, :city, :state, :country]
      ]
    end

    def name_no_spaces
      self.name.gsub(' ', '')
    end
end
