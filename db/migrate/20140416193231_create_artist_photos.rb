class CreateArtistPhotos < ActiveRecord::Migration
  def change
    create_table :artist_photos do |t|
      t.references :artist, index: true
      t.boolean :default, default: false
      t.attachment :photo
      t.integer :position, default: 0

      t.datetime :deleted_at

      t.timestamps
    end

    add_index :artist_photos, :deleted_at
  end
end
