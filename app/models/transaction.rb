class Transaction < ActiveRecord::Base
  acts_as_paranoid

  enum status: { initiated: 0, waiting_payment: 1, in_analysis: 2, paid: 3, available: 4, in_dispute: 5, refunded: 6, cancelled: 7 }

  belongs_to :cart, inverse_of: :transactions
  accepts_nested_attributes_for :cart

  serialize :pagseguro_transaction_hash

  def status_description
    case self.status
    when 'waiting_payment'
      'o comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.'
    when 'in_analysis'
      'o comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.'
    when 'paid'
      'a transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.'
    when 'available'
      'a transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.'
    when 'in_dispute'
      'o comprador, dentro do prazo de liberação da transação, abriu uma disputa.'
    when 'refunded'
      'o valor da transação foi devolvido para o comprador.'
    when 'cancelled'
      'a transação foi cancelada sem ter sido finalizada.'
    else
      'a transação ainda está sem status.'
    end
  end

  def status_translation
    case self.status
    when 'waiting_payment'
      'Aguardando Pagamento'
    when 'in_analysis'
      'Em análise'
    when 'paid'
      'Pago'
    when 'available'
      'Disponível'
    when 'in_dispute'
      'Em disputa'
    when 'refunded'
      'Devolvido'
    when 'cancelled'
      'Cancelado'
    else
      'Sem Status'
    end
  end

  after_save :set_cart_as_paid
  after_save :set_user_address, if: -> { self.pagseguro_transaction_hash.present? }
  after_save :set_user_phone, if: -> { self.pagseguro_transaction_hash.present? }
  after_save :send_email

  private

    def set_cart_as_paid
      if self.status == 'paid' or self.status == 'available'
        if self.cart.payment_date.nil?

          self.cart.all_promotions.each do |p|
            p.increment!(:uses)
            # p.uses = p.uses + 1
            # p.save(validate: false)
          end

          self.cart.update_column(:payment_date, DateTime.now)

          self.cart.update_column(:payments_counter, Cart.where.not(payment_date: nil).count)

          self.cart.update_products_number_of_sells
        end
      end
    end

    def set_user_address
      if self.pagseguro_transaction_hash.shipping.present? and self.pagseguro_transaction_hash.shipping.address.present? and self.cart.user.present?
        unless self.cart.user.address.present?
          address_hash = self.pagseguro_transaction_hash.shipping.address
          a = Address.new street: address_hash.street, street_number: address_hash.number, complement: address_hash.complement, neighborhood: address_hash.district, city: address_hash.city, state: address_hash.state, zip_code: address_hash.postal_code
          a.owner = self.cart.user
          a.save
        end
      end
    end

    def set_user_phone
      if self.pagseguro_transaction_hash.sender.present? and self.pagseguro_transaction_hash.sender.phone.present? and self.cart.user.present?
        unless self.cart.user.phone.present?
          phone_hash = self.pagseguro_transaction_hash.sender.phone
          p = Phone.new area_code: phone_hash.area_code, number: phone_hash.number
          p.owner = self.cart.user
          p.save
        end
      end
    end

    def send_email
    #   case self.status
    #   when 'waiting_payment'
    #     TransactionsMailer.new(self).deliver
    #   when 'paid'
    #     TransactionsMailer.paid(self).deliver
    #   when 'cancelled'
    #     TransactionsMailer.refused(self).deliver
    #   end
    end
end
