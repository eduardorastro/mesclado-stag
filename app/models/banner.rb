class Banner < ActiveRecord::Base
  acts_as_paranoid

  scope :active, -> { where(active: true, deleted_at: nil) }
  scope :inactive, -> { where(active: false, deleted_at: nil) }
  scope :deleted, -> { only_deleted }

  belongs_to :owner, -> { with_deleted }, polymorphic: true, inverse_of: :banner

  has_many :banner_photos, -> { order(:position) }, dependent: :destroy, inverse_of: :banner
  accepts_nested_attributes_for :banner_photos, allow_destroy: true

  validates_presence_of :banner_photos

  before_restore :restore_banner_photos

  def banner_photos
    self.destroyed? ? super.with_deleted : super
  end

  def active=(value)
    if self.owner.present?
      value = (value and self.owner.active)
    end
    super
  end

  def name
    "#{super}#{self.active ? '' : ' (Desativado)'}"
  end

  private

    def restore_banner_photos
      BannerPhoto.restore self.banner_photos.only_deleted.map { |x| x.id }
    end
end
