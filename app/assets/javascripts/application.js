// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require extend-jquery
//= require underscore
//= require just_datetime_picker/nested_form_workaround
//= require viewport-units-buggyfill
//= require bootstrap-custom
//= require owl.carousel.min
//= require loading
//= require gritter
//= require header
//= require home
//= require artist
//= require products-pagination
//= require products-index
//= require jquery.zoom.min
//= require product-show
//= require search
//= require cart-show
//= require about
//= require_self
//= require zip_code_completion
//= require waypoints.min
//= require bootstrap-fix

window.viewportUnitsBuggyfill.init();
