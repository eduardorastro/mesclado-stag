# if Rails.env.development?
#   PagSeguro.environment = 'sandbox'
# end
PagSeguro.configure do |config|
  config.token       = ENV["PAGSEGURO_TOKEN"]
  config.email       = ENV['PAGSEGURO_EMAIL']
  config.environment = :production # ou :sandbox. O padrão é production.
  config.encoding    = "UTF-8"
end