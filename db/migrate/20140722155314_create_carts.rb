class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.references :user, index: true
      t.datetime :payment_date
      t.string :tracking_code
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :carts, :deleted_at
  end
end
