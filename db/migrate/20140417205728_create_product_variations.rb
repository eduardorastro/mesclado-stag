class CreateProductVariations < ActiveRecord::Migration
  def change
    create_table :product_variations do |t|
      t.boolean :active, default: true
      t.string :product_variation_code
      t.references :product, index: true
      t.boolean :default, default: false
      t.integer :position, default: 0
      t.datetime :deleted_at

      t.boolean :customize_photos

      t.timestamps
    end
    add_index :product_variations, :active
    add_index :product_variations, :deleted_at
  end
end
