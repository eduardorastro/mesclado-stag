class AboutStrip < ActiveRecord::Base
  belongs_to :about

  has_attached_file :photo, preserve_files: true, :styles => { :big => "800x800#" }
  validates_attachment :photo, presence: true, content_type: { content_type: ["image/jpeg", "image/jpg", "image/JPG", "image/JPEG", "image/gif", "image/png"] }, size: { less_than: 2.megabyte }

  validates_presence_of :strip_title
end
