class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :omniauthable, :omniauth_providers => [:facebook]

  has_many :carts, inverse_of: :user
  has_one :address, as: :owner, inverse_of: :owner, :dependent => :destroy
  accepts_nested_attributes_for :address, allow_destroy: true

  has_one :phone, as: :owner, inverse_of: :owner, :dependent => :destroy
  accepts_nested_attributes_for :phone, allow_destroy: true

  has_many :favorites
  has_many :products, through: :favorites

  validates_presence_of :first_name, :last_name

  attr_accessor :newsletter

  def name
    if self.first_name.present? and self.last_name.present?
      "#{self.first_name} #{self.last_name}"
    else
      self.first_name || self.last_name
    end
  end

  def name= value
    parts = value.split(' ')
    self.first_name = parts.first

    if parts.length > 1
      self.last_name = value.split(' ')[1..-1].join(' ')
    end
  end

  def get_last_open_cart
    self.carts.opened.first
  end

  def active_cart
    self.carts.opened.first_or_create
  end

  def closed_carts
    self.carts.closed
  end

  def formatted_cpf
    if self.cpf.present?
      CPF.new(self.cpf).formatted
    else
      nil
    end
  end

  def self.from_omniauth auth
    where(email: auth.info.email).first_or_create do |user|
      if user.new_record?
        user.password = Devise.friendly_token[0,20]
        user.name = auth.info.name   # assuming the user model has a name
        if user.last_name.blank?

        end
      end
      user.provider = auth.provider
      user.uid = auth.uid
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def has_favorite? product
    Favorite.where(user_id: self.id, product_id: product.id).limit(1).first
  end

  def formatted_id
    "M#{self.id.to_s.rjust(8, '0')}"
  end

  def formatted_phone
    if self.phone.present?
      "#{self.phone.area_code} #{self.phone.number}"
    end
  end

  # validates_uniqueness_of :cpf, allow_nil: true, allow_blank: true
  validates :cpf, cpf: true
  before_validation :strip_cpf

  after_create :create_newsletter, if: -> { self.newsletter }

  private

    def create_newsletter
      unless Newsletter.where(email: self.email).present?
        Newsletter.create email: self.email
      end
    end

    def strip_cpf
      unless self.cpf.blank?
        self.cpf = CPF.new(self.cpf).stripped
      end
    end
end
