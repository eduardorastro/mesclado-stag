class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :cart, index: true
      t.integer :status
      t.string :pagseguro_transaction_code
      t.string :pagseguro_transaction_url
      t.text :pagseguro_transaction_hash
      t.string :pagseguro_transaction_reference
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :transactions, :deleted_at
  end
end
