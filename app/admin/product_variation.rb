ActiveAdmin.register ProductVariation do
  belongs_to :product, optional: true
  menu false

  actions :edit, :update, :index, :show

  scope :active
  scope :inactive

  batch_action :destroy, false
  batch_action :activate do |selection|
    ProductVariation.find(selection).each do |pv|
      pv.update_column(:active, true)
    end
    redirect_to admin_product_product_variations_path(product_id: ProductVariation.find(selection).first.product_id), flash: { notice: 'Camisas ativadas com sucesso.' }
  end
  batch_action :deactivate do |selection|
    ProductVariation.find(selection).each do |pv|
      pv.update_column(:active, false)
    end
    redirect_to admin_product_product_variations_path(product_id: ProductVariation.find(selection).first.product_id), flash: { notice: 'Camisas desativadas com sucesso.' }
  end
  batch_action :set_default do |selection|
    if selection.length > 1
      redirect_to admin_product_product_variations_path(product_id: ProductVariation.find(selection).first.product_id), flash: { error: 'Não é possível ter mais que uma camisa como padrão.' }
    else
      ProductVariation.find(selection).each do |pv|
        pv.update_column(:default, true)
      end
      redirect_to admin_product_product_variations_path(product_id: ProductVariation.find(selection).first.product_id), flash: { notice: 'Padrão definido com sucesso.' }
      end
  end

  permit_params do
   permitted = [:active, :default, :product_variation_code]
   permitted += [product_variation_photos_attributes: [:id, :photo, :_destroy, :position]]
   permitted
  end

  config.filters = false
  config.breadcrumb = true

  index title: -> { "#{Product.find(params[:product_id]).name} / Camisas" } do
    selectable_column
    column :name
    column :product_code
    column :photo do |pv|
      image_tag pv.photo
    end
    column 'Aparecendo?' do |pv|
      pv.ready? ? status_tag( "yes", :ok ) : status_tag( "no" )
    end
    column :active
    column :default
    column :price_in_cents do |pv|
      pv.price_in_cents.to_brazilian_currency
    end
    actions
  end

  form do |f|
    f.inputs "Detalhes" do
      f.input :active
      f.input :default
      # f.input :product_variation_code
    end

    f.has_many :product_variation_photos, heading: 'Fotos (somente a primeira aparece)', sortable: :position, new_record: 'Adicionar Foto', :class => 'inputs' do |photo_form|
      photo_form.input :photo, image_preview: true, size_hint: true
    end

    f.actions
  end

  controller do
    def index
      params[:scope] ||= 'active'
      super
    end

    def update
      update! { admin_product_product_variations_path(resource.product) }
    end
  end

end
