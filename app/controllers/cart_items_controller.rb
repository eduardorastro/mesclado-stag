class CartItemsController < ApplicationController
  layout false
  respond_to :js

  def create
    if params[:redirect_to_cart] == 'true' or params[:redirect_to_cart] == true
      @redirect_to_cart = true
    end

    if params['0'].blank?
      @no_size = true
      # flash.now[:error] = "Você deve escolher um tamanho para comprar um produto."
    else
      @no_size = nil
      @cart = get_cart
      @cart.add_product_variation params['product_variation_id'], params['0']
    end
  end

  def update
    @cart = get_cart
    @cart.update_attributes(shipping_hash: nil, shipping_price_in_cents: 0)
    @cart_item = CartItem.find params[:id]
    @cart_item.update_attributes(amount: params['amount'].to_i)
  end

  def destroy
    @cart = get_cart
    @cart.remove_product_variation params[:id]
  end
end
