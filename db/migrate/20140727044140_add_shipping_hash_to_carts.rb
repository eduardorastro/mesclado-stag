class AddShippingHashToCarts < ActiveRecord::Migration
  def change
    change_table :carts do |t|
      t.text :shipping_hash
      t.references :promotion, index: true
    end
  end
end
