class CreateStaticPages < ActiveRecord::Migration
  def change
    create_table :static_pages do |t|
      t.string :name, null: false
      t.string :title
      t.text :content
      t.string :footer_link_name
      t.string :slug
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :static_pages, :slug
    add_index :static_pages, :deleted_at
  end
end
