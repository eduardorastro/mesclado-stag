class CartPromotion < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :promotion, inverse_of: :cart_promotion

  has_one :advantage, as: :promotion_profile, inverse_of: :promotion_profile
  accepts_nested_attributes_for :advantage, allow_destroy: false

  def owner
    nil
  end

  def type
    'Promoção de Carrinho'
  end

  def minimum_cart_total_price_in_cents= value
    if value.is_a? String
      value = value.gsub(/\D/, '')
    end

    return if value.blank? or value.to_i == 0

    value = value.to_i
    super
  end

  def valid_for_cart? cart
    if self.minimum_cart_total_price_in_cents.present?
      if self.minimum_cart_total_price_in_cents <= cart.total_price_no_shipping_no_discount
        true
      else
        false
      end
    else
      true
    end
  end

  def valid_for? item
    if item.is_a? Cart
      self.valid_for_cart? item
    else
      false
    end
  end

  before_validation :check_existance_of_coupon, if: -> { self.minimum_cart_total_price_in_cents.blank? or self.minimum_cart_total_price_in_cents == 0 }

  private

    def check_existance_of_coupon
      if self.promotion.coupon.blank?
        self.errors.add :cupom, 'deve estar presente em promoções de carrinho sem preço mínimo.'
      end
    end
end
