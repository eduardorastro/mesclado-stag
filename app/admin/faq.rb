include ActiveAdmin::MetaInfoHelper
include ActiveAdmin::BannerFormHelper

ActiveAdmin.register StaticPage, as: 'FAQ' do
  menu parent: "Páginas Institucionais", priority: 3, label: "FAQ"
  actions :index, :show, :edit, :update

  permit_params do
    params = [:name, :title, :content, :footer_link_name, :slug, faq_questions_attributes: [:id, :question, :answer, :position, :_destroy]]
    params += banner_permitted_params
    params += meta_info_permitted_params
    params
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.has_many :faq_questions, heading: "Perguntas", allow_destroy: true, sortable: :position, new_record: "Adicionar Pergunta" do |faq_form|
      faq_form.input :question, input_html: { class: "tinymce small-textarea" }
      faq_form.input :answer, input_html: { class: "tinymce small-textarea" }
    end

    banner_form f

    meta_inputs f

    f.actions
  end

  controller do
    def index
      page = StaticPage.where(name: 'FAQ').first
      if page.nil?
        page = StaticPage.create name: 'FAQ', title: 'FAQ'
      end
      redirect_to admin_faq_path(id: page.id)
    end
  end

end
