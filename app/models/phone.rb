class Phone < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :owner, polymorphic: true

  validates_presence_of :area_code, :number

  after_save :update_user_cart_phone_hash

  private

    def update_user_cart_phone_hash
      if self.owner.present? and self.owner.active_cart.present?
        self.owner.active_cart.update_column(:phone_hash, self.attributes)
      end
    end
end
