class Users::SessionsController < Devise::SessionsController
  layout false
  respond_to :js

  helper_method :after_sign_in_path_for

  def create
    self.resource = warden.authenticate!(auth_options)
    set_flash_message(:notice, :signed_in) if is_flashing_format?
    sign_in(resource_name, resource)
    yield resource if block_given?
    respond_with resource
    # respond_with resource, location: after_sign_in_path_for(resource)
  end

  def auth_options
    { scope: resource_name, recall: "#{controller_path}#render_create" }
  end

  def render_create
    render 'users/sessions/create'
  end

  protected

    def after_sign_in_path_for(resource)
      if session[:after_redirect].present?
        session.delete(:after_redirect)
      else
        super
      end
    end

end


