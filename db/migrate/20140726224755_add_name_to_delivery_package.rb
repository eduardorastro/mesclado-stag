class AddNameToDeliveryPackage < ActiveRecord::Migration
  def change
    add_column :delivery_packages, :name, :string
  end
end
