class CreateArtists < ActiveRecord::Migration
  def change
    create_table :artists do |t|
      t.boolean :active, default: true
      t.string :name, null: false
      t.string :sub_title
      t.text :description
      t.string :slug
      t.integer :number_of_views, default: 0
      t.integer :number_of_sells, default: 0

      t.string :city
      t.string :state
      t.string :country

      t.string :facebook_url
      t.string :instagram_url

      t.datetime :deleted_at

      t.timestamps
    end
    add_index :artists, :slug
    add_index :artists, :active
    add_index :artists, :deleted_at
  end
end
