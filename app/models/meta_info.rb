class MetaInfo < ActiveRecord::Base
  acts_as_paranoid

  belongs_to :owner, -> { with_deleted }, polymorphic: true
end
