class AddPhoneHashToCarts < ActiveRecord::Migration
  def change
    add_column :carts, :phone_hash, :text
  end
end
