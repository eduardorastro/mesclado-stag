ready = ->
  $('.products-tray').each ->
    tray = $(this)
    amount = 3

    tray.owlCarousel
      items: amount
      navSpeed: 500
      responsiveRefreshRate: 10
      loop: true
      dots: false

    $('.carousel-control').click ->
      $(this).parents('.full-height:first').find('.products-tray').trigger($(this).data('slide') + '.owl.carousel')

  parts = ('' + window.location).split('?')
  if parts.length > 1
    for i in parts[1].split('&')
      if i.split('=')[0] == 'reset_password_token'
        $('#reset_modal').modal('show')

  if $('#success_buy').length > 0
    $('#success_buy').modal('show')

$(document).ready(ready)
$(document).on('page:load', ready)
