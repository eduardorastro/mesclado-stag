class StaticPage < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :reserved, :history]

  acts_as_paranoid

  scope :deleted, -> { only_deleted }

  has_many :faq_questions, dependent: :destroy
  accepts_nested_attributes_for :faq_questions, allow_destroy: true

  has_one :banner, as: :owner, dependent: :destroy, inverse_of: :owner
  accepts_nested_attributes_for :banner, allow_destroy: true

  has_one :meta_info, dependent: :destroy, as: :owner
  accepts_nested_attributes_for :meta_info, allow_destroy: true

  validates_presence_of :name, :title
  validates_uniqueness_of :name, case_sensitive: false

  def home?
    self.name == 'Home'
  end

  def faq?
    self.faq_questions.present?
  end

  def contact?
    self.name == 'Contato'
  end

  def faq_questions
    self.destroyed? ? super.with_deleted : super
  end

  def final_name
    self.footer_link_name || self.name
  end

  def active
    true
  end

  before_restore :restore_children

  private

    def restore_children
      FaqQuestion.restore self.faq_questions.only_deleted.map { |x| x.id }
    end

    def slug_candidates
      [
        :name
      ]
    end

    def should_generate_new_friendly_id?
      new_record? || slug.blank?
    end
end
