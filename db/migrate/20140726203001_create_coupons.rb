class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.references :promotion, index: true
      t.string :code
      t.integer :maximum_uses
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :coupons, :deleted_at
  end
end
