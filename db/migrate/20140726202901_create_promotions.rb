class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.integer :uses, default: 0
      t.boolean :active, default: true
      t.datetime :deleted_at

      t.timestamps
    end

    add_index :promotions, :deleted_at
  end
end
