class FaqQuestion < ActiveRecord::Base
  acts_as_paranoid

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :deleted, -> { only_deleted }

  belongs_to :static_page, -> { with_deleted.order('position ASC')}

  validates_presence_of :question, :answer
end
