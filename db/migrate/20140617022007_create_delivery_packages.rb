class CreateDeliveryPackages < ActiveRecord::Migration
  def change
    create_table :delivery_packages do |t|
      t.integer :maximum_weight_in_grams
      t.integer :width_in_centimeters
      t.integer :height_in_centimeters
      t.integer :depth_in_centimeters
      t.integer :maximum_product_amount
      t.integer :extra_price_in_cents, default: 0

      t.integer :position, default: 0
      t.boolean :active, default: true

      t.datetime :deleted_at

      t.timestamps
    end

    add_index :delivery_packages, :deleted_at
    add_index :delivery_packages, :active
  end
end
