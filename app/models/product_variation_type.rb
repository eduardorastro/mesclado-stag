class ProductVariationType < ActiveRecord::Base
  acts_as_paranoid
  acts_as_list

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :deleted, -> { only_deleted }

  has_many :product_variation_type_values, -> { order(:position) }, dependent: :destroy, inverse_of: :product_variation_type
  accepts_nested_attributes_for :product_variation_type_values, allow_destroy: true

  has_and_belongs_to_many :products, join_table: 'products_product_variation_types', inverse_of: :product_variation_types

  validates_presence_of :name

  before_validation :set_products
  after_save :change_children_active_state

  after_commit :update_children_values_if_needed, on: :update
  after_commit :delete_unwanted_product_variations, on: :create

  def product_variation_type_values
    self.destroyed? ? super.with_deleted : super
  end

  def name
    "#{super}#{self.active ? '' : ' (Desativado)'}"
  end

  before_restore :restore_children

  private

    def update_children_values_if_needed
      old_products = self.product_variation_type_values.map { |x| x.product_variations }.flatten.map { |x| x.product }.flatten.uniq
      new_products = self.products

      removed_products = old_products - new_products
      added_products = new_products - old_products

      changed_products = removed_products + added_products

      changed_products.each do |prod|
        prod.update_children_values_if_needed
      end
    end

    def delete_unwanted_product_variations
      if self.product_variation_type_values.reject { |x| x.keep_old_photos == false }.empty?
        self.products.each do |p|
          total_values = p.product_variation_types.length
          p.product_variations.select { |x| x.product_variation_type_values.length != total_values }.each do |pv|
            pv.really_destroy!
          end
        end
      end
    end

    def change_children_active_state
      if self.active_changed?
        self.product_variation_type_values.each do |p|
          p.update_column(:active, p.active_logic)
        end
      end
    end

    def restore_children
      ProductVariationTypeValue.restore self.product product_variation_type_values.only_deleted.map { |x| x.id }
    end

    def set_products
      if self.products.empty?
        self.products = Product.all
      end
    end
end
