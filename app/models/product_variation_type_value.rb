class ProductVariationTypeValue < ActiveRecord::Base
  acts_as_paranoid

  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :deleted, -> { only_deleted }

  has_attached_file :photo, :styles => { :icon => "50x50#" }, preserve_files: true
  validates_attachment :photo, content_type: { content_type: ["image/jpeg", "image/jpg", "image/JPG", "image/JPEG", "image/gif", "image/png"] }

  has_attached_file :selected_photo, :styles => { :icon => "50x50#" }, preserve_files: true
  validates_attachment :selected_photo, content_type: { content_type: ["image/jpeg", "image/jpg", "image/JPG", "image/JPEG", "image/gif", "image/png"] }

  belongs_to :product_variation_type, -> { with_deleted }, inverse_of: :product_variation_type_values

  has_and_belongs_to_many :product_variation_sizes, -> { order(:position) }, join_table: 'product_variation_sizes_product_variation_type_values'

  has_and_belongs_to_many :product_variations, join_table: 'product_variations_product_variation_type_values', inverse_of: :product_variation_type_values

  validates_presence_of :name
  validate :presence_of_value_or_icons

  after_save :change_children_active_state
  after_commit :create_products_variations, on: :create
  before_destroy :destroy_product_variations

  before_restore :restore_children
  before_validation :set_sizes

  def price_in_cents=(value)
    if value.class.to_s.downcase == 'string'
      value = value.gsub(/\D/, '').to_i
    end
    super
  end

  def product_variation_sizes
    self.destroyed? ? super.with_deleted : super
  end

  def product_variations
    self.destroyed? ? super.with_deleted : super
  end

  def active=(value)
    value = active_logic(value)
    super
  end

  def active_logic value = nil
    if value == "0"
      value = false
    elsif value == "1"
      value = true
    end
    value = self.active if value.nil?
    if self.product_variation_type.present?
      value and self.product_variation_type.active
    else
      value
    end
  end

  def name
    "#{super}#{self.active ? '' : ' (Desativado)'}"
  end

  def related_values_without_sizes
    values = ProductVariationTypeValue.where.not(product_variation_type_id: self.product_variation_type_id).joins(:product_variations).where(product_variations: { id:  self.product_variations.ready.pluck(:id) }).includes(:product_variation_type).uniq

    response = values.group_by { |x| x.product_variation_type.id }
  end

  def related_values
    response = self.related_values_without_sizes
    response[0] = self.product_variation_sizes

    response.each do |k, v|
      response[k] = v.sort_by { |x| x.position }.map { |x| x.id }
    end
    response
  end

  def related_values_for product
    ready_combinations = product.product_variations.select { |x| x.ready? }.map { |x| x.product_variation_type_values.map { |y| y.id } }.map { |x| x.sort }

    related = related_values_without_sizes
    # related.each do |k, v|
    #   related[k] = v.sort_by { |x| x.position }.map { |x| x.id }
    # end

    all_combinations = [self.id]

    related.each do |k, v|
      all_combinations = all_combinations.product(v).map { |x| x.flatten }
    end

    final_combinations = []

    all_combinations.each do |v|
      compare = v.map { |x| x.is_a?(Fixnum) ? x : x.id }.sort
      if ready_combinations.include? compare
        final_combinations.push(v)
      end
    end

    final_hash = {}
    final_combinations.map { |y| y.reject { |x| x.is_a? Fixnum } }.each do |comb|
      comb.each do |pvtv|
        final_hash[pvtv.product_variation_type.id] ||= []
        final_hash[pvtv.product_variation_type.id].push pvtv.id
      end
    end

    final_hash[0] = self.related_values[0]

    final_hash
  end

  private

    def set_sizes
      if self.product_variation_sizes.blank?
        self.product_variation_sizes = ProductVariationSize.all
      end
    end

    def presence_of_value_or_icons
      if self.value.blank? and self.photo.blank? and self.selected_photo.blank?
        self.errors.add :product_variation_type_value, 'deve ter ou o código da cor ou ícone'
      elsif self.photo.present? and self.selected_photo.blank?
        self.errors.add :selected_photo, 'não pode ficar em branco'
      elsif self.photo.blank? and self.selected_photo.present?
        self.errors.add :photo, 'não pode ficar em branco'
      end
    end

    def change_children_active_state
      if self.active_changed?
        self.product_variations.each do |p|
          p.update_column(:active, p.active_logic)
        end
      end
    end

    def restore_children
      ProductVariationSize.restore self.product_variation_sizes.only_deleted.map { |x| x.id }
    end

    def create_products_variations
      ProductVariation.create_all_variations_from_value self.id
    end

    def destroy_product_variations
      self.product_variations.each do |pv|
        pv.destroy
      end
    end
end
