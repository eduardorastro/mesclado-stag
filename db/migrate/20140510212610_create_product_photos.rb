class CreateProductPhotos < ActiveRecord::Migration
  def change
    create_table :product_photos do |t|
      t.references :product, index: true
      t.attachment :photo
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :product_photos, :deleted_at
  end
end
