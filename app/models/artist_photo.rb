class ArtistPhoto < ActiveRecord::Base
  acts_as_paranoid

  scope :deleted, -> { only_deleted }

  has_attached_file :photo, preserve_files: true, :styles => { :medium => "381x381#", :thumb => "250x250#" }
  validates_attachment :photo, presence: true, content_type: { content_type: ["image/jpeg", "image/jpg", "image/JPG", "image/JPEG", "image/gif", "image/png"] }, size: { less_than: 1.megabyte }

  belongs_to :artist, -> { with_deleted }
end
