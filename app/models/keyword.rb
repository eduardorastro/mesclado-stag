class Keyword < ActiveRecord::Base
  acts_as_paranoid

  has_and_belongs_to_many :products
  has_and_belongs_to_many :artists

  after_update :delete_if_no_product
  before_save :set_artists

  validates_uniqueness_of :name

  private

    def delete_if_no_product
      if self.products.blank?
        self.destroy
      end
    end

    def set_artists
      self.artists = self.products.map { |x| x.artist }.flatten.uniq
    end
end
