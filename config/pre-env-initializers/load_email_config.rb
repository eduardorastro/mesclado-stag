raw_config = File.read("#{Rails.root}/config/email_config.yml")
EMAIL_CONFIG = YAML.load(raw_config)[Rails.env].symbolize_keys

EMAIL_CONFIG[:user_name] = ENV['EMAIL_USER_NAME']
EMAIL_CONFIG[:password] = ENV['EMAIL_PASSWORD']
