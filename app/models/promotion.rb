class Promotion < ActiveRecord::Base
  just_define_datetime_picker :start_date
  just_define_datetime_picker :end_date

  delegate :advantage, to: :promotion_profile

  acts_as_paranoid

  scope :active, -> { where(active: true) }
  scope :valid, -> { active.where('(start_date <= ? OR start_date IS NULL) AND (end_date >= ? OR end_date IS NULL)', DateTime.now, DateTime.now) }

  scope :no_coupon, -> { includes(:coupon).where(coupons: { promotion_id: nil }) }
  scope :cart, -> { includes(:cart_promotion).where.not(cart_promotions: { promotion_id: nil }) }
  scope :artist, -> { includes(:artist_promotion).where.not(artist_promotions: { promotion_id: nil }) }
  scope :product, -> { includes(:product_promotion).where.not(product_promotions: { promotion_id: nil }) }

  has_one :product_promotion, inverse_of: :promotion, dependent: :destroy
  accepts_nested_attributes_for :product_promotion, allow_destroy: false

  has_one :artist_promotion, inverse_of: :promotion, dependent: :destroy
  accepts_nested_attributes_for :artist_promotion, allow_destroy: false

  has_one :cart_promotion, inverse_of: :promotion, dependent: :destroy
  accepts_nested_attributes_for :cart_promotion, allow_destroy: false

  has_one :coupon, inverse_of: :promotion
  accepts_nested_attributes_for :coupon, allow_destroy: true

  has_many :carts, inverse_of: :promotion
  has_many :cart_items, inverse_of: :promotion

  def promotion_profile
    self.product_promotion || self.artist_promotion || self.cart_promotion
  end

  def advantage_profile
    if self.promotion_profile
      self.advantage.advantage_profile
    end
  end

  def my_advantage
    advantages = []
    if self.advantage_profile.present?
      advantages.push self.advantage_profile.my_advantage
    end

    advantages.push(self.advantage.free_shipping ? 'Frete Grátis' : '')

    advantages.reject { |x| x.blank? }.join(' - ')
  end

  def final_value value
    if self.advantage_profile
      self.advantage_profile.final_value value
    else
      value
    end
  end

  def relative_item
    if self.promotion_profile
      self.promotion_profile.owner
    end
  end

  def type
    if self.promotion_profile
      self.promotion_profile.type
    end
  end

  def coupon_max_use
    if self.coupon.present?
      self.coupon.maximum_uses == 0 ? 'ilimitado' : self.coupon.maximum_uses
    end
  end

  def valid_period
    response = ""
    if self.start_date.present?
      response += "De #{self.start_date.strftime("%d/%m/%Y %H:%M")}<br>"
    end
    if self.end_date.present?
      response += "Até #{self.end_date.strftime("%d/%m/%Y %H:%M")}"
    end
    response.html_safe
  end

  def promotion_profile_advantage= value
    return if self.new_record?

    self.promotion_profile.advantage.assign_attributes(value)
  end

  def on_valid_period?
    (self.start_date.blank? or self.start_date <= DateTime.now) and (self.end_date.blank? or self.end_date >= DateTime.now)
  end

  def valid_for? item
    return false unless self.on_valid_period?

    if self.promotion_profile.present?
      self.promotion_profile.valid_for? item
    else
      false
    end
  end

  before_validation :check_promotion_profile

  after_update :update_cart_items, if: -> { self.active_changed? and self.active == false }
  after_destroy :update_cart_items

  private

    def check_promotion_profile
      profiles_sum = (self.product_promotion ? 1 : 0) + (self.artist_promotion ? 1 : 0) + (self.cart_promotion ? 1 : 0)

      if profiles_sum == 0
        self.errors.add :promotion_profile, 'não pode ficar em branco'
      elsif profiles_sum > 1
        self.errors.add :promotion_profile, 'deve ser de apenas um tipo'
      end
    end

    def update_cart_items
      self.cart_items.each do |ci|
        ci.update_promotions
        # ci.discount_in_cents = 0
        # ci.promotion = nil
        # ci.save(validate: false)
      end
    end
end
