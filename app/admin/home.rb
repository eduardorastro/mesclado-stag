include ActiveAdmin::MetaInfoHelper
include ActiveAdmin::BannerFormHelper

ActiveAdmin.register StaticPage, as: 'Home' do
  menu parent: "Páginas Institucionais", priority: 1, label: "Home"
  actions :index, :show, :edit, :update

  permit_params do
    params = [:name, :title, :content, :footer_link_name, :slug, faq_questions_attributes: [:id, :question, :answer, :position, :_destroy]]
    params += banner_permitted_params
    params += meta_info_permitted_params
    params
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    banner_form f

    meta_inputs f

    f.actions
  end

  controller do

    def index
      page = StaticPage.where(name: 'Home').first
      if page.nil?
        page = StaticPage.create name: 'Home', title: 'Home'
      end
      redirect_to admin_home_path(id: page.id)
    end

  end

end
