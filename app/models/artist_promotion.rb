class ArtistPromotion < ActiveRecord::Base
  acts_as_paranoid

  scope :active, -> { joins(:promotion).where('(promotions.start_date >= ? OR promotions.start_date IS NULL) AND (promotions.end_date <= ? OR promotions.end_date IS NULL)', DateTime.now, DateTime.now).where(promotions: { active: true }).uniq }
  scope :active_with_coupon, -> { active.where(promotion_id: Promotion.joins(:coupon)) }
  scope :active_no_coupon, -> { active.where.not(promotion_id: Promotion.joins(:coupon)) }

  belongs_to :promotion, inverse_of: :artist_promotion
  belongs_to :artist, inverse_of: :artist_promotions

  has_one :advantage, as: :promotion_profile, inverse_of: :promotion_profile
  accepts_nested_attributes_for :advantage, allow_destroy: false

  validates_presence_of :artist
  validates_presence_of :advantage

  def owner
    self.artist
  end

  def type
    'Promoção de Artista'
  end

  def valid_for_cart? cart
    if self.artist_id.present?
      if cart.cart_items.map { |x| x.artist_id }.include? self.artist_id
        true
      else
        false
      end
    else
      false
    end
  end

  def valid_for? item
    if item.is_a? Cart
      self.valid_for_cart? item
    elsif item.is_a? Artist
      item == self.artist
    elsif item.is_a? CartItem
      item.product_variation.product.artist == self.artist
    else
      false
    end
  end

  def remove_promotion_from_opened_carts
    CartItem.where(id: self.artist.joins(products: { product_variations: :cart_items }).joins(products: { product_variations: { cart_items: :cart } }).where('carts.payment_date IS NULL').select('cart_items.id, carts.user_id').uniq.map { |x| x.id }).each do |ci|
      ci.update_column(:discount_in_cents, 0)
    end
  end
end
