ActiveAdmin.register ProductVariationSize do
  menu parent: 'Produtos', priority: 3

  config.sort_order = 'position_asc' # assumes you are using 'position' for your acts_as_list column
  config.paginate   = false # optional; drag-and-drop across pages is not supported

  sortable # creates the controller action which handles the sorting

  scope :active
  scope :inactive

  config.batch_actions = false

  permit_params do
   permitted = [:active, :name, :photo, :selected_photo, :position, :product_variation_type_value_ids => []]
   permitted
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs "Detalhes Gerais", :class => 'inputs expandable in' do
      f.input :active
      f.input :name
    end
    f.inputs "Ícone", :class => 'inputs expandable in' do
      f.input :photo, image_preview: true, size_hint: true
      f.input :selected_photo, image_preview: true, size_hint: true
    end
    f.inputs "Valores", :class => 'inputs expandable in' do
      f.input :product_variation_type_values, as: :select, multiple: true
    end

    f.actions
  end

  filter :name
  filter :active

  index do
    sortable_handle_column
    column :active
    column :name
    actions
  end

  show do
    attributes_table do
      row :active
      row :name
      row :photo do
        if resource.photo.present? and resource.selected_photo.present?
          content = image_tag(resource.photo)
          content += image_tag(resource.selected_photo)
          content
        end
      end
      row :product_variation_type_values do
        content = ''
        resource.product_variation_type_values.each do |v|
          content += link_to v.name, admin_product_variation_type_path(v.product_variation_type)
        end
        content.html_safe
      end
    end
  end

end
