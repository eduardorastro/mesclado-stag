#= require spin.min

opts = {
  lines: 13
  length: 6
  width: 2
  radius: 5
  corners: 1
  rotate: 0
  direction: 1
  color: '#b1aea9'
  speed: 1
  trail: 60
  shadow: false
  hwaccel: false
  className: 'spinner'
  zIndex: 2e9
  top: 'auto'
  left: 'auto'
}

target = undefined
spinner = new Spinner(opts).spin()

$ ->
  $('body').prepend $('<div>').prop('id':'loading_overlay').css
    'width': '100%'
    'height': '100%'
    'position': 'fixed'
    'z-index': '1000000'
    'background-color': 'rgba(0,0,0,0.7)'
    'top': '0'
    'left': '0'

  target = $('#loading_overlay')
  target.append(spinner.el)

  $('.spinner').css
    left : '50%'
    top : '50%'

  target.hide()

window.start_loading = ->
  target.fadeIn('fast')

window.stop_loading = ->
  target.fadeOut('fast')
