class ArtistsController < ApplicationController
  def index
    @artists = Artist.active.public_ones.order('RANDOM()')
  end

  def page
    if request.xhr?
      artists = Artist.find params[:items]
      render partial: 'artists/page', locals: { list: artists }
    end
  end

  def show
    @artist = Artist.find params[:id]

    if @artist.public?
      @products = @artist.products.ready.public_ones
    else
      @products = @artist.products.ready
    end

    @banner = @artist.banner

    @other_artists = Artist.active.public_ones.order('RANDOM()').where.not(id: @artist.id).limit(5)
  end
end
