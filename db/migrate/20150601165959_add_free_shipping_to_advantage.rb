class AddFreeShippingToAdvantage < ActiveRecord::Migration
  def change
    add_column :advantages, :free_shipping, :boolean
  end
end
