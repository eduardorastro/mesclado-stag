include ActiveAdmin::MetaInfoHelper
include ActiveAdmin::BannerFormHelper

ActiveAdmin.register StaticPage, as: 'Contact' do
  menu parent: "Páginas Institucionais", priority: 4, label: "Contato"
  actions :index, :show, :edit, :update

  permit_params do
    params = [:name, :title, :content, :footer_link_name, :slug, faq_questions_attributes: [:id, :question, :answer, :position, :_destroy]]
    params += banner_permitted_params
    params += meta_info_permitted_params
    params
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    banner_form f

    meta_inputs f

    f.actions
  end

  controller do

    def index
      page = StaticPage.where(name: 'Contato').first
      if page.nil?
        page = StaticPage.create name: 'Contato', title: 'Contato'
      end
      redirect_to admin_contact_path(id: page.id)
    end

  end

end
