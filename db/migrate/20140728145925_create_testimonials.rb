class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.string :test_title
      t.string :test_author
      t.string :test_from
      t.references :about, index: true
      t.integer :position

      t.timestamps
    end
  end
end
