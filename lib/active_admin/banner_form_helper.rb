# encoding: UTF-8

module ActiveAdmin::BannerFormHelper
  def banner_permitted_params
    [banner_attributes: [:id, :active, :url, banner_photos_attributes: [:id, :position, :photo, :_destroy]]]
  end

  def banner_form form, opts = {}
    opts[:multiple] = true if opts[:multiple].nil?
    opts[:always_active] = false if opts[:always_active].nil?
    opts[:no_click] = false if opts[:no_click].nil?

    form.inputs "Banner", :for => [:banner, form.object.banner || Banner.new], id: "banner_form", :class => "inputs expandable in" do |banner_form|
      banner_form.input :active unless opts[:always_active]
      banner_form.input :url unless opts[:no_click]
      if banner_form.object.banner_photos.empty? and not opts[:multiple]
        banner_form.object.banner_photos = [BannerPhoto.new]
      end
      banner_form.has_many :banner_photos, heading: opts[:multiple] ? "Fotos" : false, allow_destroy: opts[:multiple], sortable: opts[:multiple] ? :position : false, new_record: opts[:multiple] ? "Adicionar Foto" : false, :class => opts[:multiple] ? 'inputs' : 'inputs no-remove' do |banner_photo_form|
        banner_photo_form.input :photo, image_preview: true, size_hint: true
      end
    end
  end
end
