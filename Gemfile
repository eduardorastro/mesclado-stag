source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.1.10'
# Use postgresql as the database for Active Record
# gem 'pg', '~> 0.17.1'
gem 'pg'
# Use SCSS for stylesheets
# gem 'sass-rails', '~> 4.0.3'
gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
# gem 'uglifier', '~> 2.5.1'
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
# gem 'coffee-rails', '~> 4.0.1'
gem 'coffee-rails'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails', '~> 3.1.1'
gem 'jquery-ui-rails', '~> 5.0.0'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.1.1'
# bundle exec rake doc:rails generates the API under doc/api.
group :doc do
  gem 'sdoc', '~> 0.4.0'
end

# Use unicorn as the app server
# gem 'unicorn', '~> 4.8.3'
gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

# Heroku related gems
group :production do
  # gem 'rails_12factor', '~> 0.0.2'
  gem 'rails_12factor'
  # gem 'silencer', '~> 0.6.0'
  gem 'silencer'
  # gem 'newrelic_rpm', '~> 3.9.0.229'
  gem 'newrelic_rpm'
  # gem 'rack-zippy', '~> 1.2.1'
  gem 'rack-zippy'
end

# gem 'new_relic_ping', '~> 0.1.2'
gem 'new_relic_ping'

# Front-end Related Gems
gem 'haml-rails'
gem 'bourbon'
# gem 'formtastic', '~> 3.1'
gem 'formtastic'

# Development related
group :development do
  gem 'thin'
  
  # gem 'spring', '~> 1.1.3'
  gem 'spring'
  # gem 'bullet', '~> 4.13.0'
  gem 'bullet'
  # gem 'letter_opener', '~> 1.2.0'
  gem 'letter_opener'
  # gem 'better_errors', '~> 1.1.0'
  gem 'better_errors'
  # gem 'pry-rails', '~> 0.3.2'
  gem 'pry-rails'
  # gem 'pry', '~> 0.10.0'
  gem 'pry'
  # gem 'nifty-generators', '~> 0.4.6'
  gem 'nifty-generators'
end

# gem 'dotenv', '~> 0.11.1'
gem 'dotenv'
# gem 'spin_loader', '~> 0.1.0'
# gem 'spin_loader'
# gem 'httparty', '~> 0.13.1'
gem 'httparty'
# gem 'correios-frete', '~> 1.9.1'
# gem 'correios-frete'
gem 'correios-frete', git: 'https://github.com/eduardorph/correios-frete.git'

gem 'correios-cep'

# gem "pagseguro-oficial", '~> 2.1.0', github: 'pagseguro/ruby'
# gem 'pagseguro-oficial', git: 'git://github.com/pagseguro/ruby.git'
gem "pagseguro-oficial", "~> 2.5.0"


# Image upload related
# gem 'paperclip', '~> 4.1.1'
gem 'paperclip'
# gem 'aws-sdk', '~> 1.46.0'
gem 'aws-sdk', '< 2.0'

# Model related
gem 'devise', '~> 3.2.4'
gem 'omniauth-facebook', '~> 1.6.0'
# gem 'squeel'
# gem 'koala', '~> 1.10.0rc2'
gem 'certified', '~> 0.1.2'
gem 'friendly_id', '~> 5.0.4'
# gem 'mail_form', '~> 1.5.0'
gem 'mail_form'
gem 'pg_search', '~> 0.7.4'
# gem 'pg_search'
gem 'acts_as_list', '~> 0.4.0'
gem 'activeadmin-sortable', '~> 0.0.3'
gem 'cpf_cnpj', '~> 0.2.0'
gem 'br_zip_code', '~> 0.2.3'
gem 'activerecord-session_store', '~> 0.1.0'

# Controllers related
# gem 'inherited_resources', '~> 1.6'
gem 'inherited_resources'
gem 'kaminari', '~> 0.16.1'
# gem 'paranoia', '~> 2.0.2'
gem 'paranoia'

# Admin related
# gem 'activeadmin', '~> 1.0.0.pre'
# gem 'activeadmin', github: 'gregbell/active_admin'
gem 'activeadmin', '~> 1.0.0.pre1'
# gem 'activeadmin', github: 'gregbell/active_admin', ref: '506143694215d889b49a0097d62bd1762afbff4a'
gem 'tinymce_aws_file_upload', '~> 0.1.0'
gem 'country-select', '~> 1.1.1'
gem 'activeadmin_expandable_inputs', '~> 0.0.4'
gem 'activeadmin_file_input_on_steroids', '~> 0.0.2'
gem 'just-datetime-picker', '~> 0.0.6'

# View Related
# gem 'bootstrap-sass', '~> 3.1.1.1'
gem 'bootstrap-sass'
gem 'bootstrap-modal-rails', '~> 2.2.5'
# gem 'owlcarousel-rails'
gem 'font-awesome-rails', '~> 4.1.0.0'
gem 'gritter', '1.1.0'
# gem 'metamagic', '~> 3.1.3'
gem 'metamagic'
gem 'underscore-rails'

gem 'browser', '~> 1.0.1'

ruby '2.2.3'
