class About < ActiveRecord::Base
  has_many :about_strips, -> { order('position ASC') }, dependent: :destroy
  accepts_nested_attributes_for :about_strips, allow_destroy: true

  has_many :testimonials, -> { order('position ASC') }, dependent: :destroy
  accepts_nested_attributes_for :testimonials, allow_destroy: true

  has_one :banner, as: :owner, dependent: :destroy, inverse_of: :owner
  accepts_nested_attributes_for :banner, allow_destroy: true

  has_one :meta_info, as: :owner, dependent: :destroy
  accepts_nested_attributes_for :meta_info, allow_destroy: true
end
