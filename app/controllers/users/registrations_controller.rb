class Users::RegistrationsController < Devise::RegistrationsController
  before_filter :configure_permitted_parameters
  layout false, only: [:new]
  respond_to :js

  helper_method :after_sign_up_path_for

  def update
    account_update_params = devise_parameter_sanitizer.sanitize(:account_update)

    if account_update_params[:password].blank?
      account_update_params.delete("password")
      account_update_params.delete("password_confirmation")
    end

    @user = User.find(current_user.id)
    if @user.update_attributes(account_update_params)
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case their password changed
      sign_in @user, :bypass => true
      redirect_to after_update_path_for(@user)
    else
      # flash[:error] = "Não possível atualizar o seu cadastro. Verifique se os campos foram preenchidos corretamente."
      flash[:error] = []
      @user.errors.full_messages.each do |msg|
        flash[:error] << msg
      end
      render "edit"
    end
  end

  protected

     def configure_permitted_parameters
      devise_parameter_sanitizer.for(:sign_up) do |u|
        u.permit(:name, :cpf, :email, :password, :password_confirmation, :birthday)
      end
      devise_parameter_sanitizer.for(:account_update) do |u|
        u.permit(:name, :cpf, :email, :password, :password_confirmation, :current_password, :newsletter, address_attributes: [:zip_code, :street, :street_number, :complement, :neighborhood, :city, :state])
      end
    end

    def after_update_path_for(resource)
      if session[:after_redirect].present?
        session.delete(:after_redirect)
      else
        super
      end
    end

    def after_sign_up_path_for(resource)
      if session[:after_redirect].present?
        session.delete(:after_redirect)
      else
        super
      end
    end
end
